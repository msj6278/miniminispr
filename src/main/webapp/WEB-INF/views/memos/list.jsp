<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../includes/header.jsp" %>
<div id="a">
	<button type="button" data-val="bbb">bb</button>
</div>

<div class="container-fluid" id="b">
	<div class="row board-row board-row-header">
		<div class="col-2">id</div>
		<div class="col-2">이름</div>
		<div class="col-3">이메일</div>
		<div class="col-1">성별</div>
		<div class="col-2">registDate</div>
		<div class="col-2">updateDate</div>
	</div>
</div>

<div class="container-fluid" id="membersList"></div>
<c:if test="${sessionScope.userId!=null}">
	<div class="input-group">
		<div class="input-group-prepend">
			<span class="input-group-text" id="">${sessionScope.userId}</span>
		</div>
		<input type="text" class="form-control" placeholder="메모 내용을 입력해주세요." aria-label="Recipient's username" aria-describedby="basic-addon2" id="content" name="content">
		<div class="input-group-append">
			<button class="btn btn-outline-secondary" type="button" id="reset">초기화</button>
			<button class="btn btn-outline-secondary" type="button" id="write">메모등록</button>
		</div>
	</div>
</c:if>


<script>
$(function(){
	$.ajax({
		type		: 'get',
		url			: '/memos',
	}).done(function(result){
		for(member of result){
			var str = "<div class='row board-row board-row-header'>"
					+	"<div class='col-2'><a href='#'>"+member.userId+"</a></div>"
					+ 	"<div class='col-2'>"+member.userName+"</div>"
					+ 	"<div class='col-3'><a href='#'>"+member.email+"</a></div>"
					+ 	"<div class='col-1'>"+member.gender+"</div>"
					+ 	"<div class='col-2'>"+member.registDate+"</div>"
					+ 	"<div class='col-2'>"+member.updateDate+"</div>"
					+ "</div>";
			$("#b").append(str);
		}//END for()
	});//END ajax
});
</script>

<%@ include file="../includes/footer.jsp" %>