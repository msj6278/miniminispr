<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>    
<%@ include file="../includes/header.jsp" %>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	메모 상세 보기
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
	            <div class="form-group">
	                <label>번호</label>
	                <input class="form-control" name="memoNo"
	                	   value="${mvo.memoNo}" readonly></div>
	            <div class="form-group">
                    <label>한줄 메모</label>
                    <input class="form-control" name="content"
							value='<c:out value="${mvo.content}"/>' readonly></div>
	            <div class="form-group">
	                <label>작성자</label>
	                <input class="form-control" name="writer"
	                	   value="${mvo.userId}" readonly></div>
	            <div class="form-group">
					<label>작성일</label> <input class="form-control" 
						value='<fmt:formatDate value="${mvo.RDate}" pattern="yyyy-MM-dd HH:mm:ss"/>' readonly="readonly">
				</div>
				<!-- 로그인한 사람만 modify 버튼 표시 -->
				<sec:authentication property="principal" var="pinfo"/>
				<sec:authorize access="isAuthenticated()">
					<c:if test="${pinfo.username==mvo.userId}">
	            		<button data-oper='modify' class="btn btn-default">Modify</button>
	            	</c:if>
            	</sec:authorize>
                <button data-oper='list' class="btn btn-info">
                	List</button>
                <form action="/memo/modify" id="operForm">
                	<input type="hidden" name="pageNum" value="${cri.pageNum}">
                	<input type="hidden" name="amount" value="${cri.amount}">
                	<!-- 검색 조건과 키워드 파라미터 추가 -->
                	<input type="hidden" name="type" value="${cri.type}">
                	<input type="hidden" name="keyword" value="${cri.keyword}">
                	<input type="hidden" id="memoNo" name="memoNo" value="${mvo.memoNo }">
                	<input type="hidden" id="memoNo" name="userId" value="${mvo.userId }">
                	<input type="hidden" name="${_csrf.parameterName }" value="${_csrf.token }">
                </form>
                
            </div>	<!-- /.panel-body -->
        </div>		<!-- /.panel -->
    </div>			<!-- /.col-lg-6 -->
</div>				<!-- /.row -->

<!-- 섬네일 원본이미지 전체화면 출력; z-index=100; 어딜 두나 상관 없음-->
<div class='bigPictureWrapper'>
	<div class='bigPicture'></div>
</div>

<!-- 첨부파일 목록 출력-->
<div class='row'>
	<div class='col-lg-12'>
		<div class="panel panel-default">
			<div class="panel-heading">Files</div>
			<div class="panel-body">
				<!-- 업로드 결과 출력 -->
				<div class='uploadResult'>
					<ul>
					</ul>
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- END 첨부파일 목록 -->

<!-- 댓글 목록 -->
<div class='row'>
	<div class='col-lg-12'>
	 	<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-comments fa-fw"></i> Reply
				<button id="addReplyBtn" type="button" class="btn btn-primary pull-right btn-xs">New Reply</button>
			</div>
			<div class="panel-body">
				<!-- START reply list -->
				<ul class="chat"></ul>
			</div>	<!-- /.panel-body -->
			<!-- 댓글 목록 페이징 -->
			<div class="panel-footer">
			</div>
			<!-- END 댓글 목록 페이징 -->
		</div>		<!-- /.panel -->
	</div>			<!-- /.col-lg-12 -->
</div>				<!-- /.row -->
<!-- END 댓글 목록 -->

<!-- 댓글 모달 창 Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">REPLY MODAL</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label>Reply</label>
					<input class="form-control" name="reply" value='New Reply!'>
				</div>
				<div class="form-group">
					<label>Replyer</label>
					<input class="form-control" name="replyer" value='replyer' readonly="readonly">
				</div>
				<div class="form-group">
					<label>Reply Date</label>
					<input class="form-control" name="replyDate" value='2019-12-02 11:22:33'>
				</div>
			</div>
			<div class="modal-footer">
				<button id="modalModBtn" type="button" class="btn btn-warning">Modify</button>
				<button id="modalRemoveBtn" type="button" class="btn btn-danger">Remove</button>
				<button id="modalRegisterBtn" type="button" class="btn btn-primary">Register</button>
				<button id="modalCloseBtn"  type="button" class="btn btn-default" data-dismiss="modal">Close</button>	<!-- dismiss -->
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script src="/resources/js/reply.js"></script>
<script>
//섬네일 이미지 원본 표시
function showImage(filePath){	//블럭안 지역메서드 내부에서만 처리 가능
	$('.bigPictureWrapper').css('display', 'flex').show();
	$('.bigPicture').html('<img src="/display?fileName=' + encodeURI(filePath) +'">')
					.animate({width:'100%', height:'100%'}, 1000);	// ajax 처리, 업로드 경로는 display매핑된 getFile에서 추가
}
(function(){
	//첨부 파일 목록 처리
	$.getJSON("/memo/getAttachList", {memoNo:${mvo.memoNo}}, function(result) {
		console.log(result);
		$(result).each(function(i, obj){
			if(obj.fileType){	
				var filePath = encodeURIComponent(obj.uploadPath+"/s_"+obj.uuid+"_"+obj.fileName);
				var filePath1 = encodeURIComponent(obj.uploadPath+"/"+obj.uuid+"_"+obj.fileName);
				var originPath = obj.uploadPath+"\\"+obj.uuid+"_"+obj.fileName;
				originPath = originPath.replace(new RegExp(/\\/g),"/");
				$('.uploadResult ul').append("<li data-path='"+obj.uploadPath+"' data-uuid='"+obj.uuid+"' data-filename='"
						+obj.fileName+"' data-type='"+obj.fileType+"'><div><img src='/display?fileName="+filePath+"'></li>");
			} else {	//
				var filePath = encodeURIComponent(obj.uploadPath+"/"+obj.uuid+"_"+obj.fileName);
				$('.uploadResult ul').append("<li data-path='"+obj.uploadPath+"' data-uuid='"+obj.uuid+"' data-filename='"
						+obj.fileName+"' data-type='"+obj.fileType+"'><div><span>"+obj.fileName+"</span><br><img src='/resources/img/attach.png'></div></li>");	//이미지가 아니면 attach.png 표시
			}
		});
	}).fail(function(xhr, status, err){
		console.log(err);
	});//END getJSON()	
})();
//END 즉시 실행 함수 - 첨부파일 목록 가져오기
$(function(){
	var boardNo = '${board.bno}';
	var replyUL = $('.chat');
	var pageNum = 1;

	var replyer = null;	//로그인한 id
	<sec:authorize access="isAuthenticated()">
		replyer = '<sec:authentication property="principal.Username"/>';
	</sec:authorize>
	
	var csrfHeaderName = '${_csrf.headerName}';	
	var csrfTokenValue = '${_csrf.token}';

	//댓글 목록 페이징
	var replyPageFooter = $('.panel-footer');
	
	//beforeSend 대신사용
	$(document).ajaxSend(function(e, xhr, options){	//전송 전 추가 헤더 설정
		xhr.setRequestHeader(csrfHeaderName, csrfTokenValue);
	});
	
	//첨부파일 클릭 이벤트 처리
	$(".uploadResult").on("click", "img, span", function(e){
	var li = $(this).closest('li')
		var path = encodeURIComponent(li.data("path")+"\\"+li.data("uuid")+"_"+li.data("filename"));	//한글파일일 경우 전송시 깨지니까 encode
		if(li.data("type")){	//이미지일때 원본 이미지 전체화면 표시
			showImage(path.replace(new RegExp(/\\/g),"/"));
		} else {				//이미지가 아닐때 파일 다운로드 처리
			self.location = ("/download?fileName="+path);	//전체 경로는 controller에서 처리
		}
	});
	
	//원본 이미지(전체화면 이미지) 클릭 이벤트 처리
	$('.bigPictureWrapper').on('click', function(){
		$('.bigPicture').animate({width:'0%', height: '0%'}, 1000);
		//setTimeout(() => { $(this).hide(); }, 1000);	//짝이 있어야 동작
		setTimeout(function(){
			$('.bigPictureWrapper').hide();
		}, 1000);
	});
	
	var frm = $('#operForm');
	$("button[data-oper='modify']").click(function(e){
		frm.attr("action", "/memo/modify").submit();
	});
	$("button[data-oper='list']").click(function(e){
		frm.find("#memoNo").remove();
		frm.attr("action", "/memo/list").submit();
	});
	/* var memoNo = 500; memo.memoNo 없는번호*/
	var memoNo = '${mvo.memoNo}';
	var replyUL = $('.chat');
		
	//댓글 목록 출력 함수 호출 - page 번호는 1로 지정
	showList(1);
	var pageNum = 1;
	//댓글 목록 <li> 구성
	function showList(page){
		replyService.getList(			//댓글 목록
			{ memoNo:memoNo, page:page},
//			function(result){
			function(replyCnt, list){
				replyUL.empty();		//append 삽입때문에 비워줘야함
				if(list == null || list.length<1) {	//댓글 목록이 없으면 replyUL의 내용을 비우고 중단/null 체크 먼저
					if(pageNum < 2){
						replyPageFooter.empty();
						return;
					} else {
						showList(--pageNum);
					}
					
				}
				
				for(rvo of list){		//댓글 목록을 replyUL에 <li>로 추가
					replyUL.append('<li class="left clearfix" data-rno="'+rvo.replyNo+'">'+
		              	'<div>'+
                   			'<div class="header">'+
	                       		'<strong class="primary-font">'+rvo.replyer+'</strong>'+
	                       		'<small class="pull-right text-muted">'+
	                       		replyService.displayTime(rvo.replyDate)+
								'</small>'+
                 	  		'</div>'+
                   			'<p>'+rvo.reply+'</p>'+
                   		'</div>'+
              			'</li>');
				}
				showReplyPage(replyCnt);//pagination 호출
			}
		);//END replyServiceList
		
	}//END showList
	
	function showReplyPage(replyCnt){	//PageDTO.java + list.jsp
		var endNum = Math.ceil(pageNum / 10.0) * 10; //현재 페이지 목록의 마지막 댓글 페이지 번호(10, 20, ...)
		var startNum = endNum - 9;		//시작 페이지 번호
		
		var prev = startNum != 1;		//현재 페이지 목록의 시작 페이지 번호가 1이면 false(prev 버튼 생성 true/false)
		var next = false;
		
		if(endNum * 10 >= replyCnt){	//실제 마지막 댓글의 끝 페이지 번호 
			endNum = Math.ceil(replyCnt/10.0);
		}
		if(endNum * 10 < replyCnt){		//마지막 댓글이 속하는 페이지목록이 아니면 next 버튼 생성 (t/f)
			next = true;
		}
		
		var str = "<ul class='pagination pull-right'>";	// pagination 문자열
		
		if(prev){	// prev 버튼 동적 생성
			str+="<li class='page-item'><a class='page-link' href='"+(startNum-1)+"'>Previous</a></li>";
		}
		for(var i = startNum; i <= endNum; i++){	//페이지 목록 생성(마지막 페이지인 경우 실제 마지막 페이지번호까지), 현재 페이지 번호는 active
			var active = pageNum == i ? "active":"";
			str+="<li class='page-item "+active+" '><a class='page-link' href='"+i+"'>"+i+"</a></li>";
		}
		if(next){	// next 버튼 생성
			str+="<li class='page-item'><a class='page-link' href='"+(endNum+1)+"'>Next</a></li>";
		}
		str += "</ul>";
		replyPageFooter.html(str);	//pagination 문자열 footer에 적용
		
	}
	//END 댓글 목록 페이징
	
	//모달 창 관련 처리----------------------------------------------------------------------
	
	var modal = $(".modal");
	var modalInputReply = modal.find("input[name='reply']");	//.find(); 어떤 요소의 하위 요소 중 특정 요소를 찾을 때 사용
	var modalInputReplyer = modal.find("input[name='replyer']");
	var modalInputReplyDate = modal.find("input[name='replyDate']");
	
	var modalModBtn = $("#modalModBtn");
	var modalRemoveBtn = $("#modalRemoveBtn");
	var modalRegisterBtn = $("#modalRegisterBtn");
	
	$("#addReplyBtn").on("click", function(e){
		modal.modal("show");	//모달창 보이기; 모달객체.modal("show")
		modalInputReply.val('');
		modalInputReplyer.val(replyer);	//replyer를 폼에 추가
		if(replyer==null){	//댓글 작성 버튼 클릭시 경고창 출력
			modalInputReply.attr('readonly', true);
		} else {
			modalInputReply.attr('readonly', false);
		}
		modalInputReplyDate.closest("div").hide();	//	$("li.item-a").closest("ul"); closest item-a 엘리멘트의 가장가까운 부모 요소 하나 선택
		modalRegisterBtn.show();
		modalModBtn.hide();
		modalRemoveBtn.hide();
	});
	
	modalRegisterBtn.on("click", function(e){
		if(replyer==null){	//로그인하지 않은 경우 작성 불가
			alert('로그인 후 작성 가능합니다.');
			modal.modal('hide');
			return;
		}
		replyService.add(	//댓글 추가
			{ memoNo:memoNo, reply:modalInputReply.val(), replyer:modalInputReplyer.val()},
			function(result){
				console.log('reply add result : ' + result);
				modal.modal("hide");	//모달 숨기기
				showList(1);			//댓글 받아오기(첫 페이지)
				showReplyPage(result.replyCnt);//pagenation 함수 호출
			})
	});
	
	$('.chat').on('click', 'li', function(e){	//.on( events [, selector] [, data] , handler(eventObject) )
		var replyNo = $(this).data("rno");
			replyService.get(replyNo, function(result){	//댓글 한개
				
			modalInputReply.val(result.reply);
			modalInputReplyer.val(result.replyer);
			modalInputReplyDate.val(replyService.displayTime(rvo.replyDate)).attr('readonly', 'readonly');
			modalModBtn.show();
			modalRemoveBtn.show();
			modalRegisterBtn.hide();
			modal.data("rno", replyNo);
			modal.modal("show");
		}, function(err){
			console.log(err);
		});
	});
	
	modalRemoveBtn.on("click", function(e){
		var replyNo = modal.data('rno');
		replyService.remove(replyNo,	//댓글 삭제
			modalInputReplyer.val(),
			function(result){
				alert('remove result : ' + result);
				modal.modal("hide");	//모달 숨기기
				showList(pageNum);			//댓글 받아오기
			}, function(err){
				alert('remove err : ' + err);
			});
		modal.modal("hide");	//모달 숨기기
	});
	
	//댓글 수정 버튼 이벤트 처리
	modalModBtn.click(function(){
		//댓글 수정
		replyService.update(
			{ replyNo:modal.data('rno'), 
			  reply:modalInputReply.val(),
			  replyer:modalInputReplyer.val()},
			function(result){
				alert(result);
				modal.modal('hide');
				showList(pageNum);
			},
			function(err){
				console.log('reply update error');
			}
		);//END replyService.update()
	});//END 댓글 수정 버튼 이벤트 처리
	
	replyPageFooter.on("click", "li a", function(e){
		e.preventDefault();
		var targetPageNum = $(this).attr('href');
		pageNum = targetPageNum;
		showList(pageNum);
	});
});
</script>
<%@ include file="../includes/footer.jsp" %>

