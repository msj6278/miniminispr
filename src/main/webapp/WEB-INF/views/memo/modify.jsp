<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>    
<%@ include file="../includes/header.jsp" %>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">메모 수정 하기</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<form method="post" action="/memo/modify" role='form'>
					<div class="form-group">
						<label>번호</label> <input class="form-control" name="memoNo"
							value='<c:out value="${mvo.memoNo}"/>' readonly="readonly">
					</div>
					<div class="form-group">
						<label>한줄 메모</label> <input class="form-control" name="content"
							value='<c:out value="${mvo.content}"/>'>
					</div>
					<div class="form-group">
						<label>작성자</label> <input class="form-control" name="userId"
							value='<c:out value="${mvo.userId}" />' readonly="readonly">
					</div>
					<div class="form-group">
						<label>작성일</label> <input class="form-control" 
							value='<fmt:formatDate value="${mvo.RDate}" pattern="yyyy-MM-dd HH:mm:ss"/>' readonly="readonly">
					</div>
					<input type="hidden" name="RDATE">
					<input type='hidden' name='pageNum' value='<c:out value="${cri.pageNum }"/>'>
					<input type='hidden' name='amount' value='<c:out value="${cri.amount }"/>'>
					<input type='hidden' name='type' value='<c:out value="${cri.type }"/>'>
					<input type='hidden' name='keyword' value='<c:out value="${cri.keyword }"/>'>
					<input type="hidden" name="${_csrf.parameterName }" value="${_csrf.token }">
					
					<button type="submit" data-oper='modify' class="btn btn-default">Modify</button>
					<button type="button" data-oper='remove' class="btn btn-danger">Remove</button>
					<button type="button" data-oper='list' class="btn btn-info">List</button>
				</form>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<!-- 섬네일 원본이미지 전체화면 출력; z-index=100; 어딜 두나 상관 없음-->
<div class='bigPictureWrapper'>
	<div class='bigPicture'></div>
</div>

<!-- 첨부파일 목록 출력-->
<div class='row'>
	<div class='col-lg-12'>
		<div class="panel panel-default">
			<div class="panel-heading">Files</div>
			<div class="panel-body">
				<div class="form-group">
					<div class="uploadDiv">
						<input type="file" name="uploadFile" multiple>
					</div>
				</div>
				<!-- 업로드 결과 출력 -->
				<div class='uploadResult'>
					<ul>
					</ul>
				</div>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- END 첨부파일 목록 -->

<script>
(function(){
	//첨부 파일 목록 처리
	$.getJSON("/memo/getAttachList", {memoNo:${mvo.memoNo}}, function(result) {
		$(result).each(function(i, obj){
			if(obj.fileType){	
				var filePath = encodeURIComponent(obj.uploadPath+"/s_"+obj.uuid+"_"+obj.fileName);
				var filePath1 = encodeURIComponent(obj.uploadPath+"/"+obj.uuid+"_"+obj.fileName);
				var originPath = obj.uploadPath+"\\"+obj.uuid+"_"+obj.fileName;
				originPath = originPath.replace(new RegExp(/\\/g),"/");
				$('.uploadResult ul').append("<li data-path='"+obj.uploadPath+"' data-uuid='"
						+obj.uuid+"' data-filename='"+obj.fileName+"' data-type='"
						+obj.fileType+"'><div><span>"+obj.fileName+"</span><button type='button' class='btn btn-warning btn-circle' data-file='"
						+filePath+"' data-type='image'><i class='fa fa-times'></i></button><br><img src='/display?fileName="+filePath+"'></li>");
			} else {	//
				var filePath = encodeURIComponent(obj.uploadPath+"/"+obj.uuid+"_"+obj.fileName);
				$('.uploadResult ul').append("<li data-path='"+obj.uploadPath+"' data-uuid='"
						+obj.uuid+"' data-filename='"+obj.fileName+"' data-type='"
						+obj.fileType+"'><div><span>"
						+obj.fileName+"</span><button type='button' class='btn btn-warning btn-circle' data-file='"
						+filePath+"' data-type='image'><i class='fa fa-times'></i></button><br><img src='/resources/img/attach.png'></div></li>");	//이미지가 아니면 attach.png 표시
			}
		});
	}).fail(function(xhr, status, err){
		console.log(err);
	}); //END getJSON()	
})();
//END 즉시 실행 함수 - 첨부파일 목록 가져오기

$(function(){
	var regex = new RegExp("(.*?)\.(exe|sh|zip|alz)$");
	var maxSize = 5242880;
	var uploadDivClone = $('.uploadDiv').clone();
	var formObj = $("form[role='form']");
	
	var csrfHeaderName = '${_csrf.headerName}';	
	var csrfTokenValue = '${_csrf.token}';

	//댓글 목록 페이징
	var replyPageFooter = $('.panel-footer');
	
	//beforeSend 대신사용
	$(document).ajaxSend(function(e, xhr, options){	//전송 전 추가 헤더 설정
		xhr.setRequestHeader(csrfHeaderName, csrfTokenValue);
	});
	
	//form 태그 submit 이벤트 처리
	$("button[type='submit']").click(function(e){
		e.preventDefault();
		var str = '';
		$('.uploadResult ul li').each(function(i, obj){	//
			var jobj = $(obj);
			console.log(jobj.data);
			str += "<input type='hidden' name='attachList["+i+"].fileName' value='"+jobj.data("filename")+"'>";	//data-type; AttachFileDTO에 넘어갈 parameter값
			str += "<input type='hidden' name='attachList["+i+"].uuid' value='"+jobj.data("uuid")+"'>";
			str += "<input type='hidden' name='attachList["+i+"].uploadPath' value='"+jobj.data("path")+"'>";
			str += "<input type='hidden' name='attachList["+i+"].fileType' value='"+jobj.data("type")+"'>";
		});
		console.log(str);
		formObj.append(str).submit();
	});
	
	var frm = $("form");
	$('button').click(function(e){
		var oper = $(this).data('oper');
		if(oper==='remove'){
			frm.empty().attr('method','post').attr("action", "/memo/remove");
			frm.append("<input type='hidden' name='pageNum' value='<c:out value="${cri.pageNum }"/>'>");
			frm.append("<input type='hidden' name='amount' value='<c:out value="${cri.amount }"/>'>");
			frm.append("<input type='hidden' name='memoNo' value='<c:out value="${mvo.memoNo }"/>'>");
			frm.append("<input type='hidden' name='userId' value='<c:out value="${mvo.userId }"/>'>");
			frm.append("<input type='hidden' name='type' value='<c:out value="${cri.type }"/>'>");
			frm.append("<input type='hidden' name='keyword' value='<c:out value="${cri.keyword }"/>'>");
			frm.append("<input type='hidden' name='${_csrf.parameterName }' value='<c:out value="${_csrf.token }"/>'>");
			
		} else if(oper==='list') {
			frm.empty().attr('method','get');
			frm.attr("action", "/memo/list");
			frm.append("<input type='hidden' name='pageNum' value='<c:out value="${cri.pageNum }"/>'>");
			frm.append("<input type='hidden' name='amount' value='<c:out value="${cri.amount }"/>'>");
			frm.append("<input type='hidden' name='memoNo' value='<c:out value="${mvo.memoNo }"/>'>");
			frm.append("<input type='hidden' name='type' value='<c:out value="${cri.type }"/>'>");
			frm.append("<input type='hidden' name='keyword' value='<c:out value="${cri.keyword}"/>'>");
		} else {
			return;	//모든 버튼에(파일삭제버튼 경우에도) submit 적용
		}
		frm.submit();
	});
	
	//업로드 결과 출력
	function showUploadedFile(result){
		var li = '';
		$(result).each(function(i, obj){
			if(obj.image){	
				var filePath = encodeURIComponent(obj.uploadPath+"/s_"+obj.uuid+"_"+obj.fileName);
				var filePath1 = encodeURIComponent(obj.uploadPath+"/"+obj.uuid+"_"+obj.fileName);
				var originPath = obj.uploadPath+"\\"+obj.uuid+"_"+obj.fileName;
				originPath = originPath.replace(new RegExp(/\\/g),"/");
				$('.uploadResult ul').append("<li data-path='"+obj.uploadPath+"' data-uuid='"
						+obj.uuid+"' data-filename='"+obj.fileName+"' data-type='"+obj.image+"'><div><span>"
						+obj.fileName+"</span><button type='button' class='btn btn-warning btn-circle' data-file='"
						+filePath+"' data-type='image'><i class='fa fa-times'></i></button><br><img src='/display?fileName="+filePath+"'></li>");
			} else {	//
				var filePath = encodeURIComponent(obj.uploadPath+"/"+obj.uuid+"_"+obj.fileName);
				$('.uploadResult ul').append("<li data-path='"+obj.uploadPath+"' data-uuid='"
						+obj.uuid+"' data-filename='"+obj.fileName+"' data-type='"+obj.image+"'><div><span>"
						+obj.fileName+"</span><button type='button' class='btn btn-warning btn-circle' data-file='"
						+filePath+"' data-type='file'><i class='fa fa-times'></i></button><br><img src='/resources/img/attach.png'></div></li>");	//이미지가 아니면 attach.png 표시
			}
		});
	}
	
	//첨부 파일 확장자 및 크기 확인 함수
	function checkExetension(fileName, fileSize){
		//파일 크기가 maxSize를 초과하는 경우
		if(fileSize > maxSize){
			alert('파일 사이즈 초과');
			return false;
		} else if(regex.test(fileName)){	//파일 확장자가 exe, sh, zip, alz인 경우
			alert('해당 종류의 파일은 업로드할 수 없습니다.');
			return false;
		}
		return true;
	}//END checkExetension()
	
	$(".uploadResult").on('click', 'button', function(e){	//x 버튼 클릭 이벤트 처리(삭제)
		var targetFile = $(this).data('file');
		var type = $(this).data('type');	//jsp 내부 버튼 선택자에서 사용
		var targetLi = $(this).closest('li');
		targetLi.remove();
	});//END ajax
	
	//첨부 파일 등록 이벤트 처리
	$(".uploadDiv").on("change", "input[type='file']", function(){
		var formData = new FormData();
		var inputFile = $("input[name='uploadFile']");
		var files = inputFile[0].files;
		console.log(files);
		for(f of files){
			formData.append('uploadFile', f);
			console.log(f.name);
			console.log(f.size);
			if(!checkExetension(f.name, f.size)) {
				return false;
			}
		}//END for
		console.log(formData);
		$.ajax({
			type		: 'post',
			url			: '/uploadAjaxAction',
			data		: formData,
			dataType	: 'json',
			contentType	: false,
			processData	: false,
			success		: function(result){		//ResponseEntity
				console.log(result);
				$('.uploadDiv').html(uploadDivClone.html());
				showUploadedFile(result);						
			},
			error		: function(error) {
				alert("upload not ok");
				console.log(error);
			}
		});//END .ajax()
	});//END change
});
</script>
<%@ include file="../includes/footer.jsp" %>