<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="container-fluid">
	<div class="row board-row board-row-header">
		<div class="col-4 col-xl-1 col-lg-1 col-md-2 col-sm-4">번호</div>
		<div class="col-4 col-xl-2 col-lg-2 col-md-2 col-sm-4">작성자</div>
		<div class="col-12 col-xl-7 col-lg-6 col-md-4 col-sm-12"><div class="board-row-header-content">한 줄 메모</div></div>
		<div class="col-4 col-xl-2 col-lg-3 col-md-3 col-sm-4">작성일</div>
	</div>
</div>
<div class="container-fluid" id="mList"></div>
<script>
$(function(){
	$('#mList').load('Memo.do?pageNo=1');	//load 메서드도 ajax
	window.addEventListener('popstate', function () {
		(history.state==null)?$('#mList').load('Memo.do?pageNo=1'):$('#mList').html(history.state);
	  });
	$('#write').click(function(){
		if($('#content').val()!="") {
			var formData = "flag=1&content="+$('#content').val();
			$.post('Memo.do', formData,
				function(result) {
					history.pushState(result, '', 'index.jsp?page=board/memo&pageNo=1');
					$('#mList').html(result);
				});
			$('#content').val('');
		}
	});//ajax
	$('#reset').click(function(){
		$('#content').val("");
		$('#content').focus();
	});
});//END $
</script>
<c:if test="${sessionScope.userId!=null}">
	<div class="input-group">
		<div class="input-group-prepend">
			<span class="input-group-text" id="">${sessionScope.userId}</span>
		</div>
		<input type="text" class="form-control" placeholder="메모 내용을 입력해주세요." aria-label="Recipient's username" aria-describedby="basic-addon2" id="content" name="content">
		<div class="input-group-append">
			<button class="btn btn-outline-secondary" type="button" id="reset">초기화</button>
			<button class="btn btn-outline-secondary" type="button" id="write">메모등록</button>
		</div>
	</div>
</c:if>