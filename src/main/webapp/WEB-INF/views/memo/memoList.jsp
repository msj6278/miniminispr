<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:if test="${empty list}">
<div class="row board-row">
	<div class="col-12">등록된 글이 없습니다.</div>
</div>
</c:if>
<c:if test="${!empty list}">
	<!-- 파라미터 el 기본 String 비교 -->
	<c:set var="pageNo" value="${param.pageNo}" scope="request"/>
	<jsp:useBean id="date" class="java.util.Date"/>
	<fmt:formatDate var="nDate" value="${date }" scope="page" pattern="yyyy-MM-dd"/>
	<fmt:parseNumber var="floorPageNo" value="${(param.pageNo-1)/10}" integerOnly="true"/>
	<fmt:parseNumber var="pageNo" value="${param.pageNo}" scope="page"/>
	<fmt:parseNumber var="totalPage" value="${param.totalPage}" scope="page"/>
	<script>
	$(function(){
		$('#pre').click(function(){
			$.get('Memo.do', "pageNo=${(pageNo>11)?pageNo-10:1}",
				function(result) {
					history.pushState(result, '', 'index.jsp?page=board/memo&pageNo=${(pageNo>11)?pageNo-10:1}');
					$('#mList').html(result);
				});
		});
		$('#pre1').click(function(){
			$.get('Memo.do', "pageNo=${pageNo-1}",
				function(result) {
					history.pushState(result, '', 'index.jsp?page=board/memo&pageNo=${pageNo-1}');
					$('#mList').html(result);
				});
		});
		$('#next').click(function(){
			$.get('Memo.do', "pageNo=${(pageNo+10>totalPage)?totalPage:pageNo+10}",
				function(result) {
					history.pushState(result, '', '${(pageNo+10>totalPage)?totalPage:pageNo+10}');
					$('#mList').html(result);
				});
		});
		$('#next1').click(function(){
			$.get('Memo.do', "pageNo=${pageNo+1}",
				function(result) {
					history.pushState(result, '', 'index.jsp?page=board/memo&pageNo=${pageNo+1}');
					$('#mList').html(result);
				});
		});
		<c:forEach items="${list }" var="memvo" varStatus="stat">
			<c:if test="${(sessionScope.userId==memvo.userId)||(sessionScope.userId=='admin')}">
//				$('#del${memvo.memoNo}').one("click", function(){
				$('#mod${memvo.memoNo}').click(function(){
					var length = $('#modText${memvo.memoNo}').val().length; //커서위치 setSelectionRange 사용해서 마지막으로 옮기기 위한 content 길이 저장 변수
					$('#cont1${memvo.memoNo}').toggle();
					$('#cont2${memvo.memoNo}').toggle();
					$('#modText${memvo.memoNo}').focus();
					$('#cont2${memvo.memoNo}').parent().css('padding-top','1px');
					$('#modText${memvo.memoNo}')[0].setSelectionRange(length, length); // JQuery 선택자는 JQuery 객체 반환/ JQuery 내에 setSelectionRange 함수 정의되지않아서 DOM접근(getElement) 필요; [0] 또는 get(0)
				});
				var tBlurFlag="n";
				var rBlurFlag="n";
				var mBlurFlag="n";
				$('#modText${memvo.memoNo}').blur(function(){	//hasFocus document만
					if(tBlurFlag=='n'&&rBlurFlag=='n'){	//리셋, 버튼클릭시 input 이벤트 제외 하기위한 플래그
						$('#cont1${memvo.memoNo}').toggle();
						$('#cont2${memvo.memoNo}').toggle();
						$('#cont2${memvo.memoNo}').parent().css('padding-top','7px');
					}					
				});
				$('#reset${memvo.memoNo}').blur(function(){
					if(tBlurFlag=='n'&&rBlurFlag=='n'){
						$('#cont1${memvo.memoNo}').toggle();
						$('#cont2${memvo.memoNo}').toggle();
						$('#cont2${memvo.memoNo}').parent().css('padding-top','7px');
					}					
				});
				$('#write${memvo.memoNo}').blur(function(){
					if(tBlurFlag=='n'&&rBlurFlag=='n'){
						$('#cont1${memvo.memoNo}').toggle();
						$('#cont2${memvo.memoNo}').toggle();
						$('#cont2${memvo.memoNo}').parent().css('padding-top','7px');
					}					
				});
				$('#modText${memvo.memoNo}').hover(function(){
					tBlurFlag='y';
					}, function(){
					tBlurFlag='n';
					});
				$('#reset${memvo.memoNo}').hover(function(){
					rBlurFlag='y';
					}, function(){
					rBlurFlag='n';
					});
				$('#write${memvo.memoNo}').hover(function(){
					rBlurFlag='y';
					}, function(){
					rBlurFlag='n';
					});
				$('#reset${memvo.memoNo}').click(function(){
					$('#modText${memvo.memoNo}').val('');
					});
				$('#write${memvo.memoNo}').click(function(){
					if($('#modText${memvo.memoNo}').val()!=''){
						if(confirm('메모를 수정하시겠습니까?')) {					
							$.post('Memo.do', "flag=3&pageNo=${pageNo}&memoNo=${memvo.memoNo}&content="+$('#modText${memvo.memoNo}').val(),
								function(result) {
									$('#mList').html(result);
								});
						}
					}
				});
				$('#del${memvo.memoNo}').click(function(){
					if(confirm('메모를 삭제하시겠습니까?')) {
						$.post('Memo.do', "flag=2&pageNo=${pageNo}&memoNo=${memvo.memoNo}",
							function(result) {
								$('#mList').html(result);
							});
					}
				});
			</c:if>
		</c:forEach>
		<c:forEach var="i" begin="${floorPageNo*10+1 }" end="${((floorPageNo*10)+10>=totalPage)?totalPage:(floorPageNo*10)+10 }" step="1">
		$('#pNo${i}').click(function(){
			$.get('Memo.do', "pageNo=${i}",
				function(result) {
					history.pushState(result, '', 'index.jsp?page=board/memo&pageNo=${i}');
					$('#mList').html(result);
				});
		});
		</c:forEach>
	});//END $
	</script>
	<c:forEach items="${list }" var="memvo" varStatus="stat">
		<div class="row board-row board-row-body">
			<div class="col-4 col-xl-1 col-lg-1 col-md-2 col-sm-4">${memvo.memoNo}</div>
			<div class="col-4 col-xl-2 col-lg-2 col-md-2 col-sm-4" ${(memvo.userId=='admin')?'style="color:#00d;"':''}>${(memvo.userId=='admin')?'관리자':memvo.userId}</div>
			<div class="col-12 col-xl-7 col-lg-6 col-md-4 col-sm-12" style="height:auto;word-break:break-all;word-wrap:break-word;">
				<div id="cont1${memvo.memoNo}">${memvo.content}
					<c:if test="${sessionScope.userId==memvo.userId}">
						<i class="material-icons material-icons-create" id="mod${memvo.memoNo}">create</i>
					</c:if>
					<c:if test="${(sessionScope.userId==memvo.userId)||(sessionScope.userId=='admin')}">
						<i class="material-icons material-icons-clear" id="del${memvo.memoNo}">clear</i>
					</c:if>
				</div>
				<c:if test="${(sessionScope.userId==memvo.userId)||(sessionScope.userId=='admin')}">
					<div class="input-group" id="cont2${memvo.memoNo}" style="display: none;">
						<input type="text" class="form-control" placeholder="수정할 내용을 입력해주세요." aria-label="Recipient's username" aria-describedby="basic-addon2"
						id="modText${memvo.memoNo}" name="content" value="${memvo.content}">
						<div class="input-group-append">
							<button class="btn btn-outline-secondary" type="button" id="reset${memvo.memoNo}">비우기</button>
							<button class="btn btn-outline-secondary" type="button" id="write${memvo.memoNo}">메모수정</button>
						</div>
					</div>	
				</c:if>
			</div>
			<fmt:formatDate var="rDate" value="${memvo.rDate}" scope="page" pattern="yyyy-MM-dd"/>
			<div class="col-4 col-xl-2 col-lg-3 col-md-3 col-sm-4">
				<c:if test="${rDate==nDate}">
					<fmt:formatDate var="rTime" value="${memvo.rDate}" scope="page" pattern="HH:mm:ss"/>
					${rTime}
				</c:if>
				<c:if test="${rDate!=nDate}">${nDate}</c:if>
			</div>
		</div>
	</c:forEach>
	<ul class="pagination" style="justify-content: center"> <%--  flexbox  속성 --%>
		<li class="${(pageNo<2)?'page-item disabled':'page-item'}">
			<a class="page-link" href="#" id="pre" onclick="return false;"><<</a>
		</li>
		<li class="${(pageNo<2)?'page-item disabled':'page-item'}">
			<a class="page-link" href="#" id="pre1" onclick="return false;"><</a>
		</li>
		<c:forEach var="i" begin="${floorPageNo*10+1 }" end="${((floorPageNo*10)+10>=totalPage)?totalPage:(floorPageNo*10)+10 }" step="1">
			<li class="${(pageNo==i)?'page-item active':'page-item'}">
				<a class="page-link" href="#" id="pNo${i}" onclick="return false;">${i }</a>
			</li>
		</c:forEach>
		<li class="${(pageNo>=totalPage)?'page-item disabled':'page-item'}">
			<a class="page-link" href="#" id="next1" onclick="return false;">></a>
		</li>
		<li class="${((floorPageNo*10)+10>=totalPage)?((pageNo==totalPage)?'page-item disabled':'page-item'):'page-item'}">
			<a class="page-link" href="#" id="next" onclick="return false;">>></a>
		</li>
	</ul>
</c:if>