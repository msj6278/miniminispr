<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../includes/header.jsp"%>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				한줄 메모 등록
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<form method="post" action="/memo/write" role="form">
					<div class="form-group">
						<label>한줄 메모</label> <input class="form-control" name="content">
					</div>
					<div class="form-group">
						<label>작성자</label> <input class="form-control" name="userId" value="<sec:authentication property="principal.Username"/>" readonly>
					</div>
					<input type="hidden" name="${_csrf.parameterName }" value="${_csrf.token }">
					<button type="submit" class="btn btn-default">등록하기</button>
					<button type="reset" class="btn btn-warning">초기화</button>
				</form>
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-6 -->
</div>
<!-- /.row -->
<!-- 첨부파일 -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<!-- DataTables Advanced Tables -->
				<!-- memo List Page -->
				File Attach
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="form-group">
					<div class="uploadDiv">
						<input type="file" name="uploadFile" multiple>
						<!-- 속성에 multiple='multiple'형태: true/false로 자바스크립트로 제어가능 -->
					</div>
				</div>
				<!-- 업로드 결과 출력 -->
				<div class='uploadResult'>
					<ul>
					</ul>
				</div>
				<!-- 섬네일 원본이미지 출력(이미지 원본보기) -->
				<div class='bigPictureWrapper'>
					<div class='bigPicture'></div>
				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-6 -->
	</div>
	<!-- /.row -->
</div>
<!-- END 첨부파일 -->
<script>
function showImage(filePath){	//블럭안 지역메서드 내부에서만 처리 가능
	$('.bigPictureWrapper').css('display', 'flex').show();
	$('.bigPicture').html('<img src="/display?fileName=' + encodeURI(filePath) +'">')
					.animate({width:'100%', height:'100%'}, 1000);	// ajax 처리, 업로드 경로는 display매핑된 getFile에서 추가
}

$(function(){
	var regex = new RegExp("(.*?)\.(exe|sh|zip|alz)$");
	var maxSize = 5242880;
	var uploadDivClone = $('.uploadDiv').clone();
	var formObj = $("form[role='form']");
	
	//헤더변수, 토큰값
	var csrfHeaderName = '${_csrf.headerName}';	
	var csrfTokenValue = '${_csrf.token}';
	
	//첨부파일 클릭 이벤트 처리
	$(".uploadResult").on("click", "img, span", function(e){
	//	alert($(this).closest('li').data("type"));
	//	console.log($(this).closest());	//parent 바로 상위 부모 요소
	var li = $(this).closest('li')
		var path = encodeURIComponent(li.data("path")+"\\"+li.data("uuid")+"_"+li.data("filename"));	//한글파일일 경우 전송시 깨지니까 encode
		if(li.data("type")){	//이미지일때 원본 이미지 전체화면 표시
			showImage(path.replace(new RegExp(/\\/g),"/"));
		} else {				//이미지가 아닐때 파일 다운로드 처리
			self.location = ("/download?fileName="+path);	//전체 경로는 controller에서 처리
		}
	});
	
	//원본 이미지(전체화면 이미지) 클릭 이벤트 처리
	$('.bigPictureWrapper').on('click', function(){
		$('.bigPicture').animate({width:'0%', height: '0%'}, 1000);
		//setTimeout(() => { $(this).hide(); }, 1000);	//짝이 있어야 동작
		setTimeout(function(){
			$('.bigPictureWrapper').hide();
		}, 1000);
	});
	
	//form 태그 submit 이벤트 처리
	$("button[type='submit']").click(function(e){
		e.preventDefault();
		var str = '';
		
		$('.uploadResult ul li').each(function(i, obj){	//파라미터값 필요(null; 400)
			var jobj = $(obj);
			console.log("jobj:"+jobj);
			str += "<input type='hidden' name='attachList["+i+"].fileName' value='"+jobj.data("filename")+"'>";	//data-type; AttachFileDTO에 넘어갈 parameter값
			str += "<input type='hidden' name='attachList["+i+"].uuid' value='"+jobj.data("uuid")+"'>";
			str += "<input type='hidden' name='attachList["+i+"].uploadPath' value='"+jobj.data("path")+"'>";
			str += "<input type='hidden' name='attachList["+i+"].fileType' value='"+jobj.data("type")+"'>";
		});
		console.log(str);
		formObj.append(str).submit();
	});
	
	//업로드 결과 출력
	function showUploadedFile(result){
		var li = '';
		$(result).each(function(i, obj){
			if(obj.image){
				var filePath = encodeURIComponent(obj.uploadPath+"/s_"+obj.uuid+"_"+obj.fileName);
				var filePath1 = encodeURIComponent(obj.uploadPath+"/"+obj.uuid+"_"+obj.fileName);
				var originPath = obj.uploadPath+"\\"+obj.uuid+"_"+obj.fileName;
				originPath = originPath.replace(new RegExp(/\\/g),"/");
				$('.uploadResult ul').append("<li data-path='"+obj.uploadPath+"' data-uuid='"+obj.uuid+"' data-filename='"
						+obj.fileName+"' data-type='"+obj.image+"'><div><span>"
						+obj.fileName+"</span><button type='button' class='btn btn-warning btn-circle' data-file='"
						+filePath+"' data-type='image'><i class='fa fa-times'></i></button><br><img src='/display?fileName="+filePath+"'></li>");
			} else {
				var filePath = encodeURIComponent(obj.uploadPath+"/"+obj.uuid+"_"+obj.fileName);
				$('.uploadResult ul').append("<li data-path='"+obj.uploadPath+"' data-uuid='"+obj.uuid+"' data-filename='"
						+obj.fileName+"' data-type='"+obj.image+"'><div><span>"
						+obj.fileName+"</span><button type='button' class='btn btn-warning btn-circle' data-file='"
						+filePath+"' data-type='file'><i class='fa fa-times'></i></button><br><img src='/resources/img/attach.png'></div></li>");	//이미지가 아니면 attach.png 표시
			}
		});
	}
	
	//첨부 파일 확장자 및 크기 확인 함수
	function checkExetension(fileName, fileSize){
		//파일 크기가 maxSize를 초과하는 경우
		if(fileSize > maxSize){
			alert('파일 사이즈 초과');
			return false;
		} else if(regex.test(fileName)){	//파일 확장자가 exe, sh, zip, alz인 경우
			alert('해당 종류의 파일은 업로드할 수 없습니다.');
			return false;
		}
		return true;
	}//END checkExetension()
	
	//x 버튼 클릭 이벤트 처리(삭제)
	$(".uploadResult").on('click', 'button', function(){
		var targetFile = $(this).data('file');
		var type = $(this).data('type');	//jsp 내부 버튼 선택자에서 사용
		var targetLi = $(this).closest('li');
		
		$.ajax({
			type	: 'POST',
			url		: '/deleteFile',	//memo 하위 경로라서 절대경로 변경 필요
			data	: { fileName:targetFile, type:type},
			dataType	: 'text',
			beforeSend	: function(xhr){
				xhr.setRequestHeader(csrfHeaderName, csrfTokenValue);
			},
			success		: function(result){
			//	alert("result: "+result);
				targetLi.remove();
			},
			error		: function(error){
				alert("error: "+error);
			}
		})//END ajax
	});
	
	//첨부 파일 등록 이벤트 처리
	$(".uploadDiv").on("change", "input[type='file']", function(){
		var formData = new FormData();
		var inputFile = $("input[name='uploadFile']");
		var files = inputFile[0].files;
		console.log(files);
		for(f of files){
			formData.append('uploadFile', f);
			console.log(f.name);
			console.log(f.size);
			if(!checkExetension(f.name, f.size)) {
				return false;
			}
		}//END for
		console.log(formData);
		$.ajax({
			type		: 'post',
			url			: '/uploadAjaxAction',
			data		: formData,
			dataType	: 'json',
			contentType	: false,
			processData	: false,
			beforeSend	: function(xhr){
				xhr.setRequestHeader(csrfHeaderName, csrfTokenValue);
			},
			success		: function(result){		//ResponseEntity
				console.log(result);
				$('.uploadDiv').html(uploadDivClone.html());
				showUploadedFile(result);						
			},
			error		: function(error) {
				alert("upload not ok");
				console.log(error);
			}
		});//END .ajax()
	});//END change
});//END $
</script>
<%@ include file="../includes/footer.jsp"%>