<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="includes/header.jsp"%>

<div class="container-fluid">
		<div class="row">
			<div class="offset-sm-1 col-sm-10 offset-md-3_2 col-md-9 offset-lg-2 col-lg-8 offset-xl-5_2 col-xl-7 offset-xxl-3 col-xxl-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h5 class="title"><strong>회원인증 대기</strong></h5>
					</div>
					<div class="panel-body panel-height-no-scroll" id="panelTerms">
						<form name='f' action='/login' method='POST'>
							<fieldset>
								<div class="form-group">
									<p>이메일 인증이 완료되지 않았습니다. 회원 인증 후 이용가능합니다.</p>
									<input type="button" value="인증 메일 다시 받기" id="loginBtn">
								</div>
							</fieldset>
						</form>
					</div>
					<div class="panel-footer">
						<input type="button" class="btn btn-default btn-block" value="메인 화면으로">
						<input type="button" id="verified" class="btn btn-success btn-block" value="회원가입 완료">
					</div>
				</div>
			</div>
		</div>
	</div>

<%@ include file="includes/footer.jsp"%>