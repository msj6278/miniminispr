<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<style>
.panel-height {
	max-height: calc(100vh - 300px);
	overflow: scroll;
	overflow-x: hidden;
}
@media ( min-width : 1680px) {
	.col-xxl {
		-ms-flex-preferred-size: 0;
		flex-basis: 0;
		-ms-flex-positive: 1;
		flex-grow: 1;
		max-width: 100%;
	}
	.col-xxl-auto {
		-ms-flex: 0 0 auto;
		flex: 0 0 auto;
		width: auto;
		max-width: 100%;
	}
	.col-xxl-1 {
		-ms-flex: 0 0 8.333333%;
		flex: 0 0 8.333333%;
		max-width: 8.333333%;
	}
	.col-xxl-2 {
		-ms-flex: 0 0 16.666667%;
		flex: 0 0 16.666667%;
		max-width: 16.666667%;
	}
	.col-xxl-3 {
		-ms-flex: 0 0 25%;
		flex: 0 0 25%;
		max-width: 25%;
	}
	.col-xxl-4 {
		-ms-flex: 0 0 33.333333%;
		flex: 0 0 33.333333%;
		max-width: 33.333333%;
	}
	.col-xxl-5 {
		-ms-flex: 0 0 41.666667%;
		flex: 0 0 41.666667%;
		max-width: 41.666667%;
	}
	.col-xxl-6 {
		-ms-flex: 0 0 50%;
		flex: 0 0 50%;
		max-width: 50%;
	}
	.col-xxl-7 {
		-ms-flex: 0 0 58.333333%;
		flex: 0 0 58.333333%;
		max-width: 58.333333%;
	}
	.col-xxl-8 {
		-ms-flex: 0 0 66.666667%;
		flex: 0 0 66.666667%;
		max-width: 66.666667%;
	}
	.col-xxl-9 {
		-ms-flex: 0 0 75%;
		flex: 0 0 75%;
		max-width: 75%;
	}
	.col-xxl-10 {
		-ms-flex: 0 0 83.333333%;
		flex: 0 0 83.333333%;
		max-width: 83.333333%;
	}
	.col-xxl-11 {
		-ms-flex: 0 0 91.666667%;
		flex: 0 0 91.666667%;
		max-width: 91.666667%;
	}
	.col-xxl-12 {
		-ms-flex: 0 0 100%;
		flex: 0 0 100%;
		max-width: 100%;
	}
	.offset-xxl-1 {
		margin-left: 8.333333%;
	}
	.offset-xxl-2 {
		margin-left: 16.666667%;
	}
	.offset-xxl-3 {
		margin-left: 25%;
	}
	.imgWindowContainer-xxl {
		position: relative;
		width: 100%; /* The size you want */
	}
	.imgWindowContainer-xxl:after {
		content: "";
		display: block;
		padding-bottom: 75%;
		/* The padding depends on the width, not on the height, so with a padding-bottom of 100% you will get a square */
	}
	.imgWindowContainer-xxl img {
		position: absolute; /* Take your picture out of the flow */
		top: 0;
		bottom: 0;
		left: 0;
		right: 0; /* Make the picture taking the size of it's parent */
		width: 100%; /* This if for the object-fit */
		height: 100%; /* This if for the object-fit */
		object-fit: contain;
		/* Equivalent of the background-size: cover; of a background-image */
		object-position: center;
	}
}
.article-form{
	margin-top: 50px;
}
</style>
<article class="article-form">
	<div class="container-fluid">
		<div class="row">
			<div class="offset-sm-1 col-sm-10 offset-md-3_2 col-md-9 offset-lg-2 col-lg-8 offset-xl-5_2 col-xl-7 offset-xxl-3 col-xxl-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h5 class="title"><strong>회원가입 약관</strong></h5>
					</div>
					<div class="panel-body panel-height" id="panelTerms">
						<form name='f' action='/login' method='POST'>
							<fieldset>
								<div class="form-group">
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing
										elit. Aenean commodo ligula eget dolor. Aenean massa. Cum
										sociis natoque penatibus et magnis dis parturient montes,
										nascetur ridiculus mus. Donec quam felis, ultricies nec,
										pellentesque eu, pretium quis, sem. Nulla consequat massa quis
										enim. Donec pede justo, fringilla vel, aliquet nec, vulputate
										eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis
										vitae, justo. Nullam dictum felis eu pede mollis pretium.
										Integer tincidunt. Cras dapibus. Vivamus elementum semper
										nisi. Aenean vulputate eleifend tellus. Aenean leo ligula,
										porttitor eu, consequat vitae, eleifend ac, enim. Aliquam
										lorem ante, dapibus in, viverra quis, feugiat a, tellus.
										Phasellus viverra nulla ut metus varius laoreet. Quisque
										rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue.
										Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam
										rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem
										quam semper libero, sit amet adipiscing sem neque sed ipsum.
										Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id,
										lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae
										sapien ut libero venenatis faucibus. Nullam quis ante. Etiam
										sit amet orci eget eros faucibus tincidunt. Duis leo. Sed
										fringilla mauris sit amet nibh. Donec sodales sagittis magna.
										Sed consequat, leo eget bibendum sodales, augue velit cursus
										nunc,Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
										Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
										natoque penatibus et magnis dis parturient montes, nascetur
										ridiculus mus. Donec quam felis, ultricies nec, pellentesque
										eu, pretium quis, sem. Nulla consequat massa quis enim. Donec
										pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
										In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
										justo. Nullam dictum felis eu pede mollis pretium. Integer
										tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean
										vulputate eleifend tellus. Aenean leo ligula, porttitor eu,
										consequat vitae, eleifend ac, enim. Aliquam lorem ante,
										dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra
										nulla ut metus varius laoreet. Quisque rutrum. Aenean
										imperdiet. Etiam ultricies nisi vel augue. Curabitur
										ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.
										Maecenas tempus, tellus eget condimentum rhoncus, sem quam
										semper libero, sit amet adipiscing sem neque sed ipsum. Nam
										quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem.
										Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien
										ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet
										orci eget eros faucibus tincidunt. Duis leo. Sed fringilla
										mauris sit amet nibh. Donec sodales sagittis magna. Sed
										consequat, leo eget bibendum sodales, augue velit cursus
										nunc,Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
										Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
										natoque penatibus et magnis dis parturient montes, nascetur
										ridiculus mus. Donec quam felis, ultricies nec, pellentesque
										eu, pretium quis, sem. Nulla consequat massa quis enim. Donec
										pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
										In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
										justo. Nullam dictum felis eu pede mollis pretium. Integer
										tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean
										vulputate eleifend tellus. Aenean leo ligula, porttitor eu,
										consequat vitae, eleifend ac, enim. Aliquam lorem ante,
										dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra
										nulla ut metus varius laoreet. Quisque rutrum. Aenean
										imperdiet. Etiam ultricies nisi vel augue. Curabitur
										ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.
										Maecenas tempus, tellus eget condimentum rhoncus, sem quam
										semper libero, sit amet adipiscing sem neque sed ipsum. Nam
										quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem.
										Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien
										ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet
										orci eget eros faucibus tincidunt. Duis leo. Sed fringilla
										mauris sit amet nibh. Donec sodales sagittis magna. Sed
										consequat, leo eget bibendum sodales, augue velit cursus
										nunc,Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
										Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
										natoque penatibus et magnis dis parturient montes, nascetur
										ridiculus mus. Donec quam felis, ultricies nec, pellentesque
										eu, pretium quis, sem. Nulla consequat massa quis enim. Donec
										pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
										In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
										justo. Nullam dictum felis eu pede mollis pretium. Integer
										tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean
										vulputate eleifend tellus. Aenean leo ligula, porttitor eu,
										consequat vitae, eleifend ac, enim. Aliquam lorem ante,
										dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra
										nulla ut metus varius laoreet. Quisque rutrum. Aenean
										imperdiet. Etiam ultricies nisi vel augue. Curabitur
										ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.
										Maecenas tempus, tellus eget condimentum rhoncus, sem quam
										semper libero, sit amet adipiscing sem neque sed ipsum. Nam
										quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem.
										Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien
										ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet
										orci eget eros faucibus tincidunt. Duis leo. Sed fringilla
										mauris sit amet nibh. Donec sodales sagittis magna. Sed
										consequat, leo eget bibendum sodales, augue velit cursus
										nunc,Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
										Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
										natoque penatibus et magnis dis parturient montes, nascetur
										ridiculus mus. Donec quam felis, ultricies nec, pellentesque
										eu, pretium quis, sem. Nulla consequat massa quis enim. Donec
										pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
										In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
										justo. Nullam dictum felis eu pede mollis pretium. Integer
										tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean
										vulputate eleifend tellus. Aenean leo ligula, porttitor eu,
										consequat vitae, eleifend ac, enim. Aliquam lorem ante,
										dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra
										nulla ut metus varius laoreet. Quisque rutrum. Aenean
										imperdiet. Etiam ultricies nisi vel augue. Curabitur
										ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.
										Maecenas tempus, tellus eget condimentum rhoncus, sem quam
										semper libero, sit amet adipiscing sem neque sed ipsum. Nam
										quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem.
										Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien
										ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet
										orci eget eros faucibus tincidunt. Duis leo. Sed fringilla
										mauris sit amet nibh. Donec sodales sagittis magna. Sed
										consequat, leo eget bibendum sodales, augue velit cursus
										nunc,Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
										Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
										natoque penatibus et magnis dis parturient montes, nascetur
										ridiculus mus. Donec quam felis, ultricies nec, pellentesque
										eu, pretium quis, sem. Nulla consequat massa quis enim. Donec
										pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
										In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
										justo. Nullam dictum felis eu pede mollis pretium. Integer
										tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean
										vulputate eleifend tellus. Aenean leo ligula, porttitor eu,
										consequat vitae, eleifend ac, enim. Aliquam lorem ante,
										dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra
										nulla ut metus varius laoreet. Quisque rutrum. Aenean
										imperdiet. Etiam ultricies nisi vel augue. Curabitur
										ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.
										Maecenas tempus, tellus eget condimentum rhoncus, sem quam
										semper libero, sit amet adipiscing sem neque sed ipsum. Nam
										quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem.
										Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien
										ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet
										orci eget eros faucibus tincidunt. Duis leo. Sed fringilla
										mauris sit amet nibh. Donec sodales sagittis magna. Sed
										consequat, leo eget bibendum sodales, augue velit cursus
										nunc,Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
										Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
										natoque penatibus et magnis dis parturient montes, nascetur
										ridiculus mus. Donec quam felis, ultricies nec, pellentesque
										eu, pretium quis, sem. Nulla consequat massa quis enim. Donec
										pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
										In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
										justo. Nullam dictum felis eu pede mollis pretium. Integer
										tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean
										vulputate eleifend tellus. Aenean leo ligula, porttitor eu,
										consequat vitae, eleifend ac, enim. Aliquam lorem ante,
										dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra
										nulla ut metus varius laoreet. Quisque rutrum. Aenean
										imperdiet. Etiam ultricies nisi vel augue. Curabitur
										ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.
										Maecenas tempus, tellus eget condimentum rhoncus, sem quam
										semper libero, sit amet adipiscing sem neque sed ipsum. Nam
										quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem.
										Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien
										ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet
										orci eget eros faucibus tincidunt. Duis leo. Sed fringilla
										mauris sit amet nibh. Donec sodales sagittis magna. Sed
										consequat, leo eget bibendum sodales, augue velit cursus
										nunc,Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
										Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
										natoque penatibus et magnis dis parturient montes, nascetur
										ridiculus mus. Donec quam felis, ultricies nec, pellentesque
										eu, pretium quis, sem. Nulla consequat massa quis enim. Donec
										pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
										In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
										justo. Nullam dictum felis eu pede mollis pretium. Integer
										tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean
										vulputate eleifend tellus. Aenean leo ligula, porttitor eu,
										consequat vitae, eleifend ac, enim. Aliquam lorem ante,
										dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra
										nulla ut metus varius laoreet. Quisque rutrum. Aenean
										imperdiet. Etiam ultricies nisi vel augue. Curabitur
										ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.
										Maecenas tempus, tellus eget condimentum rhoncus, sem quam
										semper libero, sit amet adipiscing sem neque sed ipsum. Nam
										quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem.
										Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien
										ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet
										orci eget eros faucibus tincidunt. Duis leo. Sed fringilla
										mauris sit amet nibh. Donec sodales sagittis magna. Sed
										consequat, leo eget bibendum sodales, augue velit cursus
										nunc,Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
										Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
										natoque penatibus et magnis dis parturient montes, nascetur
										ridiculus mus. Donec quam felis, ultricies nec, pellentesque
										eu, pretium quis, sem. Nulla consequat massa quis enim. Donec
										pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
										In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
										justo. Nullam dictum felis eu pede mollis pretium. Integer
										tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean
										vulputate eleifend tellus. Aenean leo ligula, porttitor eu,
										consequat vitae, eleifend ac, enim. Aliquam lorem ante,
										dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra
										nulla ut metus varius laoreet. Quisque rutrum. Aenean
										imperdiet. Etiam ultricies nisi vel augue. Curabitur
										ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.
										Maecenas tempus, tellus eget condimentum rhoncus, sem quam
										semper libero, sit amet adipiscing sem neque sed ipsum. Nam
										quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem.
										Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien
										ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet
										orci eget eros faucibus tincidunt. Duis leo. Sed fringilla
										mauris sit amet nibh. Donec sodales sagittis magna. Sed
										consequat, leo eget bibendum sodales, augue velit cursus
										nunc,Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
										Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
										natoque penatibus et magnis dis parturient montes, nascetur
										ridiculus mus. Donec quam felis, ultricies nec, pellentesque
										eu, pretium quis, sem. Nulla consequat massa quis enim. Donec
										pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.
										In enim justo, rhoncus ut, imperdiet a, venenatis vitae,
										justo. Nullam dictum felis eu pede mollis pretium. Integer
										tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean
										vulputate eleifend tellus. Aenean leo ligula, porttitor eu,
										consequat vitae, eleifend ac, enim. Aliquam lorem ante,
										dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra
										nulla ut metus varius laoreet. Quisque rutrum. Aenean
										imperdiet. Etiam ultricies nisi vel augue. Curabitur
										ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.
										Maecenas tempus, tellus eget condimentum rhoncus, sem quam
										semper libero, sit amet adipiscing sem neque sed ipsum. Nam
										quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem.
										Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien
										ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet
										orci eget eros faucibus tincidunt. Duis leo. Sed fringilla
										mauris sit amet nibh. Donec sodales sagittis magna. Sed
										consequat, leo eget bibendum sodales, augue velit cursus nunc,</p>
								</div>
								<div class="checkbox">
									<label for="termsChk"> <input type="checkbox"
										id="termsChk">이용약관에 동의합니다.
									</label>
								</div>
							</fieldset>
						</form>
					</div>
					<div class="panel-footer">
						<input type="reset" class="btn btn-default btn-block" value="취소" onclick="back()">
						<input type="button" id="agree" class="btn btn-success btn-block disabled" value="확인" onclick="agree()">
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<script>
function agree() {
	chk = document.getElementById('termsChk');
	if (chk.checked) {
		$(function() {
			$('#wrapper').load('joinForm');
		});
	} else {
		alert('약관에 동의하지 않았습니다.');
	}
}
function back() {
	history.back();
}
</script>
<script>
$(function(){
	$('#panelTerms').on('scroll', _.debounce(function() {
		//스크롤 도달시 회원가입 버튼 활성화
		if($('#panelTerms').prop('scrollHeight') <= ($('#panelTerms').scrollTop() + $('#panelTerms').height() + 31)) {
			$('#agree').removeClass('disabled');
			$('#panelTerms').off('scroll');
		}
	}, 50));//END 스크롤 이벤트 처리
});
</script>