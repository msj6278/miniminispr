<%@ page language="java" contentType="text/html; charset=UTF-8"
		pageEncoding="UTF-8"%>
<style>
.article-form{
	margin-top: 50px;
}
.panel-height {
	max-height: calc(100vh - 300px);
	overflow: scroll;
	overflow-x: hidden;
}

.wrap-loading { 
	position: fixed;
	left: 0;
	right: 0;
	top: 0;
	bottom: 0;
	background: rgba(0, 0, 0, 0.2); /*not in ie */
	filter: progid:DXImageTransform.Microsoft.Gradient(startColorstr='#20000000',
		endColorstr='#20000000'); /* ie */
}

.wrap-loading div { /*로딩 이미지*/
	position: fixed;
	top: 50%;
	left: 50%;
	margin-left: -21px;
	margin-top: -21px;
}

.display-none { /*감추기*/
	display: none;
}

.row.no-pad {
  margin-right:0;
  margin-left:0;
}
.row.no-pad > [class*='col-'] {
  padding-right:0;
  padding-left:0;
}

.glbl{
	font-weight: lighter;
}

.imgWindowContainer-xs {
	position: relative;
	width: 100%;
}
.imgWindowContainer-xl:after {
	content: "";
	display: block;
	padding-bottom: 50%;
}
.imgWindowContainer-xs img {
	border: 1px solid lightgray;
	position: absolute;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;
	width: 100%;
	height: 100%;
	object-fit: contain;
	object-position: center;
}

.intro{
	width: 100%;
	height: 100%; 
}

.introContainer-xs{
	width: 100%;
    height: 200px;
}

@media (min-width: 576px) {
	.introContainer-sm{
		width: 100%;
	    height: 150px;
	}
	.imgWindowContainer-sm {
		position: relative;
		width: 100%; /* The size you want */
	}
	.imgWindowContainer-sm:after {
		content: "";
		display: block;
		padding-bottom: 45%;
	}
	.imgWindowContainer-sm img {
		border: 1px solid lightgray;
		position: absolute;
		top: 0;
		bottom: 0;
		left: 0;
		right: 0;
		width: 100%;
		height: 100%;
		object-fit: contain;
		object-position: center;
	}
}
@media (min-width: 768px) {
	.introContainer-md{
		width: 100%;
	    height: 300px;
	}
	.imgWindowContainer-md {
		position: relative;
		width: 100%;
		height:300px;
	}
}
@media (min-width: 1200px) {
	.offset-xl-05 {
		margin-left: 4.166667%;
	}
	.imgWindowContainer-xl {
		position: relative;
		width: 100%;
		height:300px;
	}
}

@media ( min-width : 1680px) {
	.col-xxl {
		-ms-flex-preferred-size: 0;
		flex-basis: 0;
		-ms-flex-positive: 1;
		flex-grow: 1;
		max-width: 100%;
	}
	.col-xxl-auto {
		-ms-flex: 0 0 auto;
		flex: 0 0 auto;
		width: auto;
		max-width: 100%;
	}
	.col-xxl-1 {
		-ms-flex: 0 0 8.333333%;
		flex: 0 0 8.333333%;
		max-width: 8.333333%;
	}
	.col-xxl-2 {
		-ms-flex: 0 0 16.666667%;
		flex: 0 0 16.666667%;
		max-width: 16.666667%;
	}
	.col-xxl-3 {
		-ms-flex: 0 0 25%;
		flex: 0 0 25%;
		max-width: 25%;
	}
	.col-xxl-4 {
		-ms-flex: 0 0 33.333333%;
		flex: 0 0 33.333333%;
		max-width: 33.333333%;
	}
	.col-xxl-5 {
		-ms-flex: 0 0 41.666667%;
		flex: 0 0 41.666667%;
		max-width: 41.666667%;
	}
	.col-xxl-6 {
		-ms-flex: 0 0 50%;
		flex: 0 0 50%;
		max-width: 50%;
	}
	.col-xxl-7 {
		-ms-flex: 0 0 58.333333%;
		flex: 0 0 58.333333%;
		max-width: 58.333333%;
	}
	.col-xxl-8 {
		-ms-flex: 0 0 66.666667%;
		flex: 0 0 66.666667%;
		max-width: 66.666667%;
	}
	.col-xxl-9 {
		-ms-flex: 0 0 75%;
		flex: 0 0 75%;
		max-width: 75%;
	}
	.col-xxl-10 {
		-ms-flex: 0 0 83.333333%;
		flex: 0 0 83.333333%;
		max-width: 83.333333%;
	}
	.col-xxl-11 {
		-ms-flex: 0 0 91.666667%;
		flex: 0 0 91.666667%;
		max-width: 91.666667%;
	}
	.col-xxl-12 {
		-ms-flex: 0 0 100%;
		flex: 0 0 100%;
		max-width: 100%;
	}
	.offset-xxl-1 {
		margin-left: 8.333333%;
	}
	.offset-xxl-2 {
		margin-left: 16.666667%;
	}
	.offset-xxl-3 {
		margin-left: 25%;
	}
	.imgWindowContainer-xxl {
		position: relative;
		width: 100%;
		height:350px;
	}
	.imgWindowContainer-xxl {
		position: relative;
		width: 100%;
		height: 350px;
	}
	.introContainer-xxl{
		width: 100%;
	    height: 350px;
	}
}

.spanContainer{
	height: 1em;
}

#delPhotoBtn{
	cursor: pointer;
}
</style>
<article class="article-form" id="joinForm">
	<div class="container-fluid">
		<div class="row">
			<div class="offset-sm-1 col-sm-10 offset-md-3_2 col-md-9 offset-lg-2 col-lg-8 offset-xl-5_2 col-xl-7 offset-xxl-3 col-xxl-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h5 class="title"><strong>회원가입 양식</strong></h5>
					</div>
					<div class="panel-body panel-height">
						<form action="join/joinForm" method="POST" role="join">
							<fieldset>
								<div class="form-group">
									<label for="userId">ID</label>
									<input type="text" name="userId" id="userId" class="form-control" 
										placeholder="영문 소문자, 숫자 5 ~ 10자이내" size="25" autofocus="autofocus">
									<div class="spanContainer">
										<span id="userIdCheckMessage" class="msg">필수 입력사항입니다.</span>
									</div>
								</div>
								<div class="form-group">
									<label for="password1">비밀번호</label>
									<input type="password" name="userPassword" id="password1" class="form-control" 
										placeholder="영어 소문자,숫자 5 ~ 10자이내" size="25">
									<div class="spanContainer">
										<span id="password1CheckMessage" class="msg">필수 입력사항입니다.</span>
									</div>
								</div>
								<div class="form-group">
									<label for="password2">비밀번호 확인</label>
									<input type="password" id="password2" class="form-control" 
										placeholder="영어 소문자,숫자 5 ~ 10자이내" size="25">
									<div class="spanContainer">
										<span id="password2CheckMessage" class="msg">필수 입력사항입니다.</span>
									</div>
								</div>
								<div class="form-group">
									<label for="userName">이름</label>
									<input type="text" name="userName" id="userName" class="form-control"
										placeholder="이름을 입력하세요." size="25">
									<div class="spanContainer">
										<span id="userNameCheckMessage" class="msg">필수 입력사항입니다.</span>
									</div>
								</div>
								<div class="form-group">
									<label for="email1">이메일</label>
									<div class="row no-pad">
										<div class="col-xs-12 col-sm-12 col-md-5">
											<input type="text" class="email form-control" id="email1" size="10">
										</div>
										<div class="col-xs-12 col-sm-6 col-md-4">
											<div class="input-group">
												<span class="input-group-addon">@</span>
												<input type="text" class="email form-control" id="email2" size="10">
											</div>
										</div>
										<div class="col-xs-12 col-sm-6 col-md-3">
											<select size="1" id="email3" class="email form-control" style="height:34px">
												<option value="direct" selected="selected">직접입력</option>
												<option value="naver.com">naver.com</option>
												<option value="gmail.com">gmail.com</option>
												<option value="daum.net">daum.net</option>
											</select>
										</div>
									</div>
									<div class="spanContainer">
										<span id="emailCheckMessage" class="msg">필수 입력사항입니다.</span>
									</div>
								</div>
								<div class="form-group">
									<label>성별</label>
									<div class="row no-pad">
										<div class="col-xs-6">
											<input type="radio" name="gender" id="genderM" value="m">
											<label for="genderM" class="glbl">남자</label>
										</div>
										<div class="col-xs-6">
											<input type="radio" name="gender" id="genderF" value="f">
											<label for="genderF" class="glbl">여자</label>
										</div>
									</div>
									<div class="spanContainer">
										<span id="genderCheck" class="msg">필수 입력사항입니다.</span>
									</div>
								</div>
								<div class="form-group">
									<label>프로필 사진</label>
									<div class="input-group">
										<input type="file" name="uploadFile" class="form-control">
										<div id="delPhotoBtn" class="input-group-addon">
											<span>사진 삭제</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row no-pad">
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-5 col-xxl-5   
											imgWindowContainer-xs imgWindowContainer-md 
											imgWindowContainer-lg imgWindowContainer-xl imgWindowContainer-xxl">
											<img src="/resources/img/none.png" id="photo" width="100%" alt="프로필 사진">
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-7 col-xxl-7 
											introContainer-xs introContainer-md introContainer-xxl">
											<textarea id="intro" name="intro" class="intro" placeholder="자기소개를 입력해 주세요."></textarea>
										</div>
										<div class="offset-md-6 offset-xl-5">
											<div class="spanContainer">
												<span id="introCheckMessage" class="msg"></span>
											</div>
										</div>
									</div>
								</div>
								<input type='hidden' name='email'>
								<input type='hidden' name='photo'>
							</fieldset>
						</form>
					</div>
					<div class="panel-footer">
						<input type="reset" class="btn btn-default btn-block" value="취소" onclick="back()">
						<input type="button" id="submitJoinForm" class="btn btn-success btn-block" value="회원 가입">
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<script type="text/javascript" src="/resources/js/members/joinForm.js"></script>
