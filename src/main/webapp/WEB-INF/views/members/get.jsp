<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../includes/header.jsp" %>

<style>
.btn-circle {
  width: 45px;
  height: 45px;
  line-height: 45px;
  text-align: center;
  padding: 0;
  border-radius: 50%;
}
</style>
${userId}
<div id="pp">
	<img id="photo">
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h1>회원 상세 보기</h1>
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<div class="form-group">
					<label>회원 아이디</label>
					<input class="form-control" name="userId" readonly>
				</div>
				<div class="form-group">
					<label>회원 이름</label>
					<input class="form-control" name="userName" readonly>
				</div>
				<div class="form-group">
					<label>Email</label>
					<input class="form-control" name="email" readonly>
				</div>
				<div class="form-group">
					<label>성별</label>
					<input class="form-control" name="gender" readonly>
				</div>
				<div class="form-group">
					<label>자기소개</label>
					<textarea class="form-control" name="intro" readonly></textarea>
				</div>
				<div class="form-group">
					<label>가입일</label>
					<input class="form-control" name="registDate" readonly>
				</div>
				<div class="form-group">
					<label>마지막 이용일</label>
					<input class="form-control" name="updateDate" readonly>
				</div>
				<!-- 로그인한 사람만 modify 버튼 표시 -->
				<sec:authentication property="principal" var="pinfo"/>
				<sec:authorize access="isAuthenticated()">
					<c:if test="${pinfo.username==mvo.userId}">
						<button data-oper='modify' class="btn btn-default">Modify</button>
					</c:if>
				</sec:authorize>
				<form action="/memo/modify" id="operForm">
					<input type="hidden" name="pageNum" value="${cri.pageNum}">
					<input type="hidden" name="amount" value="${cri.amount}">
					<!-- 검색 조건과 키워드 파라미터 추가 -->
					<input type="hidden" name="type" value="${cri.type}">
					<input type="hidden" name="keyword" value="${cri.keyword}">
					<input type="hidden" name="${_csrf.parameterName }" value="${_csrf.token }">
				</form>
				
			</div>	<!-- /.panel-body -->
		</div>		<!-- /.panel -->
	</div>			<!-- /.col-lg-6 -->
</div>				<!-- /.row -->

	<button id='toMembersBtn' class="btn btn-info">List</button>
	<div id="btns">
	</div>
<!-- 댓글 목록 -->
<div class='row'>
	<div class='col-lg-12'>
	 	<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-comments fa-fw"></i> Reply
				<button id="addReplyBtn" type="button" class="btn btn-primary pull-right btn-xs">New Reply</button>
			</div>
			<div class="panel-body">
				<!-- START reply list -->
				<ul class="chat"></ul>
			</div>	<!-- /.panel-body -->
			<!-- 댓글 목록 페이징 -->
			<div class="panel-footer">
			</div>
			<!-- END 댓글 목록 페이징 -->
		</div>		<!-- /.panel -->
	</div>			<!-- /.col-lg-12 -->
</div>				<!-- /.row -->
<!-- END 댓글 목록 -->

<!-- 댓글 모달 창 Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">REPLY MODAL</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label>Reply</label>
					<input class="form-control" name="reply" value='New Reply!'>
				</div>
				<div class="form-group">
					<label>Replyer</label>
					<input class="form-control" name="replyer" value='replyer' readonly="readonly">
				</div>
				<div class="form-group">
					<label>Reply Date</label>
					<input class="form-control" name="replyDate" value='2019-12-02 11:22:33'>
				</div>
			</div>
			<div class="modal-footer">
				<button id="modalModBtn" type="button" class="btn btn-warning">Modify</button>
				<button id="modalRemoveBtn" type="button" class="btn btn-danger">Remove</button>
				<button id="modalRegisterBtn" type="button" class="btn btn-primary">Register</button>
				<button id="modalCloseBtn"  type="button" class="btn btn-default" data-dismiss="modal">Close</button>	<!-- dismiss -->
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<script>

$(function(){
	var url = new URL(location);
	var params  = url.searchParams;
	
	var csrfHeaderName = '${_csrf.headerName}';	
	var csrfTokenValue = '${_csrf.token}';
	
	//beforeSend 대신사용
	$(document).ajaxSend(function(e, xhr, options){	//전송 전 추가 헤더 설정
		xhr.setRequestHeader(csrfHeaderName, csrfTokenValue);
	});
	
	getMember();
	
	function displayTime(timeValue){
		var date = new Date(timeValue);
		var ndate = new Date();
		
		var yy = (date.getFullYear()>9?'':'0')+date.getFullYear();
		var mm = date.getMonth()+1>9?date.getMonth()+1:'0'+(date.getMonth()+1);
		var dd = (date.getDate()>9?'':'0')+date.getDate();
		
		var nyy = (ndate.getFullYear()>9?'':'0')+ndate.getFullYear();
		var nmm = ndate.getMonth()+1>9?ndate.getMonth()+1:'0'+(ndate.getMonth()+1);
		var ndd = (ndate.getDate()>9?'':'0')+ndate.getDate();
		
		var date1 = yy+'/'+mm+'/'+dd;
		var ndate1 = nyy+'/'+nmm+'/'+ndd;
		
		if(ndate1 === date1){	//오늘 작성한 댓글은 시:분:초 출력
			var hh = date.getHours()>9?date.getHours():'0'+date.getHours();
			var mi = date.getMinutes()>9?date.getMinutes():'0'+date.getMinutes();
			var ss = date.getSeconds()>9?date.getSeconds():'0'+date.getSeconds(); 
			return hh+':'+mi+':'+ss;
		} else {				//오늘 이전 작성한 댓글은 연/월/일 출력
			return date1;
		}
	}
	
	function getMember(){
		return new Promise((resolve, reject) => {
			$.ajax({
				type		: 'get',
				url			: '/members/${userId}',
				contentType : 'text/plain'
			}).done(function(result, status, xhr){
				history.replaceState({}, null, url);
				if(xhr.status===204){
					alert('회원이 존재하지 않습니다.');
					return;
				}
				$("input[name='userId']").val(result.userId);
				$("input[name='userName']").val(result.userName);
				$("input[name='email']").val(result.email);
				$("input[name='gender']").val(result.gender=='m'?"남":"여");
				$("textarea[name='intro']").val(result.intro);
				$("input[name='registDate']").val(displayTime(result.registDate));
				$("input[name='updateDate']").val(displayTime(result.updateDate));
				if(result.photo){
					$('#photo').attr('src','/display?fileName=profile/'+result.photo).data('filename',result.photo)
								.after("<button type='button' class='btn btn-warning btn-circle' data-type='file'>"
										+"<i class='fa fa-times'></i></button>");
				} else {
					$('#photo').attr('src','/resources/img/none.png');
				}
				if(result.authList[0].auth==='ROLE_SUSPENDED'){
					$('#btns').html("<button id='changeAuthBtn' class='btn btn-warning'"
									+ "data-auth='"+ result.authList[0].auth +"'>정지해제</button>");
				} else if(result.authList[0].auth==='ROLE_USER') {
					$('#btns').html("<button id='changeAuthBtn' class='btn btn-danger'"
									+ "data-auth='"+ result.authList[0].auth +"'>회원정지</button>");
				}
				resolve('회원 정보 불러오기 성공');
			}).fail(function(xhr, status){
				reject('회원정보를 가져오지 못했습니다.');
			});
		});
	}
	
	$('#photo').on('error', function(){
		$(this).unbind('error').attr('src','/resources/img/imageNotFound.png');
	});
	
	$('#toMembersBtn').click(function(){
		$('#wrapper').load('/members?'+ params +' #wrapper', function(data){
			history.pushState({}, null, '/members?'+params);
		});
	});
	
	const swalWithBootstrapButtons = Swal.mixin({
		customClass: {
		confirmButton: 'btn btn-success',
		cancelButton: 'btn btn-danger'
		},
		buttonsStyling: false
	});
	
	$('#btns').on('click', 'button', function(){
		var auth = $(this).data("auth");
		var mainTitle;
		if(auth==='ROLE_USER'){
			mainTitle = "계정을 정지 하시겠습니까?";
			
		} else if(auth==='ROLE_SUSPENDED'){
			mainTitle = "정지를 해제 하시겠습니까?";
		}
		
		swalWithBootstrapButtons.fire({
			  title:mainTitle,
			  text: "You won't be able to revert this!",
			  icon: 'warning',
			  showCancelButton: true,
			  confirmButtonText: 'Yes, delete it!',
			  cancelButtonText: 'No, cancel!',
			  reverseButtons: true
		}).then((result) => {
			if (result.value) {
				suspendMember(this).then(function(){
					return getMember();
				}).then(function(){
					swalWithBootstrapButtons.fire(
						'Deleted!',
						'Your file has been deleted.',
						'success'
					);
				}).catch(function(reject){
					alert(reject);
				});
		  } else if (
		    /* Read more about handling dismissals below */
		    result.dismiss === Swal.DismissReason.cancel
		  ) {
		    swalWithBootstrapButtons.fire(
				'Cancelled',
				'Your imaginary file is safe :)',
				'error'
			)}
		});
	});
	
	$('#pp').on('click', "[data-type='file']", function(){
		deleteFile().then(function(resolve){
			return deletePhoto();
		}).then(function(resolve){
			$('#photo').attr('src','/resources/img/none.png');
			$("[data-type='file']").remove();
			alert(resolve);
		}).catch(function(reject){
			alert(reject);
		});
		
	});
	
	function deleteFile(){
		return new Promise((resolve, reject) => {
			var targetFile = "profile/"+$('#photo').data('filename');
			$.ajax({
				type	: 'POST',
				url		: '/deleteFile',	//memo 하위 경로라서 절대경로 변경 필요
				data	: { fileName:targetFile, type:null},
				dataType: 'text'
			}).done(function(result){
				resolve("파일 삭제 성공");
			}).fail(function(xhr, status, error){
				reject("파일 삭제 실패");
			});//END ajax
		});
	}//END deleteFile()
	
	function deletePhoto(){
		return new Promise((resolve, reject) => {
			var userId = $("input[name='userId']").val();
			$.ajax({
				type	: 'PUT',
				url		: '/members/deletePhoto',
				data	: JSON.stringify({userId:userId}),
				contentType	: 'application/json'
			}).done(function(result){
				resolve("프로필 사진 삭제 성공");
			}).fail(function(xhr, status, error){
				console.log(status);
				reject("프로필 사진 삭제 실패");
			});//END ajax
		});
	}//END deletePhoto()
	
	function suspendMember(object){
		return new Promise((resolve, reject) => {
			var userId = $("input[name='userId']").val();
			var auth = $(object).data("auth");
			$.ajax({
				type	: 'PUT',
				url		: '/members/spnd',
				data	: JSON.stringify({userId:userId, auth:auth}),
				contentType	: 'application/json'
			}).done(function(result){
				resolve("업데이트 실패");
			}).fail(function(xhr, status, error){
				console.log(status);
				reject("업데이트 성공");
			});//END ajax
		});
	}//END deletePhoto()
});
</script>


<!-- <script src="/resources/js/reply.js"></script>
<script>
/* (function(){
	//첨부 파일 목록 처리
	$.getJSON("/memo/getAttachList", {memoNo:${mvo.memoNo}}, function(result) {
		console.log(result);
		$(result).each(function(i, obj){
			if(obj.fileType){	
				var filePath = encodeURIComponent(obj.uploadPath+"/s_"+obj.uuid+"_"+obj.fileName);
				var filePath1 = encodeURIComponent(obj.uploadPath+"/"+obj.uuid+"_"+obj.fileName);
				var originPath = obj.uploadPath+"\\"+obj.uuid+"_"+obj.fileName;
				originPath = originPath.replace(new RegExp(/\\/g),"/");
				$('.uploadResult ul').append("<li data-path='"+obj.uploadPath+"' data-uuid='"+obj.uuid+"' data-filename='"
						+obj.fileName+"' data-type='"+obj.fileType+"'><div><img src='/display?fileName="+filePath+"'></li>");
			} else {	//
				var filePath = encodeURIComponent(obj.uploadPath+"/"+obj.uuid+"_"+obj.fileName);
				$('.uploadResult ul').append("<li data-path='"+obj.uploadPath+"' data-uuid='"+obj.uuid+"' data-filename='"
						+obj.fileName+"' data-type='"+obj.fileType+"'><div><span>"+obj.fileName+"</span><br><img src='/resources/img/attach.png'></div></li>");	//이미지가 아니면 attach.png 표시
			}
		});
	}).fail(function(xhr, status, err){
		console.log(err);
	});//END getJSON()	
})(); */
//END 즉시 실행 함수 - 첨부파일 목록 가져오기
$(function(){
	var boardNo = '${board.bno}';
	var replyUL = $('.chat');
	var pageNum = 1;

	var replyer = null;	//로그인한 id
	<sec:authorize access="isAuthenticated()">
		replyer = '<sec:authentication property="principal.Username"/>';
	</sec:authorize>
	
	var csrfHeaderName = '${_csrf.headerName}';	
	var csrfTokenValue = '${_csrf.token}';

	//댓글 목록 페이징
	var replyPageFooter = $('.panel-footer');
	
	//beforeSend 대신사용
	$(document).ajaxSend(function(e, xhr, options){	//전송 전 추가 헤더 설정
		xhr.setRequestHeader(csrfHeaderName, csrfTokenValue);
	});
	
	//첨부파일 클릭 이벤트 처리
	$(".uploadResult").on("click", "img, span", function(e){
	var li = $(this).closest('li')
		var path = encodeURIComponent(li.data("path")+"\\"+li.data("uuid")+"_"+li.data("filename"));	//한글파일일 경우 전송시 깨지니까 encode
		if(li.data("type")){	//이미지일때 원본 이미지 전체화면 표시
			showImage(path.replace(new RegExp(/\\/g),"/"));
		} else {				//이미지가 아닐때 파일 다운로드 처리
			self.location = ("/download?fileName="+path);	//전체 경로는 controller에서 처리
		}
	});
	
	//원본 이미지(전체화면 이미지) 클릭 이벤트 처리
	$('.bigPictureWrapper').on('click', function(){
		$('.bigPicture').animate({width:'0%', height: '0%'}, 1000);
		//setTimeout(() => { $(this).hide(); }, 1000);	//짝이 있어야 동작
		setTimeout(function(){
			$('.bigPictureWrapper').hide();
		}, 1000);
	});
	
	var frm = $('#operForm');
	$("button[data-oper='modify']").click(function(e){
		frm.attr("action", "/memo/modify").submit();
	});
	$("button[data-oper='list']").click(function(e){
		frm.find("#memoNo").remove();
		frm.attr("action", "/memo/list").submit();
	});
	/* var memoNo = 500; memo.memoNo 없는번호*/
	var memoNo = '${mvo.memoNo}';
	var replyUL = $('.chat');
		
	
});//END showList

//모달 창 관련 처리----------------------------------------------------------------------

var modal = $(".modal");
var modalInputReply = modal.find("input[name='reply']");	//.find(); 어떤 요소의 하위 요소 중 특정 요소를 찾을 때 사용
var modalInputReplyer = modal.find("input[name='replyer']");
var modalInputReplyDate = modal.find("input[name='replyDate']");

var modalModBtn = $("#modalModBtn");
var modalRemoveBtn = $("#modalRemoveBtn");
var modalRegisterBtn = $("#modalRegisterBtn");

$("#addReplyBtn").on("click", function(e){
	modal.modal("show");	//모달창 보이기; 모달객체.modal("show")
	modalInputReply.val('');
	modalInputReplyer.val(replyer);	//replyer를 폼에 추가
	if(replyer==null){	//댓글 작성 버튼 클릭시 경고창 출력
		modalInputReply.attr('readonly', true);
	} else {
		modalInputReply.attr('readonly', false);
	}
	modalInputReplyDate.closest("div").hide();	//	$("li.item-a").closest("ul"); closest item-a 엘리멘트의 가장가까운 부모 요소 하나 선택
	modalRegisterBtn.show();
	modalModBtn.hide();
	modalRemoveBtn.hide();
});

modalRegisterBtn.on("click", function(e){
	if(replyer==null){	//로그인하지 않은 경우 작성 불가
		alert('로그인 후 작성 가능합니다.');
		modal.modal('hide');
		return;
	}
	replyService.add(	//댓글 추가
		{ memoNo:memoNo, reply:modalInputReply.val(), replyer:modalInputReplyer.val()},
		function(result){
			console.log('reply add result : ' + result);
			modal.modal("hide");	//모달 숨기기
			showList(1);			//댓글 받아오기(첫 페이지)
			showReplyPage(result.replyCnt);//pagenation 함수 호출
		})
});

$('.chat').on('click', 'li', function(e){	//.on( events [, selector] [, data] , handler(eventObject) )
	var replyNo = $(this).data("rno");
		replyService.get(replyNo, function(result){	//댓글 한개
			
		modalInputReply.val(result.reply);
		modalInputReplyer.val(result.replyer);
		modalInputReplyDate.val(replyService.displayTime(rvo.replyDate)).attr('readonly', 'readonly');
		modalModBtn.show();
		modalRemoveBtn.show();
		modalRegisterBtn.hide();
		modal.data("rno", replyNo);
		modal.modal("show");
	}, function(err){
		console.log(err);
	});
});

modalRemoveBtn.on("click", function(e){
	var replyNo = modal.data('rno');
	replyService.remove(replyNo,	//댓글 삭제
		modalInputReplyer.val(),
		function(result){
			alert('remove result : ' + result);
			modal.modal("hide");	//모달 숨기기
			showList(pageNum);			//댓글 받아오기
		}, function(err){
			alert('remove err : ' + err);
		});
	modal.modal("hide");	//모달 숨기기
});

//댓글 수정 버튼 이벤트 처리
modalModBtn.click(function(){
	//댓글 수정
	replyService.update(
		{ replyNo:modal.data('rno'), 
		  reply:modalInputReply.val(),
		  replyer:modalInputReplyer.val()},
		function(result){
			alert(result);
			modal.modal('hide');
			showList(pageNum);
		},
		function(err){
			console.log('reply update error');
		}
	);//END replyService.update()
});//END 댓글 수정 버튼 이벤트 처리

replyPageFooter.on("click", "li a", function(e){
	e.preventDefault();
	var targetPageNum = $(this).attr('href');
	pageNum = targetPageNum;
	showList(pageNum);
});
</script> -->

<%@ include file="../includes/footer.jsp" %>

