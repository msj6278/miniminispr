<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../includes/header.jsp" %>
<article id="battlesGet">
	<div class='container-fluid'>
		<div class="card text-center">
			<div class="card-header">
				<h5 id='title'></h5>
			</div>
			<div class="card-body">
				<div class='row no-gutters'>
					<div class='col-6 imgWindowContainer imgWindowContainer-xl imgWindowContainer-xxl  card leftImg' style='cursor:pointer'>
						<img class='card-img-bottom' src='/resources/img/noimage.png' style='width:100%'
							id="leftImg">
					</div>
					<div class='col-6 imgWindowContainer imgWindowContainer-xl imgWindowContainer-xxl card rightImg' style='cursor:pointer'>
						<img class='card-img-bottom' src='/resources/img/noimage.png' style='width:100%'
							id="rightImg">
					</div>
				</div>
				<span id="content"></span>
			</div>
		</div>
	</div>
	<script>
	$(function(){
		//init
		$(window).off('scroll');
		
		$('#additionalHeader').off().find('*').off();
		$('#additionalHeader').slideUp('fast');
		
		//이미지 클릭시 선호도 카운트++
		
		var url = new URL(location);
		var params  = url.searchParams;
		
		var csrfHeaderName = '${_csrf.headerName}';	
		var csrfTokenValue = '${_csrf.token}';
		
		$(document).ajaxSend(function(e, xhr, options){	//전송 전 추가 헤더 설정
			xhr.setRequestHeader(csrfHeaderName, csrfTokenValue);
		});
		
		getPage(parseInt(params.get("pageNum"))).then(function(result){
			return drawBattle(result);
		}).catch(function(reject){
			alert(reject);
		});
		
		//회원 목록 데이터 받아오기, 화면 출력 함수
		function getPage(battleNo){
			return new Promise((resolve, reject) => {
				var pathname = url.pathname;
				var battleNo = pathname.substring(pathname.lastIndexOf("/")+1);
				$.ajax({
					type		: 'get',
					url			: '/battles/'+battleNo,
					contentType : 'text/plain'
				}).done(function(result, status, xhr){
					if(xhr.status===204){
						alert('배틀이 존재하지 않습니다.');
						return;
					}
					resolve(result);
				}).fail(function(xhr, status){
					reject("회원 목록을 불러오지 못했습니다.");
				});//END ajax
			});
		}//END getPage()
		
		function drawBattle(result){
			return new Promise((resolve, reject) => {
				$('#title').text(result.title);
				$('#content').text(result.content);
				$('#leftImg').replaceWith("<img class='card-img-bottom' src='/display?fileName="
						+ encodeURIComponent(result.image1) +"' style='width:100%' id='leftImg' data-ori='image1'>");
				$('#rightImg').replaceWith("<img class='card-img-bottom' src='/display?fileName="
						+ encodeURIComponent(result.image2) +"' style='width:100%' id='rightImg' data-ori='image2'>");
				resolve();
			});
		}
		
		$('#battlesGet').on('mouseenter','#leftImg', function (event) {
			$(this).closest('.card').css({"transform":"scale(1.05)", "box-shadow":"2px 2px 3px darkgray", "z-index":"1000"});
			$('.rightImg').css({"transform":"scale(0.95)", "box-shadow":"", "z-index":"990"});
		}).on('mouseleave','#leftImg', function(){
			$('.leftImg').css({"transform":"scale(1)", "box-shadow":"", "z-index":"990"});
			$('.rightImg').css({"transform":"scale(1)", "box-shadow":"", "z-index":"990"});
		}).on('mouseenter','#rightImg', function (event) {
			$('.leftImg').css({"transform":"scale(0.95)", "box-shadow":"", "z-index":"990"});
			$(this).closest('.card').css({"transform":"scale(1.05)", "box-shadow":"2px 2px 3px darkgray", "z-index":"1000"});
		}).on('mouseleave','#rightImg',  function(){
			$('.leftImg').css({"transform":"scale(1)", "box-shadow":"", "z-index":"990"});
			$('.rightImg').css({"transform":"scale(1)", "box-shadow":"", "z-index":"990"});
		});
		
		$('#battlesGet').on('click', '#leftImg, #rightImg', function(){
			var battleNo = url.pathname.substring(url.pathname.lastIndexOf("/")+1)
			var data = {};
			var vote = $(this).data('ori');
			data = {battleNo:battleNo, vote:vote};
			
			$.ajax({
				type		: 'PUT',
				url			: '/battles/vote',
				data		: JSON.stringify(data),
				contentType : 'application/json'
			}).done(function(result, status, xhr){
				alert(result);
			}).fail(function(xhr, status, error){
				alert(xhr.responseText);
			});//END ajax
		});
	});
	</script>
</article>

<%@ include file="../includes/footer.jsp" %>

