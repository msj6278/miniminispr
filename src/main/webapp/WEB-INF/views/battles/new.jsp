<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../includes/header.jsp"%>
<script>
$('#additionalHeader').html('');
</script>
<article id="battlesNew">
	<div class='bigPictureWrapper'>
		<div class='bigPicture'></div>
	</div>
	
	<div class='container-fluid'>
		<div class="card">
			<div class="card-header text-center">
				<span class="font-weight-bold">배틀 등록 화면</span>
			</div>
			<div class="card-body">
				<form action="battles/new" method="post" role="registBattle">
					<div class="form-group">
						<label for="titleKey" class="font-weight-bold">배틀 제목</label>
						<div class="input-group">
							<input type="text" class="form-control" name="title"
								data-a="title" placeholder="배틀 제목을 입력해 주세요.">
						</div>
					</div>
					<div class="form-group">
						<label for="titleKey" class="font-weight-bold">이미지 업로드</label>
						<span>(업로드 순서와 관계없이 임의로 노출됩니다)</span>
						<div class="input-group">
							<div class="custom-file">
								<input type="file" class="custom-file-input" multiple>
								<label class="custom-file-label text-wrap" for="customFile">Choose file</label>
							</div>
						</div>
						<div id="dropZone">
							<select class="custom-select" size="3" id="fileSelect" multiple>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class='row no-gutters' id="imgWindowContainer">
							<div class='col-6 imgWindowContainer imgWindowContainer-xl imgWindowContainer-xxl card leftImg'>
								<img class='card-img-bottom' src='/resources/img/noimage.png' style='width:100%'
									id="leftImg">
							</div>
							<div class='col-6 imgWindowContainer-xl imgWindowContainer-xxl imgWindowContainer card rightImg'>
								<img class='card-img-bottom' src='/resources/img/noimage.png' style='width:100%'
									id="rightImg">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="titleKey" class="font-weight-bold">본문</label>
						<textarea rows="5" cols="30" name="content" id="content" class="form-control" placeholder="내용을 입력해 주세요."></textarea>
					</div>
					<div class="form-group">
						<div class="row no-gutters">
							<div class="col-12 input-group">
								<a href="javascript:void(0)" id="registBattle"
									class="btn btn-secondary btn-block" tabindex="-1"
									role="button" aria-disabled="true">배틀 등록</a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</article>

<script src="/resources/js/battles/new.js"></script>

<%@ include file="../includes/footer.jsp"%>