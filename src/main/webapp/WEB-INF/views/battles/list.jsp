<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../includes/header.jsp" %>
<script>	//header에 검색 메뉴 동적 생성
var battlesHeader = 
`<div id='battlesHeader' class='card'>
	<div class='card-header'>
		<div class='input-group'>
			<select name='page' class='custom-select'></select>
			<div class='input-group-append bg-white'>
				<button class='btn btn-outline-secondary' type='button' id='battlesCriteriaToggler'
					data-toggle='collapse' data-target='#collapseSearch' aria-controls='collapseSearch'>상세검색</button>
			</div>
		</div>
	</div>
	<!-- collapse menu; battle 상세 검색 메뉴 -->
	<div class="collapse" id="collapseSearch">
		<div class="card-body">
			<div class="form-group">
				<label for="userIdKey" class="font-weight-bold">작성자 ID로
					찾기</label>
				<div class="row no-gutters">
					<div class="col-12 input-group">
						<input type="text" class="form-control" id="userIdKey"
							data-a="userId" placeholder="ID를 입력해 주세요.">
						<div class="input-group-append">
							<div class="btn-group-toggle bg-white" data-toggle="button">
								<a href="#" class="btn btn-outline-secondary disabled"
									data-key="userId" data-type="criteria" tabindex="-1"
									role="button" aria-disabled="true">Checked</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="titleKey" class="font-weight-bold">제목으로 찾기</label>
				<div class="row no-gutters">
					<div class="col-12 input-group">
						<input type="text" class="form-control" id="titleKey"
							data-a="title" placeholder="제목을 입력해 주세요.">
						<div class="input-group-append">
							<div class="btn-group-toggle bg-white" data-toggle="button">
								<a href="#" class="btn btn-outline-secondary disabled"
									data-key="title" data-type="criteria" tabindex="-1"
									role="button" aria-disabled="true">Checked</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="contentKey" class="font-weight-bold">내용으로 찾기</label>
				<div class="row no-gutters">
					<div class="col-12 input-group">
						<input type="text" class="form-control" id="contentKey"
							data-a="content" placeholder="내용을 입력해 주세요.">
						<div class="input-group-append">
							<div class="btn-group-toggle bg-white" data-toggle="button">
								<a href="#" class="btn btn-outline-secondary disabled"
									data-key="content" data-type="criteria" tabindex="-1"
									role="button" aria-disabled="true">Checked</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="inputAddress" class="font-weight-bold">작성일로
					찾기</label>
				<div class="row">
					<div class="col-5 col-md-6 row no-gutters">
						<div class="col-12">
							<label for="startRegistDate">시작일</label>
						</div>
						<div class="col-12">
							<input type="date" id="startRegistDate" class="form-control"
								min="2020-01-01" data-a="registDate">
						</div>
					</div>
					<div class="col-7 col-md-6 row no-gutters">
						<div class="col-12">
							<label for="endRegistDate">종료일</label>
						</div>
						<div class="col-12 input-group">
							<input type="date" id="endRegistDate" class="form-control"
								min="2020-01-01" data-a="registDate">
							<div class="input-group-append">
								<div class="btn-group-toggle bg-white input-group-append"
									data-toggle="button">
									<a href="#" class="btn btn-outline-secondary disabled"
										data-key="registDate" data-type="criteria" tabindex="-1"
										role="button" aria-disabled="true">Checked</a>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
			<div class="form-group">
				<div class="row no-gutters">
					<div class="col-12 input-group">
						<a href="javascript:void(0)" id="searchBattles"
							class="btn btn-secondary btn-block" tabindex="-1"
							role="button" aria-disabled="true">검색 하기</a>
					</div>
				</div>
			</div>
		</div><!-- END card-body -->
	</div><!-- END collapse search -->
</div><!-- END battlesHeader -->`;
$('#additionalHeader').html(battlesHeader);
</script>	<!--END header에 검색 메뉴 동적 생성 -->

<!-- 배틀 목록 container -->
<article id="battlesList">
	<!-- battleList container -->
	<div class="container-fluid">
		<div class="card" id="battlesContainer">
			<div class="card-body"> 
				<div class="grid no-gutters">
					<div class="grid-sizer col-1"></div>
				</div>
			</div>
		</div>
	</div>
</article>

<script src="/resources/js/battles/list.js"></script>

<%@ include file="../includes/footer.jsp" %>