<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Preference Battle</title>

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
<script src="/resources/js/customjquery.js"></script>

<!-- lodash.js -->
<script src="https://cdn.jsdelivr.net/npm/lodash@4.17.4/lodash.min.js"></script>

<!-- bootstrap Core -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

<!-- bootstrap Core CSS -->
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">

<!-- masonry Layout; https://masonry.desandro.com/ -->
<script	src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
	
<!-- imagesloaded https://imagesloaded.desandro.com/-->
<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>

<!-- Sweet alert2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<!-- fake favicon; favicon request prevent(histroy API 조작시 favicon 요청; 크롬 브라우저 issue) -->
<link rel="icon" type="image/png" href="data:image/png;base64,iVBORw0KGgo=">

<!-- timeago.js; https://timeago.yarp.com/ -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timeago/1.6.7/jquery.timeago.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timeago/1.6.7/locales/jquery.timeago.ko.js"></script>

<!-- material components; https://material.io/-->
<link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined">

<!-- custom style -->
<link rel="stylesheet" type="text/css" href="/resources/css/style.css">

</head>
<body>
<div id="wrapper">
	<header class="sticky-top">
		<div class="container-fluid">
			<div class="card card-body bg-light">
				<nav class="navbar navbar-expand-md navbar-light">
					<%@ include file="nav.jsp" %>
				</nav>
			</div><!-- END card body; navbar 테두리 -->
		</div><!-- END navbar container-->
		
		<div class="container-fluid">
			<div id="additionalHeader"></div>
		</div>
	</header>
	<main>
		<div id="mainWrapper">