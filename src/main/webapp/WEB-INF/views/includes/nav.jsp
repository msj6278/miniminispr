<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<a class="navbar-brand font-weight-bold" href="/index">선호도 배틀</a>
<button class="navbar-toggler" type="button" id="navbarToggler"
	data-toggle="collapse" data-target="#navbarSupportedContent"
	aria-controls="navbarSupportedContent" aria-expanded="true"
	aria-label="Toggle navigation">
	<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarSupportedContent">
	<ul class="navbar-nav mr-auto">
		<sec:authorize access="!hasRole('ROLE_ADMIN')">
			<li class="nav-item" id="about"><a class="nav-link"
				href="index.jsp?page=/include/about">사이트소개</a></li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="myPage"
					role="button" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false">마이페이지</a>
				<div class="dropdown-menu" aria-labelledby="myPage">
					<a class="dropdown-item" href="/members/" id="userInfo">회원 정보</a>
					<a class="dropdown-item" href="/members/withdraw">회원 탈퇴</a>
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="#">배틀 참여 이력</a>
					<a class="dropdown-item" href="#">게시글 관리</a>
				</div>
			</li>
			<li class="nav-item dropdown"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">배틀 메뉴</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item" href="#">배틀 설명</a> <a
						class="dropdown-item" href="/battles/new">배틀 등록</a> <a
						class="dropdown-item" href="/battles">배틀 시작</a>
				</div></li>
			<li class="nav-item"><a class="nav-link" href="/memos">메모게시판</a></li>
		</sec:authorize>
		<sec:authorize access="hasRole('ROLE_ADMIN')">
			<li class="nav-item"><a class="nav-link" href="#" id="adminInfo">관리자 정보</a></li>
			<li class="nav-item"><a class="nav-link" href="/members">회원 관리</a></li>
			<li class="nav-item"><a class="nav-link" href="#" id="navbarDropdown">배틀 관리</a></li>
			<li class="nav-item"><a class="nav-link" href="#" id="memo">게시판 관리</a></li>
		</sec:authorize>
	</ul>
	<ul class="navbar-nav ml-auto">
		<li class="nav-item">
			<sec:authorize access="isAnonymous()">
				<a class="nav-link" href="/members/join">Join</a>
			</sec:authorize>
		</li>
		<li class="nav-item">
			<sec:authorize access="isAnonymous()">
				<a class="nav-link" href="/login">Login</a>
			</sec:authorize>
			<sec:authorize access="isAuthenticated()">
				<a class="nav-link" href="/logout">Logout</a>
			</sec:authorize>
		</li>
	</ul>
</div>