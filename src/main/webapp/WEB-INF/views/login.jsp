<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Bootstrap Admin Theme</title>

    <!-- Bootstrap Core CSS -->
    <link href="/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/resources/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/resources/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/resources/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<!-- Sweet alert2 -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	
<style>
#capsLockShow {
	height: 1em;
}
#index{
	font-size: large;
	font-weight: bold;
	cursor: pointer;
}
</style>
</head>

<body>

    <div class="container-fluid" id="#wrapper">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <span id="index">선호도 배틀</span>
                    </div>
                    <form name='f' action='/login' method='POST'>
	                    <div class="panel-body">
                            <fieldset>
                                <div class="form-group">
									<input class="form-control" placeholder="ID를 입력해 주세요."  name='username' id='username' autofocus>
                                </div>
                                <div class="form-group">
									<input class="form-control" placeholder="비밀번호를 입력해 주세요." name="password" id='password' type="password" value="">
									<div id='capsLockShow'></div>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name='remember-me'>Remember Me
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <input type="hidden" name="${_csrf.parameterName }" value="${_csrf.token }">
                            </fieldset>
	                    </div>
	                    <div class="panel-footer">
		                    <div class="container-fluid">
		                    	<div class="row">
		                    		<div class="col-xs-6">
										<input type="button" id="join" class="btn btn-default btn-block" value="회원 가입">
									</div>
									<div class="col-xs-6">
										<input type="button" id="login" class="btn btn-success btn-block" value="로그인">
									</div>
								</div>
							</div>
						</div>
					</form>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="/resources/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/resources/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="/resources/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="/resources/dist/js/sb-admin-2.js"></script>

<script>
$(function(){
	$(window).on('popstate', function() {
		location.replace(location.href);
	});
	
	<c:if test="${param.logout != null }">
		alert('로그아웃되었습니다.');
	</c:if>
	<c:if test="${error!=null}">
		Swal.fire({
			icon:  'error',
			title: '${error}'
		});
	</c:if>
	$("#login").click(function(e){
		e.preventDefault();
		$('form').submit();
	});

	$('#password').keydown(function(e){
		var capsLockMessage = '';
		if (event.getModifierState("CapsLock")) {
			capsLockMessage = '<Caps Lock> 켜진 상태입니다.';
		} else {
			capsLockMessage = '';
		}
		$('#capsLockShow').text(capsLockMessage);
	});
	
	$('#join').click(function(){
		location.href='/members/join';
	});
	
	$('#index').click(function(){
		location.href='/';
	});
});
</script>
</body>

</html>
