$(function(){
	var maxFileSize = 5242880;				//프로필 사진 최대 사이즈; 5MB 이하
	var maxFileNameLength = 20;				//프로필 사진 파일명 최대길이; 20자
	var patternImageFileType = /^image.*/;	//프로필 사진 타입; image
	
	var patternId1 = /[^a-z0-9]+/;
	var patternId2 = /[a-z]+/;
	var patternPassword1 = /[a-z]+/;
	var patternPassword2 = /[0-9]+/;
	var patternPassword3 = /[~․!@#$%^&*()_\-+={}[\]|\\;:‘“<>,.?/]+/;
	var patternPassword4 = /(?=.*[0-9].*)(?=.*[a-z].*)(?=.*[~․!@#$%^&*()_\-+={}[\]|\\;:‘“<>,.?/].*).{8,15}/;
	var patternUserName1 = /[^ㄱ-ㅎㅏ-ㅣ가-힣]$/;
	var patternUserName2 = /[^가-힣]$/;
	var patternEmail1 = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))$/;	//local-part 검사
	var patternEmail2 = /^((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;	//domain 검사
	
	var uploadedFile;	//업로드된 파일 정보 json
	
	var formData;
	var file;
	
	$('#indexBtn').click(function () {
		window.history.back();
	});
	
	//가입하기 클릭 처리
	$('#submitJoinForm').click(function(){
		inputIdCheck().then(function(){
			return inputPassword1Check();
		}).then(function(){
			return inputPassword2Check();
		}).then(function(){
			return inputUserNameCheck();
		}).then(function(){
			return inputEmail1Check();
		}).then(function(){
			return inputEmail2Check();
		}).then(function(){
			return inputGenderCheck();
		}).then(function(){
			return inputIntroCheck();
		}).then(function(){
			$("input[name='email']").val($('#email1').val()+"@"+$('#email2').val());
			if(uploadedFile){
				$("input[name='photo']").val(uploadedFile.uuid+"_"+uploadedFile.fileName);
			}
			formData = $("form[role='join']").serialize();
			//submit
			$.ajax({
				type		: 'POST',
				url			: '/members',
				data		: formData,
				dataType	: 'text'
			}).done(function(result){
				sendEmail(formData);
				var loginData = {username: $('#userId').val(), password: $('#password1').val()};
				//submit
				$.ajax({
					type		: 'POST',
					url			: '/login',
					data		: loginData
				}).done(function(){
					$('#wrapper').load('/members/joinVerify #wrapper', function(data){
						if (data && data != '') {
							history.pushState({}, null, '/members/joinVerify');
						}
					});
				}).fail(function(){
					$('#wrapper').load('/loginProc #wrapper');
				});
				
			}).fail(function(xhr, status){
				console.log(xhr.status+":"+xhr.responseText);
			});//END ajax
		}).catch(function(reject){
			$(reject.selector).focus().siblings('span').html(reject.reason);
		});//END catch
	});//END 가입하기 클릭 처리
	
	function sendEmail(formData){
		$.ajax({
			type		: 'POST',
			url			: '/members/sendJoinCode',
			data		: formData,
			dataType	: 'text',
			beforeSend	: function(){
				Swal.fire({
					title: '회원 인증 메일 발송',
					html: '인증 메일 발송 중..',
					onBeforeOpen: () => {
						Swal.showLoading();
					}
				});
			}
		}).done(function(result){
			Swal.close();
			Swal.fire({
				icon: 'success',
				title: result,
				showConfirmButton: false,
			});
		}).fail(function(xhr, status){
			Swal.close();
			Swal.fire({
				icon: 'error',
				title: xhr.responseText,
				showConfirmButton: false,
			});
			alert(xhr.responseText);
		});
	}
	
	//이메일 도메인 드롭다운 선택, 직접입력시에만 입력가능
	$('#email3').change(function(){
		var email3Val = $('#email3').val();
		if(email3Val != 'direct') {
			$('#email2').val(email3Val).attr('readonly', true);
		} else {
			$('#email2').val('').attr('readonly', false).focus();
		}
	});
	
	//ID 입력시 처리
	$('#userId').on('input', _.debounce(function(){
		inputIdCheck().then(function(resolve){
			document.getElementById("userIdCheckMessage").innerHTML = resolve;
		}).catch(function(reject){
			document.getElementById("userIdCheckMessage").innerHTML = reject.reason;
		});
	}, 500));//END ID 입력 처리
	
	function inputIdCheck(){
		return new Promise((resolve, reject) => {
			var inputIdVal = $('#userId').val();
			
			if(patternId1.test(inputIdVal)){
				reject({"selector":"#userId",
						"reason":"사용 불가능한 문자를 포함하고 있습니다."});
			} else if(inputIdVal.length > 10){
				reject({"selector":"#userId",
						"reason":"사용 가능한 길이를 초과했습니다."});
			} else if(inputIdVal.length < 1 | inputIdVal === "") {
				reject({"selector":"#userId",
						"reason":"필수 입력사항 입니다."});
			} else if(inputIdVal.length < 5){
				reject({"selector":"#userId",
						"reason":"영문 소문자, 숫자 사용가능 5~15자 이내"});
			} else if(!patternId2.test(inputIdVal)){
				reject({"selector":"#userId",
						"reason":"하나 이상의 영문 소문자를 반드시 포함해야 합니다."});
			} else {
				var data = {userId : inputIdVal}
				$.ajax({
					type	: 'get',
					url		: '/members/idCheck',
					data	: data,
					contentType	: 'text/plain'
				}).done(function(){
					resolve("사용가능한 아이디입니다.");
				}).fail(function(xhr, status, error){
					if(xhr.status==409){
					reject({"selector":"#userId",
							"reason":"이미 존재하는 아이디입니다."});
					} else {
					reject({"selector":"#userId",
							"reason":"서버에서 응답을 받지 못했습니다."});
					}
				});//END ajax()
			}//END else
		});
	}//END inputIdCheck()
	
	//비밀번호 입력 처리
	$('#password1').on('input', function(){
		inputPassword1Check().then(function(resolve){
			document.getElementById("password1CheckMessage").innerHTML = resolve;
		}).catch(function(reject){
			document.getElementById("password1CheckMessage").innerHTML = reject.reason;
		});
		inputPassword2Check().then(function(resolve){
			document.getElementById("password2CheckMessage").innerHTML = resolve;
		}).catch(function(reject){
			document.getElementById("password2CheckMessage").innerHTML = reject.reason;
		});
	});//END 비밀번호 입력 처리
	
	function inputPassword1Check(){
		return new Promise((resolve, reject) => {
			var inputPassword1Val = $('#password1').val();
			
			if(inputPassword1Val.length < 1 | inputPassword1Val === "") {
				reject({"selector":"#password1",
						"reason":"필수 입력사항 입니다."});
			} else if(inputPassword1Val.length < 8){
				reject({"selector":"#password1",
						"reason":"영문자, 숫자, 특수문자 조합 8 ~ 15자 이내"});
			} else if(inputPassword1Val.length > 15){
				reject({"selector":"#password1",
						"reason":"사용 가능한 길이를 초과했습니다."});
			} else if(!patternPassword1.test(inputPassword1Val)){
				reject({"selector":"#password1",
						"reason":"하나 이상의 영문 소문자를 포함해야 합니다."});
			} else if(!patternPassword2.test(inputPassword1Val)){
				reject({"selector":"#password1",
						"reason":"하나 이상의 숫자를 포함해야 합니다."});
			} else if(!patternPassword3.test(inputPassword1Val)){
				reject({"selector":"#password1",
						"reason":"하나 이상의 특수 문자를 포함해야 합니다."});
			} else {
				resolve("사용 가능한 비밀번호입니다.");
			}//END else
		});
	}//END inputPassword1Check()
	
	//비밀번호 확인 입력 처리
	$('#password2').on('input', function(){
		inputPassword2Check().then(function(resolve){
			document.getElementById("password2CheckMessage").innerHTML = resolve;
		}).catch(function(reject){
			document.getElementById("password2CheckMessage").innerHTML = reject.reason;
		});
	});//END 비밀번호 확인 입력 처리
	
	function inputPassword2Check(){
		return new Promise((resolve, reject) => {
			var inputPassword1Val = $('#password1').val();
			var inputPassword2Val = $('#password2').val();
			
			if(!patternPassword4.test(inputPassword1Val)) {
				reject({"selector":"#password2",
						"reason":"사용할 수 없는 비밀번호 입니다."});
			} else if(inputPassword2Val.length < 1 | inputPassword2Val === "") {
				reject({"selector":"#password2",
						"reason":"필수 입력사항 입니다."});
			} else if (inputPassword1Val === inputPassword2Val){
				console.log('b');
				resolve("비밀번호 일치");
			} else {
				reject({"selector":"#password2",
						"reason":"입력하신 비밀번호와 일치하지 않습니다."});
			}//END else
		});
	}//END inputPassword2Check()
	
	//유저 이름 입력시 처리
	$('#userName').on('input', function(){
		var inputNameVal = $('#userName').val();
		
		if(patternUserName1.test(inputNameVal)){
			document.getElementById("userNameCheckMessage").innerHTML = "한글만 입력 가능합니다.";
		} else if(inputNameVal.length < 1 | inputNameVal === "") {
			document.getElementById("userNameCheckMessage").innerHTML = "필수 입력사항 입니다.";
		} else if(inputNameVal.length > 7) {
			document.getElementById("userNameCheckMessage").innerHTML = "최대 7자 까지 입력 가능합니다.";
		} else {
			document.getElementById("userNameCheckMessage").innerHTML = "한글 이름 공백없이 2 ~ 7자";
		}
	});//END inputUserName()
	
	//유저 입력창 blur시 처리
	$('#userName').blur(function(){
		inputUserNameCheck().then(function(resolve){
			document.getElementById("userNameCheckMessage").innerHTML = resolve;
		}).catch(function(reject){
			document.getElementById("userNameCheckMessage").innerHTML = reject.reason;
		});
	});//END 유저 입력창 blur시 처리
	
	function inputUserNameCheck(){
		return new Promise((resolve, reject) => {
			var inputNameVal = $('#userName').val();
			
			if(patternUserName2.test(inputNameVal)){
				reject({"selector":"#userName",
						"reason":"잘못된 이름 입니다."});
			} else if(inputNameVal.length < 1 | inputNameVal === "") {
				reject({"selector":"#userName",
						"reason":"필수 입력사항 입니다."});
			} else if(inputNameVal.length > 7) {
				reject({"selector":"#userName",
						"reason":"최대 7자 까지 입력 가능합니다."});
			} else {
				resolve("사용가능한 이름입니다.");
			}
		});
	}

	//이메일 입력시 처리
	$('#email1, #email2').on('input', function(){
		Promise.all([inputEmail1Check(), inputEmail2Check()]).then(function(resolve) {
			document.getElementById("emailCheckMessage").innerHTML = resolve[0];
		}).catch(function(reject){
			document.getElementById("emailCheckMessage").innerHTML = reject.reason;
		});
	});//END 이메일 입력시 처리
	
	$('#email3').on('change', function(){
		Promise.all([inputEmail1Check(), inputEmail2Check()]).then(function(resolve) {
			document.getElementById("emailCheckMessage").innerHTML = resolve[0];
		}).catch(function(reject){
			document.getElementById("emailCheckMessage").innerHTML = reject.reason;
		});
	});//END 이메일 입력시 처리
	
	function inputEmail1Check(){
		return new Promise((resolve, reject) => {
			var inputEmail1Val = $('#email1').val();
			
			if(inputEmail1Val.length < 1 | inputEmail1Val === "") {
				reject({"selector":"#email1",
						"reason":"필수 입력사항 입니다."});
			} else if(!patternEmail1.test(inputEmail1Val)){
				reject({"selector":"#email1",
						"reason":"유효하지 않은 이메일 계정입니다."});
			} else if(inputEmail1Val.length > 64) {
				reject({"selector":"#email1",
						"reason":"최대 64자 까지 입력 가능합니다."});
			} else {
				resolve("유효한 이메일 주소입니다.");
			}
		});
	}
	
	function inputEmail2Check(){
		return new Promise((resolve, reject) => {
			var inputEmail2Val = $('#email2').val();
			
			if(inputEmail2Val.length < 1 | inputEmail2Val === "") {
				reject({"selector":"#email2",
						"reason":"도메인을 입력해 주세요."});
			} else if(!patternEmail2.test(inputEmail2Val)){
				reject({"selector":"#email2",
						"reason":"유효하지 않은 이메일 계정입니다."});
			} else if(inputEmail2Val.length > 255) {
				reject({"selector":"#email2",
						"reason":"최대 255자 까지 입력 가능합니다."});
			} else {
				resolve("유효한 이메일 주소입니다.");
			}
		});
	}
	
	//성별 선택 처리
	$("input[name='gender']").one('input', function(){
		document.getElementById("genderCheck").innerHTML = "";
	});//END 성별 선택 처리
	
	function inputGenderCheck(){
		return new Promise((resolve, reject) => {
			if($("input[name='gender']:checked").val()){
				resolve();
			} else {
				reject({"selector":"input[name='gender']",
						"reason":"필수 입력사항 입니다."});
			}
		});
	}//END inputGenderCheck()
	
	//자기소개 입력 처리
	$('#intro').on('input', function(){
		inputIntroCheck().then(function(resolve){
			document.getElementById("introCheckMessage").innerHTML = resolve;
		}).catch(function(reject){
			document.getElementById("introCheckMessage").innerHTML = reject.reason;
		});
	});//END 자기소개 입력 처리
	
	function inputIntroCheck(){
		return new Promise((resolve, reject) => {
			var inputIntroVal = $('#intro').val();
			
			if(inputIntroVal.length > 100){
				reject({"selector":"#intro",
						"reason":"최대 입력 글자 수 초과"});
			} else {
				resolve("최대 100자까지 입력 가능합니다.");
			}
		});
	}
	
	//업로드 파일 유효성 체크
	function uploadFileCheck(){
		return new Promise((resolve, reject) => {
			formData = new FormData();
			file = $("input[type='file']")[0].files[0];
			formData.append('uploadFile', file);
			
			if(file.name.lastIndexOf(".") > maxFileNameLength){	//파일명 20자를 초과하는 경우
				reject({"selector":"input[type='file']",
						"reason":"파일명 길이 제한 (최대 20자)"});
			} else if(file.size > maxFileSize){					//파일 크기가 5MB를 초과하는 경우
				reject({"selector":"input[type='file']",
						"reason":"파일 사이즈 초과 (최대 5MB)"});
			} else if(!patternImageFileType.test(file.type)){	//파일 타입이 image가 아닌 경우
				reject({"selector":"input[type='file']",
						"reason":"해당 종류의 파일은 업로드할 수 없습니다."});
			} else {
				resolve();
			}
		});
	}//END checkExetension()
	
	//프로필 업로드 결과 출력
	function showUploadedFile(result){
		var filePath = encodeURIComponent(result.uploadFolder+"/"+result.uuid+"_"+result.fileName);
		$('#photo').attr('src','/display?fileName='+filePath);
	}
	
	//프로필 사진 업로드
	$("input[type='file']").on("change", function(){
		uploadFileCheck().then(function(resolve){
			$.ajax({
				type		: 'POST',
				url			: '/uploadProfile',
				data		: formData,
				dataType	: 'json',
				contentType	: false,
				processData	: false,
				success		: function(result){
					deletePhoto(function(){
						Swal.fire({
		    				icon: 'success',
		    				title: '파일 업로드 완료',
		    				text: '프로필 사진이 업로드 되었습니다.'
			        	});
						showUploadedFile(result);
						$('input[name=photo]').val(result.uploadFolder+result.uuid+"_"+result.fileName);
						uploadedFile = result;
					});
				},
				error		: function(error) {
					Swal.fire({
	    				icon: 'error',
	    				title: '파일 업로드 오류',
	    				text: '프로필 사진 업로드 실패'
	    			});
					$("input[type='file']").val('');
					$("input[type='file']").val('');
					$('#photo').replaceWith('<img src="/resources/img/none.png" id="photo" width="200px" height="200px" alt="프로필 사진">');
				}
			});//END ajax()
		}).catch(function(reject){
			$("input[type='file']").val('');
			document.getElementById("uploadFileCheckMessage").innerHTML = reject.reason;
			$('#photo').replaceWith('<img src="/resources/img/none.png" id="photo" width="200px" height="200px" alt="프로필 사진">');
		});
	});//END 파일 선택 처리(프로필 사진 업로드)
	
	//기존 프로필 사진 파일 삭제
	function deletePhoto(callback){
		if(uploadedFile&&uploadedFile!==''){
			var targetFile = uploadedFile.uploadFolder+'/'+uploadedFile.uuid+'_'+uploadedFile.fileName;
			$.ajax({
				type	: 'POST',
				url		: '/deleteFile',	//memo 하위 경로라서 절대경로 변경 필요
				data	: { fileName:targetFile, type:null},
				dataType: 'text',
				success	: function(result){
					callback && callback();
				},
				error	: function(error){
					alert("error: "+error);
				}
			});//END ajax
		} else {
			callback && callback();
		}
	}//END deletePhoto()
	
	$('#delPhotoBtn').click(function(){
		if(!uploadedFile||uploadedFile===''){
			return;
		}
		Swal.fire({
			title: '프로필 사진을 삭제 하시겠습니까?',
			text:  "삭제한 파일은 복구 불가능합니다.",
			icon:  'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Change'
		}).then((result) => {
			if (result.value) {
				deletePhoto(function(){
					$('#photo').attr('src','/resources/img/none.png');
					uploadedFile = null;
					$('input[name=photo]').val('');
					$("input[type='file']").val('');
					Swal.fire(
						'파일 삭제 완료',
						'프로필 사진이 삭제되었습니다.',
						'success'
					);
				});
			}
		});
	});
	
	//회원가입 폼 제출 제외한 방법으로 페이지 나갈때 기존 업로드 프로필 사진 파일 삭제
	$(window).on("beforeunload", function(e){
		return deletePhoto();
	});
});