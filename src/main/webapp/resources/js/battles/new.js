$(function() {
	var maxFileSize = 5242880;				//프로필 사진 최대 사이즈; 5MB 이하
	var maxFileNameLength = 20;				//프로필 사진 파일명 최대길이; 20자
	var patternImageFileType = /^image.*/;	//프로필 사진 타입; image
	
	var uploadedFile;	//업로드된 파일 정보 json
	var formData;
	var file;
	
	var pattern = /(\b.|[ㄱ-ㅎ|ㅏ-ㅣ|가-힣])+/;
	
	$('#registBattle').click(function(){
		var title = $("input[name='title']");
		var titleVal = $("input[name='title']").val();
		var content = $("textarea[name='content']");
		var contentVal = $("textarea[name='content']").val();
		var options = $('#fileSelect option');
		var inputFile = $("input[type='file']");
		var reject = '';
		
		if(!titleVal){
			reject = '제목을 입력해 주세요.';
			title.focus();
		} else if(!pattern.test(titleVal)){
			reject = '공백, 특수문자를 제외한 문자가 반드시 필요합니다.'
			title.focus();
		} else if(options.length < 1){
			reject = '이미지를 등록해 주세요.'
			inputFile.focus();
		} else if(options.length < 2){
			reject = '나머지 이미지를 등록해주세요.'
			inputFile.focus();
		} else if(!contentVal){
			reject = '내용을 입력해 주세요.'
			content.focus();
		} else if(!pattern.test(contentVal)){
			reject = '공백, 특수문자를 제외한 문자가 반드시 필요합니다.'
			content.focus();
		} else {
			var formData = $("form[role='registBattle']").serializeArray();
			options.each(function(index){
				formData.push({name:'image'+ (index+1), value:$(this).data('file')});
			});
			$.ajax({
				type		: 'POST',
				url			: '/battles',
				data		: formData
	        }).done(function(result){
	        	$('#mainWrapper').load('/battles/'+ result.battleNo +' #mainWrapper');
        		history.pushState({}, null, '/battles/'+ result.battleNo);
        		Swal.fire({
    				icon: 'success',
    				title: '배틀 등록 성공',
    				text: '배틀이 등록되었습니다.'
    			});
	        }).fail(function(xhr, status, error){
	        	Swal.fire({
    				icon: 'error',
    				title: '배틀 등록 실패',
    				text: error
    			});
	        });//END ajax
	        return;
		}//END else
		Swal.fire({
			icon: 'error',
			title: '배틀 작성 오류',
			text: reject
		});
	});
	//페이지 이동시 초기화
	$(window).off('scroll');
	$('#additionalHeader').off().find('*').off();
	//브라우저 기본 drag&drop 이벤트 차단
	$('#battlesNew').on('dragover', function(e) {
		if(['INPUT', 'TEXTAREA'].indexOf($(e.target).prop('tagName')) < -1) {
			e.preventDefault();
		} else if($(e.target).prop('type')==='file'){
			if(e.originalEvent.dataTransfer.types.indexOf("Files") < 0){
				e.preventDefault();
			}
		} else {
			e.preventDefault();
		}
	}).on('drop', function(e) {	//dragover, drop 설정 둘다 필요, 묶지않고 선언해줘야 동작?
		if(['INPUT', 'TEXTAREA'].indexOf($(e.target).prop('tagName')) < -1) {
			e.preventDefault();
		} else if($(e.target).prop('type')==='file'){
			if(e.originalEvent.dataTransfer.types.indexOf("Files") < 0){
				e.preventDefault();
			}
		} else {
			e.preventDefault();
		}
	});
	
	$("#battlesNew").on('dragenter', "#fileSelect, input[type='file']", function(e){
		if(e.originalEvent.dataTransfer.types.indexOf("Files") < 0){
			return false;
		}
		$('#fileSelect option').css('pointer-events', 'none');
		$('#fileSelect').css({'box-shadow':'2px 2px 2px darkgray inset', 'background-color':'#f5f5f5'});
	}).on('dragleave', "#fileSelect, input[type='file']", function(e){
		$('#fileSelect option').css('pointer-events', 'auto');
		$('#fileSelect').css({"box-shadow":"", 'background-color':'#fff'});
	}).on('dragover', "#fileSelect, input[type='file']", function(e){
		e.stopPropagation();
		e.preventDefault();
	}).on('keyup', function(e){	//선택 option 파일 삭제 처리()
		if(e.keyCode == 46){
			var selectedOption = $('#fileSelect option:selected');
			selectedOption.each(function(){
				var selectedOptionVal = $(this).val();
				var targetImg = $("img[data-val='"+ selectedOptionVal +"']");
				deletePhoto(selectedOption).then(function(){
					selectedOption.remove();
					if($('#leftImg')[0]===targetImg[0]){	//객체 비교시 해당 노드만 선택되도록  get 혹은 javascript 객체로 비교
						$('#leftImg').replaceWith("<img class='card-img-bottom' src='/resources/img/noimage.png'"
								+ "' style='width:100%' id='leftImg'>");
					} else if($('#rightImg')[0]===targetImg[0]){
						$('#rightImg').replaceWith("<img class='card-img-bottom' src='/resources/img/noimage.png'"
								+ "' style='width:100%' id='rightImg'>");
					}
					$("input[type='file']").siblings('label').text('Choose file');
				}).catch(function(reject){
					if(reject) alert(reject);
				}).finally(function(){
					selectedOption.remove();
				});
			});
		}
	}).on('drop', "#fileSelect, input[type='file']", function(e){	//originalEvent; jQuery 객체로 반환
		e.stopPropagation();
		e.preventDefault();
		var files = e.originalEvent.dataTransfer.files;
		var formData = new FormData();
		
		validFileCheck(files).then(function(resolve){
			return appendFormData(files, formData);
		}).then(function(){
			return uploadFiles(files, formData);
		}).then(function(result){
			return addOptions(result);
		}).catch(function(reject){
			if(reject) alert(reject);
		}).finally(function(){
			$('#fileSelect option').css('pointer-events', 'auto');
			$('#fileSelect').css({"box-shadow":"", 'background-color':'#fff'});
		});
	});
	
	//이미지 업로드 ajax 결과값으로 option 추가
	function addOptions(result) {
		var optionLength = $('option').length;
		var i=1;
		for(res of result){
			var filePath = encodeURIComponent(res.uploadFolder+"/"+res.uuid+"_"+res.fileName);
			//낮은순으로 빈 번호 할당
			if($('#fileSelect option').length < 1){
				$('#fileSelect').prepend("<option value='"+ i +"' data-file='"+ filePath +"'>"
						+ i +". "+ res.fileName +"</option>");
				updateImg(filePath, i);
			} else {
				$('#fileSelect option').each(function(index){
					if(i==$(this).val()){
						i++;
						if($('#fileSelect option').length-1 <= index){
							$("#fileSelect option[value='"+(i-1)+"']").after("<option value='"+ i +"' data-file='"+ filePath +"'>"
									+ i +". "+ res.fileName +"</option>");
							updateImg(filePath, i);
						}
					} else if(i==1){
						$('#fileSelect').prepend("<option value='"+ i +"' data-file='"+ filePath +"'>"
								+ i +". "+ res.fileName +"</option>");
						updateImg(filePath, i);
						return false;
					} else {
						$("#fileSelect option[value='"+(i-1)+"']").after("<option value='"+ i +"' data-file='"+ filePath +"'>"
								+ i +". "+ res.fileName +"</option>");
						updateImg(filePath, i);
						return false;
					}
				});
			}
			i++;
		}
	}
	
	//img 영역 drag&drop 화면 효과 처리; img태그는 inset 적용 되지 않음
	$("#imgWindowContainer").on('dragenter', '#leftImg', function(e){
		if(e.originalEvent.dataTransfer.types.indexOf("Files") > -1){
			$('#leftImg').parent().css("box-shadow", "2px 2px 2px darkgray inset");
			$('#leftImg').css("padding", "2px 0 0 2px");
		}
	}).on('dragleave', '#leftImg', function(){
		$('#leftImg').parent().css("box-shadow", "");
		$('#leftImg').css("padding", "0");
	});
	$("#imgWindowContainer").on('dragenter', '#rightImg', function(e){
		if(e.originalEvent.dataTransfer.types.indexOf("Files") > -1){
			$('#rightImg').parent().css("box-shadow", "2px 2px 2px darkgray inset");
			$('#rightImg').css("padding", "2px 0 0 2px");
		}
	}).on('dragleave', '#rightImg', function(){
		$('#rightImg').parent().css("box-shadow", "");
		$('#rightImg').css("padding", "0");
	});
	
	
	$("#imgWindowContainer").on('dragover', function(e){
		e.stopPropagation();
		e.preventDefault();
	}).on('drop', function(e){	//originalEvent; jQuery 객체로 반환
		e.stopPropagation();
		e.preventDefault();
		var files = e.originalEvent.dataTransfer.files;
		var formData = new FormData();
		checkChangeImage(e).then(function(){
			return validFileCheck(files);
		}).then(function(resolve){
			return appendFormData(files, formData);
		}).then(function(){
			return uploadFiles(files, formData);
		}).then(function(result){
			return addOptions(result);
		}).catch(function(reject){
			if(reject) alert(reject);
		}).finally(function(){
			//$('#fileSelect option').css('pointer-events', 'auto');
			$('#imgWindowContainer > div').css({"box-shadow":"", 'background-color':'#fff'});
		});
	});
	
	//이미지에 drag&drop시 기존 업로드 이미지 교체 여부 확인
	function checkChangeImage(event){
		return new Promise((resolve, reject) => {
			if(event.originalEvent.dataTransfer.files.length===1
					&& $(event.target).data('val')){
				Swal.fire({
					title: '이미지를 교체 하시겠습니까?',
					text:  "기존 파일은 복구 불가능합니다.",
					icon:  'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Change'
				}).then((result) => {
					if (result.value) {
						$("option[value='"+ $(event.target).data('val') +"']").remove();
						if($('#leftImg')[0]===event.target){	//객체 비교시 해당 노드만 선택되도록 get 혹은 javascript 객체로 비교
							$('#leftImg').replaceWith("<img class='card-img-bottom' src='/resources/img/noimage.png'"
									+ "' style='width:100%' id='leftImg'>");
							resolve();
						} else if($('#rightImg')[0]===event.target){
							$('#rightImg').replaceWith("<img class='card-img-bottom' src='/resources/img/noimage.png'"
									+ "' style='width:100%' id='rightImg'>");
							resolve();
						}
						Swal.fire(
							'이미지 교체 완료',
							'성공적으로 교체 되었습니다.',
							'success'
						)
					} else {
						reject();
					}
				});
			} else {
				resolve();				
			}
		});
	}
	
	function updateImg(filePath, i){
		//data selector 최초 dom 생성시에만? replaceWith로 대체
		if($('#leftImg').data('left')){
			$('#rightImg').replaceWith("<img class='card-img-bottom' src='/display?fileName="
					+ filePath +"' style='width:100%' id='rightImg' data-right='true' data-val="+ i +">");
		} else {
			$('#leftImg').replaceWith("<img class='card-img-bottom' src='/display?fileName="
					+ filePath +"' style='width:100%' id='leftImg' data-left='true' data-val="+ i +">");
		}
	}
	
	//option(업로드 파일리스트) 선택시 처리
	$('#fileSelect').change(function(){
		var selectedOption = $('#fileSelect option:selected');
		var strBuffer = '';
		selectedOption.each(function(index){
			strBuffer +=  (strBuffer==''? '' : ' / ') + $(this).text();
		});
		$("input[type='file']").siblings('label').text(strBuffer);
	});
	
	//input file 파일선택시 처리
	$("input[type='file']").change(function(){
		var formData = new FormData();
		var files = $("input[type='file']")[0].files;	//jQueryObject[0]; javascript 객체 반환
		
		validFileCheck(files).then(function(resolve){
			return appendFormData(files, formData);
		}).then(function(){
			return uploadFiles(files, formData);
		}).then(function(result){
			addOptions(result)
		}).catch(function(reject){
			if(reject) alert(reject);
		});
	});
	
	//기존 프로필 사진 파일 삭제
	function deletePhoto(){
		return new Promise((resolve, reject) => {
			if(uploadedFile&&uploadedFile!==''){
				var targetFile = uploadedFile.uploadFolder+'/'+uploadedFile.uuid+'_'+uploadedFile.fileName;
				$.ajax({
					type	: 'POST',
					url		: '/deleteFile',	//memo 하위 경로라서 절대경로 변경 필요
					data	: { fileName:targetFile, type:null},
					dataType: 'text'
				}).done(function(result){
					resolve(result);
				}).fail(function(xhr, status, error){
					resolve();//스케쥴러로 일별 이미지 삭제
				});//END ajax
			} else {
				resolve();
			}
		});
	}//END deletePhoto()
	
	//업로드 파일 유효성 체크
	function validFileCheck(files){
		return new Promise((resolve, reject) => {
			if(!files || files.length<1){
				reject();
			} else if($('#fileSelect option').length + files.length > 2){
				reject("등록 가능한 파일 개수를 초과했습니다.");
			}
			for(file of files){
				if(!patternImageFileType.test(file.type)){	//파일 타입이 image가 아닌 경우
					reject("해당 종류의 파일은 업로드할 수 없습니다.");
				} else if(file.size > maxFileSize){			//파일 크기가 5MB를 초과하는 경우
					reject("파일 사이즈 초과 (최대 5MB)");
				} else if(file.name.lastIndexOf(".") > maxFileNameLength){	//파일명 20자를 초과하는 경우
					reject("파일명 길이 제한 (최대 20자)");
				} else {
					if(file === files[files.length -1]){	//마지막 루프에서 전체 파일의 유효성 반환
						resolve();
					}
				}//END else
			}//END for
		});//END promise
	}//END checkExetension()
	
	//파일 업로드
	function uploadFiles(files, formData){
		return new Promise((resolve, reject) => {
			$.ajax({
				type		: 'POST',
				url			: '/uploadFile',
				data		: formData,
				dataType	: 'json',
				contentType	: false,
				processData	: false
			}).done(function(result){
				resolve(result);
			}).fail(function(xhr, status, error){
				reject(error);
			});//END ajax
		});//END promise
	}//END uploadFile
	
	//파일 배열 formData 등록
	function appendFormData(files, formData){
		return new Promise((resolve, reject) => {
			for(file of files){
				formData.append('uploadFile', file);
			}
			resolve("append end");
		});//END promise
	}//END selectFiles
	
	//원본 이미지(전체화면 이미지) 클릭 이벤트 처리
	$('.bigPictureWrapper').click(function(){
		$('.bigPicture').animate({width:'0%', height: '0%'}, 1000);
		setTimeout(function(){
			$('.bigPictureWrapper').hide();
		}, 1000);
	});
	
	function showImage(filePath){	//블럭안 지역메서드 내부에서만 처리 가능
		//$('#leftImg').attr('src','/display?fileName='+($(this).data('origin')));
		$('.bigPictureWrapper').css('display', 'flex').show();
		$('.bigPicture').html('<img src="/display?fileName=' + filePath +'">')
						.animate({width:'100%', height:'100%'}, 1000);	// ajax 처리, 업로드 경로는 display매핑된 getFile에서 추가
	}
	
	$('#frmSubmit').click(function(){
		var titleVal = $('#title').val();
		var contentVal = $('#content').val();
		
		if(!titleVal || titleVal.length < 1) {
			$('#title').focus();
			alert('제목을 입력해 주세요.');
		} else if(!contentVal || contentVal.length < 1) {
			$('#content').focus();
			alert('내용을 입력해 주세요.');
		} else if($('option').length < 2){
			$("input[type='file']").focus();
			alert('이미지를 등록해 주세요.');
		} else {
			var data = {};
			var options = $('option');
			var i = 0;
			
			data["title"] = titleVal;
			data["content"]   = contentVal;
			
			$('option').each(function(index){
				data['image'+ (index+1)] = $(this).data('uploadfolder') + "\\" + $(this).data('uuid') + $(this).val();
			});
			$.ajax({
				type		: 'POST',
				url			: '/battles',
				data		: JSON.stringify(data),
				contentType : "application/json"
	        }).done(function(result){
        		//$('#wrapper').load('/members/joinVerify #wrapper');
	        }).fail(function(xhr, status){
	        	console.log(xhr.status+":"+xhr.responseText);
	        });
		}
	});
});//END $
