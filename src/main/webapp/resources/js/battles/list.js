$(function(){
	//페이지 이동시 초기화
	$(window).off('scroll');
	$(window).on('scroll', _.debounce(function() {
		//스크롤 도달시 리스트 불러오기, 표시하기
		if($(window).scrollTop() >= ($(document).height() - $(window).height() -1)) {
			if(realEndPage && realEndPage <= parseInt(params.get("pageNum"))){
				if(selectGetPageSyncFlag==false){	//selectGetPage와 동기화
					debouncedSetPageNum();	//마지막 페이지 이벤트 트리거
				}
			} 
			debouncedGetPage();
		}
		if(selectGetPageSyncFlag==false){
			debouncedSetPageNum();	//마지막 페이지 트리거 발생하지 않음
		}
	}, 50));//END 스크롤 이벤트 처리

	//masonry init
	$('.grid').imagesLoaded(function(){
		$('.grid').masonry({
			itemSelector: '.grid-item',
			columnWidth: '.grid-sizer',
			percentPosition: true
		});
	});
	var moveTargetPageNum;
	
	//페이지 선택 메뉴 hover
	$('#battlesHeader').mouseenter(function() {
		$(this).css({"transform":"scale(1.001)", "box-shadow":"2px 2px 3px darkgray"}); 
	}).mouseleave(function(){ 
		$(this).css({"transform":"scale(1)", "box-shadow":""}); 
	}); 
	
	//battles 목록 hover
	$('#battlesContainer').on('mouseenter','.card', function() {
		$(this).css({"transform":"scale(1.01)", "box-shadow":"2px 2px 3px darkgray", "z-index":"1000"});
	}).on('mouseleave','.card',  function(){
		$(this).css({"transform":"scale(1)", "box-shadow":"", "z-index":"990"});
	});
	
	//선택 배틀 상세보기
	$('#battlesContainer').on('click', "*[data-role='getBattle']", function(e){
		e.preventDefault();
		var battleNo = $(this).data('battleno');
		$('#mainWrapper').load('/battles/'+battleNo+' #mainWrapper', function(){
			//battlesHeader에 생성된 컨텐츠, 해당 뷰에서 bind된 이벤트 제거
			history.pushState({}, null, '/battles/'+battleNo+'?'+params);
		});
	});//END 선택 배틀 상세보기
	
	$('#beginDate').val(dateToString());
	$('#endDate').val(dateToString());
	
	$("input[type='date']").attr('min', '2020-01-01');
	
	var url = new URL(location);
	var params  = url.searchParams;
	
	var realEndPage;
	
	//배틀 목록 추가시 스크롤 이벤트 방지 true: 실행 불가, false: 실행 대기  
	var selectGetPageSyncFlag = false;
	
	var selectGetSyncFlag = false;
	
	//초기화면 배틀목록 불러오기, 표시, 검색옵션 초기화
	criteriaData(parseInt(params.get("pageNum"))).then(function(data){
		return getPage(data);
	}).then(function(result){
		return drawBattleList(result, 'search', function(pageMaker){
			setParams(pageMaker, function(){
				history.replaceState({}, null, url.pathname+'?'+params);
				initKeyword();
				
				//resize 트리거 발생; masonry 초기 정렬 issue(이미지 로딩전 사이즈로 정렬, 리사이즈 이벤트 발생시 재정렬)
				window.dispatchEvent(new Event('resize'));
			});
		});
	}).catch(function(reject){
		if(reject)		alert(reject);
	});//END 초기화면 배틀목록 불러오기, 표시, 검색옵션 초기화
	
	//쿼리스트링 검색조건으로 검색옵션 초기화
	function initKeyword(){
		var userIdKeyVal  = params.get("userId");
		var titleKeyVal   = params.get("title");
		var contentKeyVal = params.get("content");
		var startRegistDateVal = params.get("startRegistDate");
		var endRegistDateVal   = params.get("endRegistDate");
		
		//파라미터 값 입력, 체크 버튼 활성화
		if(userIdKeyVal){
			$('#userIdKey').val(userIdKeyVal);
			$("a[data-key='userId']").removeClass('disabled').addClass('active');
			$('#battlesCriteriaToggler').addClass('active');
		}
		if(titleKeyVal){
			$('#titleKey').val(titleKeyVal);
			$("a[data-key='title']").removeClass('disabled').addClass('active');
			$('#battlesCriteriaToggler').addClass('active');
		}
		if(contentKeyVal){
			$('#contentKey').val(contentKeyVal);
			$("a[data-key='content']").removeClass('disabled').addClass('active');
			$('#battlesCriteriaToggler').addClass('active');
		}
		if(startRegistDateVal){
			$('#startRegistDate').val(startRegistDateVal);
			$("a[data-key='registDate']").removeClass('disabled').addClass('active');
			$('#battlesCriteriaToggler').addClass('active');
		}
		if(endRegistDateVal){
			$('#endRegistDate').val(endRegistDateVal);
			$("a[data-key='registDate']").removeClass('disabled').addClass('active');
			$('#battlesCriteriaToggler').addClass('active');
		}
	}
	
	//배틀 목록 검색 옵션 이벤트 처리
	$('#collapseSearch').on('input', 'input[type=text]', function(){
 		var inputVal = $(this).val();
		var selectA = $("a[data-key='"+ $(this).data("a") +"']");
		if(inputVal){
			selectA.removeClass('disabled').addClass('active');
		} else {
			selectA.removeClass('active').addClass('disabled');
		}
	}).on('change', 'input[type=date]', function(){
		var startRegistDateVal = $("#startRegistDate").val();
		var endRegistDateVal   = $("#endRegistDate").val();
		var startUpdateDateVal = $("#startUpdateDate").val();
		var endUpdateDateVal   = $("#endUpdateDate").val();
		var selectA = $("a[data-key='"+ $(this).data("a") +"']");
		updateMaxDate();
		
		switch($(this).attr('id')){
		case 'startRegistDate':
			if(endRegistDateVal
					&& stringTodate(startRegistDateVal) > stringTodate(endRegistDateVal)){
				$("#startRegistDate").val(endRegistDateVal);
				alert('검색 시작일 보다 종료일이 빠릅니다.');
			}
			break;
		case 'endRegistDate':
			if(startRegistDateVal
					&& stringTodate(startRegistDateVal) > stringTodate(endRegistDateVal)){
				$("#endRegistDate").val(startRegistDateVal);
				alert('검색 시작일 보다 종료일이 빠릅니다.');
			}
			break;
		}
		
		if(startRegistDateVal || endRegistDateVal){
			selectA.removeClass('disabled').addClass('active');
			$('#searchBattles').removeClass('disabled');
		} else {
			selectA.removeClass('active').addClass('disabled');
			if(!$('#collapseSearch a').hasClass("active")){
				$('#searchBattles').addClass('disabled');
			}
		}
	}).on('click', '#searchBattles', _.debounce(function(){	//검색하기 클릭 처리
		criteriaData(1, true).then(function(data){
			return getPage(data);
		}).then(function(result){
			return drawBattleList(result, 'search', function(pageMaker){
				setParams(pageMaker, function(){
					updateCollapseSearch();
					history.pushState({}, null, url.pathname+'?'+params);
				});
			});
		}).catch(function(reject){
			alert(reject);
		});
	}, 500)).on('click', "a[data-type='criteria']", function(e){	//검색 옵션 check 클릭 처리
		e.preventDefault();
	});//END #collapseSearch event
	
	//criteriaData return
	function criteriaData(page, searchFlag) {
		return new Promise((resolve, reject) => {
			if(page<1 || (page>realEndPage)){
				reject();
			} else if(!searchFlag){	//검색하기 이벤트가 아닐때 처리
				var pageNum  = page;
				var amount   = params.get("amount");
				var userIdKeyVal  = params.get("userId");
				var titleKeyVal   = params.get("title");
				var contentKeyVal = params.get("content");
				var startRegistDateVal = params.get("startRegistDate");
				var endRegistDateVal   = params.get("endRegistDate");
				
				if(!pageNum)			pageNum = 1;
				if(!amount)				amount  = 12;
				if(!userIdKeyVal)		userIdKeyVal = "";
				if(!titleKeyVal)		titleKeyVal = "";
				if(!contentKeyVal)		contentKeyVal = "";
				
				//amount 고정
				var data = {pageNum:pageNum, amount:amount,
							userId : userIdKeyVal,
							title  :  titleKeyVal,
							content:contentKeyVal};
	
				//date형 파라미터 동적 추가(controller에서 date=null일때 패싱 피하기 위함); date값 프로퍼티 형식으로 key값 설정
				if(startRegistDateVal)	data["startRegistDate"] = startRegistDateVal;
				if(endRegistDateVal)	data["endRegistDate"]   =   endRegistDateVal;
				
				resolve(data);
			} else {	//검색하기 클릭; page=1, criteria 선택값으로 변경
				var userIdKeyChecked  = $("a[data-key='userId']").hasClass('active');
				var titleKeyChecked   = $("a[data-key='title']").hasClass('active');
				var contentKeyChecked = $("a[data-key='content']").hasClass('active');
				var registDateChecked = $("a[data-key='registDate']").hasClass('active');
				
				var userIdKeyVal  = $("#userIdKey").val();
				var titleKeyVal   = $("#titleKey").val();
				var contentKeyVal = $("#contentKey").val();
				
				if(!userIdKeyVal  || !userIdKeyChecked)		 userIdKeyVal = "";
				if(!titleKeyVal   || !titleKeyChecked)		  titleKeyVal = "";
				if(!contentKeyVal || !contentKeyChecked)	contentKeyVal = "";
				
				var startRegistDateVal = $("#startRegistDate").val();
				var endRegistDateVal   = $("#endRegistDate").val();
	
				var registDateChecked  = $("a[data-key='registDate']").hasClass('active');
				
				//page=1, amount 고정
				var data = {pageNum:'1', amount:'6',
							userId : userIdKeyVal,
							title  :  titleKeyVal,
							content:contentKeyVal};
	
				//date형 파라미터 동적 추가(controller에서 date=null일때 패싱 피하기 위함); date값 프로퍼티 형식으로 key값 설정
				if(startRegistDateVal && registDateChecked)  data["startRegistDate"] = startRegistDateVal;
				if(endRegistDateVal   && registDateChecked)  data["endRegistDate"]   =   endRegistDateVal;
				
				resolve(data);
			}
		});
	}
	
	function updateCollapseSearch(){
		var userIdKeyVal  = params.get("userId");
		var titleKeyVal   = params.get("title");
		var contentKeyVal = params.get("content");
		var startRegistDateVal = params.get("startRegistDate");
		var endRegistDateVal   = params.get("endRegistDate");
		
		if(userIdKeyVal || titleKeyVal || contentKeyVal || startRegistDateVal || endRegistDateVal){
			$('#battlesCriteriaToggler').addClass('active');
		} else {
			$('#battlesCriteriaToggler').removeClass('active');
		}
	}
	
	//회원 목록 데이터 받아오기, 화면 출력 함수
	function getPage(criteriaData){
		return new Promise((resolve, reject) => {
			$.ajax({
				type		: 'get',
				url			: '/battles',
				data		: criteriaData,
				contentType : 'application/json'
			}).done(function(result, status, xhr){
				if(xhr.status===204){
					alert('배틀이 존재하지 않습니다.');
					return;
				}
				realEndPage = result.pageMaker.realEndPage;
				resolve(result);
			}).fail(function(xhr, status){
				reject("회원 목록을 불러오지 못했습니다.");
			});//END ajax
		});
	}//END getPage()
	
	function drawBattleList(result, event, callback){
		var battleList = result.battleList;
		var pageMaker = result.pageMaker;
		//DOM 동적 생성 문자열 버퍼
		var battleStr = '';
		var pageMakerStr = '';
		
		var date = new Date();
		var i=0;
		//배틀 리스트 출력
		for(battle of battleList){
			var registDate = new Date(battle.registDate);
			var dateFlag = (date - registDate) < 86400000;
			var thumbnail1;
			var thumbnail2;
			if(Math.random() < 0.5){
				thumbnail1 = battle.image1.substring(0, battle.image1.lastIndexOf('/')+1)+'s_'+battle.image1.substring(battle.image1.lastIndexOf('/')+1);
				thumbnail2 = battle.image2.substring(0, battle.image2.lastIndexOf('/')+1)+'s_'+battle.image2.substring(battle.image2.lastIndexOf('/')+1);
			} else {
				thumbnail1 = battle.image2.substring(0, battle.image2.lastIndexOf('/')+1)+'s_'+battle.image2.substring(battle.image2.lastIndexOf('/')+1);
				thumbnail2 = battle.image1.substring(0, battle.image1.lastIndexOf('/')+1)+'s_'+battle.image1.substring(battle.image1.lastIndexOf('/')+1);
			}
			battleStr += "<div class='grid-item col-12 col-sm-12 col-md-6 col-lg-6 col-xl-4 col-xxl-3'>"
						+	"<div class='grid-item-content'>"
						+		"<div class='card battleCardHover' data-page='"+ pageMaker.cri.pageNum +"' "+ (i==0?"data-head='head'":"") +">"
						+			"<div class='card-header'>"
						+				"<a href='#' class='text-secondary text-decoration-none font-weight-bold' data-role='getBattle' "
						+					"data-battleno='"+ battle.battleNo +"'>"+ battle.title +"</a>"
						+				(dateFlag?	/*1day=86400000milliseconds*/
										"<i class='material-icons-outlined material-icons-outlined-fiber_new'>fiber_new</i>":"")
						+			"</div>"
						+			"<div class='row no-gutters' style='cursor:pointer'>"
						+				"<div class='col-6 imgContainer'>"
						+					"<img class='card-img-bottom' src='/display?fileName="+ encodeURIComponent(thumbnail1) 
						+							"' style='width:100%' data-battleno='"+ battle.battleNo +"' data-role='getBattle'>"
						+				"</div>"//END col-6 div
						+				"<div class='col-6 imgContainer'>"
						+					"<img class='card-img-bottom' src='/display?fileName="+ encodeURIComponent(thumbnail2) 
						+							"' style='width:100%' data-battleno='"+ battle.battleNo +"' data-role='getBattle'>"
						+				"</div>"//END col-6 div
						+			"</div>"//END row
						+			"<div class='card-body'>"+ battle.content +"</div>"
						+			"<div class='card-footer bg-light'>"
						+				"<a href='#' class='text-secondary text-decoration-none'>"+ battle.userId +"</a>"
						+				"<div><span class='text-dark'>조회수 "+ battle.viewCount +"<span>"
						+					"<span class='pull-right "+ (dateFlag?" text-dark":" text-muted") +"'>"
						+						$.timeago(battle.registDate) +"</span>"
						+				"</div>"
						+			"</div>"
						+		"</div>"//END card
						+	"</div>"//END subCardContainer
						+"</div>";
			i++;
		}
		
		var $items = $(battleStr);
		
		var optionCurrentPageNum = $("option[value='"+ pageMaker.cri.pageNum +"']");
		
		if(event=='scroll'){
			$('.grid').imagesLoaded(function(){
				$('.grid').masonry({
					itemSelector: '.grid-item',
					columnWidth: '.grid-sizer',
					percentPosition: true
				});
				$('.grid').append($items).masonry('appended', $items).masonry('reloadItems');					
			});
			
			optionCurrentPageNum.attr('class', '');
		} else {
			var body = $("html, body");
			body.scrollTop(0);
			var selected = '';
			var textColor = '';
			
			var dropboxPageMakerStr;
			
			$(".grid-item").remove();
			$('.grid').imagesLoaded(function(){
				$('.grid').masonry({
					itemSelector: '.grid-item',
					columnWidth: '.grid-sizer',
					percentPosition: true
				});
				$('.grid').prepend($items).masonry('prepended', $items).masonry('reloadItems');					
			});
			
			//dropdown pagination
			for(var i=1; i<=pageMaker.realEndPage; i++){
				if(pageMaker.cri.pageNum==i){
					selected = "selected='selected'";
					textColor = "";
				} else {
					selected = "";
					textColor = "class='text-warning'";
				}
				dropboxPageMakerStr += "<option value='"+i+"' "
									+			selected + textColor
									+ ">page "+ i +"</option>";
			}
			$("select[name='page']").html(dropboxPageMakerStr);
		}
		callback(pageMaker);
	}
	
	function setParams(pageMaker, callback){
		//location객체 url에 실행 쿼리스트링 set
		var pageNum = pageMaker.cri.pageNum;
		var amount  = pageMaker.cri.amount;
		var userIdKeyVal  = pageMaker.cri.userId;
		var titleKeyVal   = pageMaker.cri.title;
		var contentKeyVal = pageMaker.cri.content;
		
		if(!pageNum)		pageNum	= 1;
		if(!amount)			amount  = 6;
		if(!userIdKeyVal)	userIdKeyVal  = "";
		if(!titleKeyVal)	titleKeyVal   = "";
		if(!contentKeyVal)	contentKeyVal = "";
		
		params.set("pageNum", pageMaker.cri.pageNum);
		params.set("amount",  pageMaker.cri.amount);
		params.set("userId",  pageMaker.cri.userId);
		params.set("title",	  pageMaker.cri.title);
		params.set("content", pageMaker.cri.content);
		
		if(pageMaker.cri.startRegistDate){
			params.set("startRegistDate", dateToString(pageMaker.cri.startRegistDate));
		} else {
			params.delete("startRegistDate");
		}
		if(pageMaker.cri.endRegistDate){
			params.set("endRegistDate",   dateToString(pageMaker.cri.endRegistDate));
		} else {
			params.delete("endRegistDate");
		}
		callback();
	}
	
	//배틀 목록 가져오기; select page 이벤트
	var debouncedSelectGetPage = _.debounce(function(selectVal, biasHeight){
		selectGetSyncFlag = true;
		criteriaData(selectVal).then(function(data){
			return getPage(data); 
		}).then(function(result){
			return drawBattleList(result, 'search', function(pageMaker){
				var selectVal = $("select[name='page']").val();
				var divPage = $("div[data-page='"+selectVal+"']");
				if(divPage.length > 0){
					var offsetTop = divPage.first().offset().top - biasHeight - divPage.height()*0.5;
					var body = $("html, body");
					body.animate({scrollTop:offsetTop}, 500, 'swing');
				}
				debouncedSelectGetSync();
				setParams(pageMaker, function(){
					history.pushState({}, null, url.pathname+'?'+params);
				});//END setParams()
			});//END return
		}).catch(function(reject){
			debouncedSelectGetSync();
			if(reject)		alert(reject);
		});//END getPage()
	}, 500);
	
	//페이지 선택시 동기화 flag
	var debouncedSelectGetSync = _.debounce(function(){
		selectGetSyncFlag = false;
	}, 500);
	
	//배틀 목록 가져오기; 스크롤 이벤트
	var debouncedGetPage = _.debounce(function(){
		criteriaData(parseInt(params.get("pageNum"))+1).then(function(data){
			return getPage(data);
		}).then(function(result){
			return drawBattleList(result, 'scroll', function(pageMaker){
				setParams(pageMaker, function(){
					history.replaceState({}, null, url.pathname+'?'+params);
				});//END setParams()
			});//END return
		}).catch(function(reject){
			if(reject)		alert(reject);
		}).finally(function(){
			return;
		});//END getPage()
	}, 500);
	
	//페이지 선택 처리
	$('#battlesHeader').on('change', "select[name='page']", function(e){
		e.stopImmediatePropagation();
		var body = $("html, body");
		body.stop();
		var selectVal = $(this).val();
		var endPage = $("select[name='page'] > option").last().val();
		var biasHeight = $('main').first().position().top;
		var optionCurrentPageNum = $("option[value='"+ selectVal +"']");
		
		//선택한 페이지가 이미 존재하는 경우 해당 페이지로 스크롤 이동
		if(!optionCurrentPageNum.attr('class')){
			moveTargetPageNum = selectVal;
			debouncedMovePage(selectVal, biasHeight);
		} else {
			debouncedSelectGetPage(selectVal, biasHeight);
		}//END else
	});
	
	//(기존)페이지 선택시 해당 위치로 이동 
	var debouncedMovePage = _.debounce(function(selectVal, biasHeight){
		var body = $("html, body");
		var divPage = $("div[data-page='"+selectVal+"']");
		if(divPage.length<1){
			return;
		}
		var offsetTop = divPage.first().offset().top - biasHeight;
		//animate 재호출시 실행중인 animate 즉시 중지
		body.stop().animate({scrollTop:offsetTop}, 300, 'swing', function(){
		});
	}, 500);
	
	//스크롤 위치에따라 페이지값, url 쿼리스트링 변경
	var debouncedSetPageNum = _.debounce(function(){
		var pageScrollTop = new Map();
		var biasHeight = $('main').first().position().top;
		var viewPageNum;
		var pageHeadDiv = $("div[data-head='head']");
		//각 페이지 첫 박스 top 위치와 비교 
		pageHeadDiv.each(function(index){
			var biasHeight = $('main').first().position().top;
			var headOffsetTop = $(this).offset().top - biasHeight;
			if($(window).scrollTop() >= headOffsetTop - biasHeight){
				viewPageNum = ($(this).data('page'));
				return;
			}
		});
		if(selectGetSyncFlag==false){
			$("select[name='page']").val(viewPageNum);
			params.set("pageNum", viewPageNum);
			history.replaceState({}, null, url.pathname+'?'+params);
		}
	}, 50);
	
});