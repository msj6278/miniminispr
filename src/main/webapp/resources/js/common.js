$(function() {
	$(document).ajaxSend(function(e, xhr, options){	//전송 전 추가 헤더 설정
		xhr.setRequestHeader(csrfHeaderName, csrfTokenValue);
	});
	
	//nav 메뉴 클릭 처리
	$('nav').on("click", 'a', function(e) {
		e.preventDefault();
		var href = $(this).attr('href');
		switch (href) {
		case '/logout':
			Swal.fire({
				title: '로그아웃 하시겠습니까?',
				icon:  'question',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Logout'
			}).then((result) => {
				if(result.value) {
					$.ajax({
						type		: 'POST',
						url			: '/logout'
					}).done(function(){
						Swal.fire({
		    				icon: 'success',
		    				title: '로그아웃 되었습니다.'
		    			}).then(function(){
		    				location.replace('/');
		    			});
					}).fail(function(){
						Swal.fire({
		    				icon: 'error',
		    				title: '로그아웃 실패'
		    			});
					});
				}
			});
			return false;
			
		case '/login':
			location.replace('/login');
			history.pushState({}, null, '/login');
			break;

		case '/members':
			$('#mainWrapper').load('/members #mainWrapper', function(data) {
				if (data && data != '') {
					history.pushState({}, null, '/members');
				}
			});
			break;

		case '/members/join':
			location.replace('/members/join')
			history.pushState({}, null, '/members/join');
			break;

		case '/battles':
			$('#mainWrapper').load('/battles #mainWrapper', function(data) {
				if (data && data != '') {
					history.pushState({}, null, '/battles');
				}
			});
			break;

		case '/battles/new':
			$('#mainWrapper').load('/battles/new #mainWrapper', function(data) {
				if (data && data != '') {
					history.pushState({}, null, '/battles/new');
				}
			});
			return false;
			break;

		case '/memos':
			$('#mainWrapper').load('/memos #mainWrapper', function(data) {
				if (data && data != '') {
					history.pushState({}, null, '/memos');
				}
			});
			break;
		}
	});//END nav 메뉴 클릭 처리

	//뒤로, 앞으로 가기 실행시 이벤트 처리; 저장된 url로(ajax 뷰 변환 history API) replace(페이지 이동x, 주소 기록x)
	$(window).on('popstate', function() {
		location.replace(location.href);
	});
	
	//collapse 이외 영역 클릭시 숨김 처리
	$(document).click(function(e){
		var targetCollapse = $(e.target).parents('.collapse');
		$('.collapse').each(function(){
			if(targetCollapse[0]!==this){
				$(this).collapse('hide');
			}
		});
	});
});//END $.jQuery

function dateToString(milliseconds, addYears, addMonths, addDays){ 
	var date = new Date(milliseconds);

	if(!addYears)	addYears  = 0; 
	if(!addMonths)	addMonths = 0; 
	if(!addDays)	addDays   = 0; 

	date = new Date(date.setFullYear(date.getFullYear()+addYears,
									date.getMonth()+addMonths, date.getDate()+addDays));
	 
	var yy = (date.getFullYear()>9?'':'0')+date.getFullYear(); 
	var mm = date.getMonth()+1>9?date.getMonth()+1:'0'+(date.getMonth()+1); 
	var dd = (date.getDate()>9?'':'0')+date.getDate(); 
	
	var date1 = yy+'-'+mm+'-'+dd; 
	return date1; 
}

function updateMaxDate(){
	var date = new Date();
	var dateStr = (date.getFullYear()>9? '':'0') + date.getFullYear() + '-'
				+ (date.getMonth()+1>9? date.getMonth()+1:'0') + (date.getMonth()+1) + '-'
				+ (date.getDate()>9? '':'0') + date.getDate();
	$("input[type='date']").attr('max', dateStr);
}

function stringTodate(dateStr, addYears, addMonths, addDays){
	if(!addYears)	addYears  = 0;
	if(!addMonths)	addMonths = 0;
	if(!addDays)	addDays   = 0;
	
	var date = new Date(Number(dateStr.substr(0,4))+addYears,
						Number(dateStr.substr(5,2))+addMonths-1,
						Number(dateStr.substr(8,2))+addDays);
	return date;
}

//yyyy-MM-dd 포맷에 날짜 더하기
function strDateAdd(dateStr, addYears, addMonths, addDays){ 
	if(!addYears)	addYears  = 0;
	if(!addMonths)	addMonths = 0;
	if(!addDays)	addDays   = 0;
		
	date = new Date(Number(dateStr.substr(0,4))+addYears,
					Number(dateStr.substr(5,2))+addMonths-1,
					Number(dateStr.substr(8,2))+addDays);
	
	var yy = (date.getFullYear()>9?'':'0')+date.getFullYear();
	var mm = date.getMonth()+1>9?date.getMonth()+1:'0'+(date.getMonth()+1);
	var dd = (date.getDate()>9?'':'0')+date.getDate();
	
	var date1 = yy+'-'+mm+'-'+dd; 
	return date1;
}

//milliseconds > yyyy-MM-dd 포맷으로 리턴; 현재 날짜면 hh:mm:ss 포맷으로 리턴
function displayTime(milliseconds){
	var date = new Date(milliseconds);
	var ndate = new Date();
	
	var yy = (date.getFullYear()>9?'':'0')+date.getFullYear();
	var mm = date.getMonth()+1>9?date.getMonth()+1:'0'+(date.getMonth()+1);
	var dd = (date.getDate()>9?'':'0')+date.getDate();
	
	var nyy = (ndate.getFullYear()>9?'':'0')+ndate.getFullYear();
	var nmm = ndate.getMonth()+1>9?ndate.getMonth()+1:'0'+(ndate.getMonth()+1);
	var ndd = (ndate.getDate()>9?'':'0')+ndate.getDate();
	
	var date1 = yy+'/'+mm+'/'+dd;
	var ndate1 = nyy+'/'+nmm+'/'+ndd;
	
	if(ndate1 === date1){	//오늘 작성한 댓글은 시:분:초 출력
		var hh = date.getHours()>9?date.getHours():'0'+date.getHours();
		var mi = date.getMinutes()>9?date.getMinutes():'0'+date.getMinutes();
		var ss = date.getSeconds()>9?date.getSeconds():'0'+date.getSeconds(); 
		return hh+':'+mi+':'+ss;
	} else {				//오늘 이전 작성한 댓글은 연/월/일 출력
		return date1;
	}
}
