package edu.springz.security.domain;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import edu.springz.domain.AuthVO;
import edu.springz.domain.MemberVO;

import lombok.Getter;

@Getter
public class CustomUserDetails extends User implements UserDetails {
	private MemberVO mvo;
	
	public CustomUserDetails(MemberVO mvo) {
		super(mvo.getUserId(), mvo.getUserPassword(), 
				mvo.getAuthList().stream()
					.map(avo -> new SimpleGrantedAuthority(avo.getAuth()))
					.collect(Collectors.toList()));
		this.mvo = mvo;
	}

	public CustomUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
	}

	@Override
	public boolean isEnabled() {
		for (AuthVO auth : mvo.getAuthList()) {
			if(auth.getAuth().equals("ROLE_SUSPENDED")) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean isAccountNonExpired() {
		return super.isAccountNonExpired();
	}

	@Override
	public boolean isAccountNonLocked() {
		return super.isAccountNonLocked();
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return super.isCredentialsNonExpired();
	}

}
