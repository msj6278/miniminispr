package edu.springz.security;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import lombok.extern.log4j.Log4j;

@Log4j
public class CustomAccessDeniedHandler implements AccessDeniedHandler {
	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response,
			AccessDeniedException accessDeniedException) throws IOException, ServletException {
		if(request.isUserInRole("ROLE_UNVERIFIED")) {
			response.sendRedirect("/members/joinVerify");
			return;
		} else if(request.isUserInRole("ROLE_SUSPENDED")) {
			response.sendRedirect("/suspended");
			return;
		}
		
		request.setAttribute("error", "접근 권한이 없습니다.");
		RequestDispatcher rd = request.getRequestDispatcher("/login");
		rd.forward(request, response);
		return;
	}
}
