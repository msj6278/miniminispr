package edu.springz.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import lombok.extern.log4j.Log4j;

@Log4j
public class CustomLoginSuccessHandler implements AuthenticationSuccessHandler{

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		log.info("CustomLoginSuccessHandler login success");
		//사용자 권한 리스트에 저장
		List<String> roleList = new ArrayList<String>();
		authentication.getAuthorities().forEach(
				authority -> {
					roleList.add(authority.getAuthority());
				});
		log.info("ROLE: "+roleList);
		if(roleList.contains("ROLE_ADMIN")) {
			response.sendRedirect("/");
			return;
		}
		
		if(roleList.contains("ROLE_USER")) {
			response.sendRedirect("/");
			return;
		}
		
		if(roleList.contains("ROLE_UNVERIFIED")) {
			response.sendRedirect("members/joinVerify");
			return;
		}
		response.sendRedirect("/login");
	}
}
