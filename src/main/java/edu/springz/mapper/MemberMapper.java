package edu.springz.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import edu.springz.domain.AuthVO;
import edu.springz.domain.MembersCriteria;
import edu.springz.domain.MemberVO;

public interface MemberMapper {
	public MemberVO read(String userId);
	public boolean findUserId(String userId);
	public boolean insert(MemberVO memberVO);
	public boolean insertJoinVerify(String userId);
	public boolean insertMemberAuth(String userId);
	public boolean updateJoinCode(@Param("userId") String userId, @Param("joinCode") String joinCode);
	public boolean updateAuth(AuthVO authVO);
	public boolean deleteJoinVerify(@Param("userId") String userId, @Param("joinCode") String joinCode);
	public List<MemberVO> getList(MembersCriteria cri);
	public MemberVO selectUserId(String userId);
	public int getTotalCount(MembersCriteria cri);
	public boolean updateDelPhoto(String userId);
	public boolean updateMember(MemberVO memberVO);
	public String selecttMemberAuthByUserId(String userId);
}
