package edu.springz.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import edu.springz.domain.MemoVO;

public interface MemoMapper {
	
	public List<MemoVO> getList();
	
	public MemoVO get(Long memoNo);
	
	public boolean update(MemoVO mvo);
	
	public boolean delete(Long memoNo);
	
	public boolean insert(MemoVO mvo);
	
	public boolean insertSelectKey(MemoVO mvo);	
	
//	public List<MemoVO> getListWithPaging(Criteria cri);

//	public int getTotalCount(Criteria cri);
	
	public void updateReplyCnt(@Param("memoNo") Long memoNo, @Param("amount") int amount);
}
