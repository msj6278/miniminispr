package edu.springz.mapper;

import java.util.List;

import edu.springz.domain.BattleVO;
import edu.springz.domain.BattlesCriteria;
import edu.springz.domain.VoteVO;

public interface BattleMapper {
	public int insert(BattleVO battleVO);

	public int getTotalCount(BattlesCriteria cri);

	public List<BattleVO> getList(BattlesCriteria cri);

	public BattleVO selectByBattleNo(int battleNo);

	public boolean updateViewCount(int battleNo);

	public boolean inserttBattleVoted(int battleNo);

	public int insertSelectKey(BattleVO battleVO);

	public boolean updateVote(VoteVO voteVO);

	public boolean updatetBattleVoted(VoteVO voteVO);
	
}
