package edu.springz.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import edu.springz.domain.ReplyVO;

public interface ReplyMapper {
	public int getCountByMemoNo(Long memoNo);	//게시글 하나에 해당하는 댓글들
//	public List<ReplyVO> getListWithPaging(@Param("cri") Criteria cri, 
//										   @Param("memoNo") Long memoNo);	//댓글 전체 조회
	public int update(ReplyVO rvo);	//댓글 수정
	public int delete(Long rno);	//댓글 삭제
	public ReplyVO read(Long rno);	//댓글 조회 
	public int insert(ReplyVO rvo);	//댓글 등록
}	
