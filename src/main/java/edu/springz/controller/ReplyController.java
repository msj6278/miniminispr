package edu.springz.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.springz.domain.ReplyPageDTO;
import edu.springz.domain.ReplyVO;
import edu.springz.service.ReplyService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

@RestController
@Log4j
@RequestMapping("/replies")
@AllArgsConstructor
public class ReplyController {
	private ReplyService service;
	
	@GetMapping(value = "{replyNo}")
	public ResponseEntity<ReplyVO> get(@PathVariable("replyNo") Long replyNo){
		log.info("ReplyController register() replyNo : " + replyNo);
		return new ResponseEntity<>(service.get(replyNo), HttpStatus.OK);
	}
	
	@PreAuthorize("principal.username==#rvo.replyer")	//작성자 확인
	@DeleteMapping(value = "{replyNo}", produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> remove(@PathVariable("replyNo") Long replyNo,
										@RequestBody ReplyVO rvo) {
		log.info("ReplyController remove() replyNo : "+replyNo);
		return service.remove(replyNo) == 1
				? new ResponseEntity<>("success", HttpStatus.OK)	//성공일때 replyCnt 반환
				: new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
//	@GetMapping(value = "pages/{memoNo}/{page}")
//	public ResponseEntity<ReplyPageDTO> getList(@PathVariable("memoNo") Long memoNo,
//												@PathVariable("page") int page){
//		log.info("ReplyController getList() memoNo : " + memoNo);
//		Criteria cri = new Criteria(page, 10);
//		return new ResponseEntity<>(service.getListPage(cri, memoNo), HttpStatus.OK);
//	}
	
	@PreAuthorize("isAuthenticated()")
	@PostMapping(value = "new", consumes = "application/json", produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> register(@RequestBody ReplyVO rvo){
		log.info("ReplyController register() rvo : " + rvo);
		return service.register(rvo) == 1
				? new ResponseEntity<>("success", HttpStatus.OK)
				: new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@PreAuthorize("principal.username==#rvo.replyer")
	@RequestMapping(method = { RequestMethod.PUT, RequestMethod.PATCH},
					value="{replyNo}",
					consumes = "application/json",
					produces = { MediaType.TEXT_PLAIN_VALUE})
	public ResponseEntity<String> modify(@PathVariable("replyNo") Long replyNo,
										@RequestBody ReplyVO rvo) {
		rvo.setReplyNo(replyNo);
		log.info("ReplyController modify() replyNo : "+replyNo);
		log.info("ReplyController modify() rvo : "+rvo);
		return service.modify(rvo) == 1
				? new ResponseEntity<>("success", HttpStatus.OK)
				: new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
