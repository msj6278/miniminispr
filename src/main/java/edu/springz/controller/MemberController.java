package edu.springz.controller;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.springz.domain.AuthVO;
import edu.springz.domain.MembersCriteria;
import edu.springz.domain.MembersPageDTO;
import edu.springz.domain.MemberVO;
import edu.springz.service.MailService;
import edu.springz.service.MemberService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

@Controller
@Log4j
@AllArgsConstructor
public class MemberController {
	private MemberService memberService;
	private PasswordEncoder passwordEncoder;
	private MailService mailService;
	
	@InitBinder
	public void InitBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(java.util.Date.class, new CustomDateEditor(dateFormat, false));
	}
	
	//회원 등록 뷰
	@GetMapping("/members/join")
	public void joinMain() {
		log.info("MemberController join view......................");
	}

	//회원 등록 뷰 - 1. 약관
	@GetMapping(value="/members/joinTerms", consumes="application/html")
	public void terms() {
		log.info("MemberController joinTerms view......................");
	}
	
	//회원 등록 뷰 - 2. form
	@GetMapping(value="/members/joinForm", consumes="application/html")
	public void form() {
		log.info("MemberController joinForm view......................");
	}
	
	//회원 등록 뷰 - 3. 이메일 인증 대기
	@PreAuthorize("hasRole('ROLE_UNVERIFIED')")
	@GetMapping(value="/members/joinVerify")
	public void joinVerify() {
		log.info("MemberController joinVerify view......................");
	}
	
	//아이디 중복 여부 체크
	@GetMapping(value="/members/idCheck", consumes="text/plain")
	@ResponseBody	//view resolver를 거치지않고 결과 body로 반환
	public ResponseEntity<String> idCheck(String userId) {
		log.info("MemberController idCheck get userId : "+ userId);
		return memberService.idExist(userId)?
				new ResponseEntity<>(HttpStatus.CONFLICT):
				new ResponseEntity<>(HttpStatus.OK);
	}
	
	//회원 가입 폼 전송
	@PostMapping(value="/members", produces ="text/plain;charset=UTF-8")
	@ResponseBody
	public ResponseEntity<String> regist(MemberVO memberVO) {
		log.info("MemberController joinForm post ...");
		log.info(memberVO.getIntro());
		memberVO.setUserPassword(passwordEncoder.encode(memberVO.getUserPassword()));
		return memberService.join(memberVO)?
				new ResponseEntity<>("가입 성공", HttpStatus.OK):
				new ResponseEntity<>("회원가입에 실패했습니다.", HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	//회원 가입 인증 메일 전송
	@PreAuthorize("hasRole('ROLE_UNVERIFIED')")
	@PostMapping(value="/members/sendJoinCode", produces="text/plain;charset=UTF-8")
	@ResponseBody
	public ResponseEntity<String> sendJoinCode(MemberVO memberVO) {
		log.info("MemberController sendJoinCode post memberVO : "+ memberVO);
		return mailService.sendJoinCode(memberVO)?
				new ResponseEntity<>("인증메일이 발송되었습니다. 인증메일을 확인해 주세요.", HttpStatus.OK):
				new ResponseEntity<>("인증메일이 발송되지 않았습니다. 인증메일을 다시 요청해주세요.", HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	//회원 인증코드 확인
	@PostMapping("/members/verifyJoinCode")
	public String verifyJoinCode(String userId, String joinCode) {
		log.info("MemberController verifyJoinCode post ......................");
		Encoder encoder = Base64.getEncoder();
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			memberService.verifyJoinCode(userId, new String(encoder.encode(md.digest(joinCode.getBytes()))));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return "/";
		}
		return "members/vrfd";
	}
	
	//회원 권한 세션 업데이트
	@PreAuthorize("isAuthenticated()")
	@ResponseBody
	@GetMapping(value="/members/auth", consumes="text/plain")
	public ResponseEntity<String> verified(String userId) {
		log.info("MemberController updateAuth get userId : " +userId);
		return memberService.verified(userId)?
			new ResponseEntity<>("회원 인증 완료", HttpStatus.OK)
			:new ResponseEntity<>("회원 인증 실패", HttpStatus.NO_CONTENT);
	}
	
	//회원 목록 뷰
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("/members")
	public String list() {
		log.info("MemberController list view.................");
		return "members/list";
	}
	
	//회원 목록 가져오기
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping(value="/members", consumes="text/plain", produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> memberList(MembersCriteria cri) {
		log.info("MemberController list with cri : " + cri);
		int totalCount = memberService.getTotalCount(cri);
		if(totalCount<1) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		MembersPageDTO pageDTO = new MembersPageDTO(cri, memberService.getTotalCount(cri));
		map.put("memberList", memberService.getList(pageDTO.getCri()));
		map.put("pageMaker", pageDTO);
		return new ResponseEntity<>(map, HttpStatus.OK);
	}
	
	//회원 정보 뷰
	@GetMapping("/members/{userId}")
	public String member(@PathVariable("userId") String userId, Model model){
		log.info("MemberController member view : " + userId);
		model.addAttribute("userId", userId);
		return "members/get";
	}
	
	//회원 정보 가져오기
	@GetMapping(value="/members/{userId}", consumes="text/plain", produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<MemberVO> getMember(@PathVariable("userId") String userId){
		log.info("MemberController getMember() userId : " + userId);
		MemberVO memberVO = memberService.getMember(userId);
		return memberVO!=null
				?new ResponseEntity<>(memberVO, HttpStatus.OK)
				:new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	//회원 프로필 photo값 삭제하기
	@RequestMapping(method={RequestMethod.PUT, RequestMethod.PATCH},
							value = "/members/deletePhoto",
							consumes = "application/json")
	public ResponseEntity<String> deletePhoto(@RequestBody Map<String, String> map) {
		log.info("MemberController deletePhoto() : "+ map.get("userId"));
		return memberService.deletePhoto(map.get("userId"))==true
				?new ResponseEntity<>("회원 권한 수정 완료", HttpStatus.OK)
				:new ResponseEntity<>("회원 권한 수정 실패", HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	//회원 정지, 정지해제
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(method={RequestMethod.PUT, RequestMethod.PATCH},
							value = "/members/spnd",
							consumes = "application/json")
	public ResponseEntity<String> suspendMember(@RequestBody AuthVO authVO) {
		log.info("MemberController suspendMember() : " + authVO);
		return memberService.suspendMember(authVO)==true
				?new ResponseEntity<>("업데이트 완료", HttpStatus.OK)
				:new ResponseEntity<>("업데이트 실패", HttpStatus.INTERNAL_SERVER_ERROR);
	}

	//회원 정보 수정(self)
	@PreAuthorize("principal.username==#memberVO.userId")
	@RequestMapping(method={RequestMethod.PUT, RequestMethod.PATCH},
							value = "/members/{userId}",
							consumes = "application/json")
	public ResponseEntity<String> modifyMember(@RequestBody MemberVO memberVO) {
		log.info("MemberController modifyMember() : " + memberVO);
		return memberService.modifyMember(memberVO)==true
				?new ResponseEntity<>("회원 정보 수정 완료", HttpStatus.OK)
				:new ResponseEntity<>("회원 정보 수정 실패", HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	//회원 탈퇴(self)
	@PreAuthorize("principal.username==#memberVO.userId")
	@DeleteMapping(value = "{replyNo}", produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> withdraw(@RequestBody MemberVO memberVO) {
		log.info("MemberController withdraw() : " + memberVO);
		return memberService.withdraw(memberVO.getUserId())==true
				?new ResponseEntity<>("회원 탈퇴 완료", HttpStatus.OK)
				:new ResponseEntity<>("회원 탈퇴 실패", HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
}
