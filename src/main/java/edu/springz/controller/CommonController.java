package edu.springz.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import lombok.extern.log4j.Log4j;

@Controller
@Log4j
public class CommonController {
	@GetMapping("/accessError")
	public void accessDenied(Authentication auth, Model model) {	//model 쓰지않아도 기본 예외 메시지도 있음
		log.info("CommonController accessDenied(): ");
		model.addAttribute("msg", "접근 권한없음");
	}
	
	//spring-security 기본 로그인 페이지; loginProc.jsp에서 login 페이지로 replace 처리(ajax 화면전환용)
	@GetMapping("/loginProc")
	public void loginProc() {
		
	}
	
	@GetMapping("/login")
	public void customLogin(String error, String logout, Model model) {
		log.info("CommonController loginInput()");
		log.info("error: "+error);
		log.info("logout: "+logout);
		
		if(error!=null) {
			model.addAttribute("error", "로그인 에러");
		}
		if(logout!=null) {
			model.addAttribute("logout", "로그아웃");
		}
	}
	
	@GetMapping("/")
	public String home(Model model) {
		return "redirect:index";
	}
	
	@GetMapping("/index")
	public void index(Model model) {
		log.info("CommonController index(): ");
	}
	
	@GetMapping("/board/memo")
	public void memoList(Model model) {
		
		log.info("CommonController memoList(): ");
	}
	
	//정지회원 리다이렉트 화면
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("/suspended")
	public void suspended() {
		log.info("CommonController suspended view.................");
	}
}
