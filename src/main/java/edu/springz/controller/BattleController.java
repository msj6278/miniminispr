package edu.springz.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.springz.domain.BattleVO;
import edu.springz.domain.BattlesCriteria;
import edu.springz.domain.BattlesPageDTO;
import edu.springz.domain.VoteVO;
import edu.springz.service.BattleService;
import edu.springz.util.Cookies;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

@Controller
@Log4j
@AllArgsConstructor
public class BattleController {
	private BattleService battleService;
	
	@InitBinder
	public void InitBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(java.util.Date.class, new CustomDateEditor(dateFormat, false));
	}
	
	//배틀 등록 뷰
	@GetMapping("/battles/new")
	public void registBattle() {
		log.info("BattleController new view......................");
	}

	//배틀 등록 폼 전송
	@PostMapping(value="/battles", produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Map<String, String>> newBattle(Principal principal, BattleVO battleVO) {
		log.info("BattleController battleNew post... BattleVO : "+ battleVO);
		Map<String, String> map = new HashMap<String, String>();
		//userId 정보를 form아닌 principal로 받기 위함
		/*if(principal==null || principal.getName().equals("")) {
			map.put("message", "로그인 후 이용 가능합니다.");
			return new ResponseEntity<>(map, HttpStatus.UNAUTHORIZED); 
		}*/
		//battleVO.setUserId(principal.getName());
		battleVO.setUserId("user01");
		try {
			battleVO.setImage1(URLDecoder.decode(battleVO.getImage1(), "UTF-8"));
			battleVO.setImage2(URLDecoder.decode(battleVO.getImage2(), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		int battleNo = battleService.newBattle(battleVO);
		if(battleNo > 0) {
			map.put("message", "배틀 등록 성공");
			map.put("battleNo", Integer.toString(battleNo));
			return new ResponseEntity<>(map, HttpStatus.OK);
		} else {
			map.put("message", "등록에 실패했습니다.");
			map.put("battleNo", Integer.toString(battleNo));
			return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	//배틀 목록 뷰
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("/battles")
	public String list() {
		log.info("MemberController list view.................");
		return "battles/list";
	}
	
	//배틀 목록 가져오기
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping(value="/battles", consumes="application/json", produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Map<String, Object>> memberList(BattlesCriteria cri) {
		log.info("BattleController list with cri : " + cri);
		int totalCount = battleService.getTotalCount(cri);
		if(totalCount<1) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		BattlesPageDTO pageDTO = new BattlesPageDTO(cri, battleService.getTotalCount(cri));
		log.info("cri.getPageNum: "+cri.getPageNum());
		log.info("pageDTO.getRealEndPage(): "+pageDTO.getRealEndPage());
		map.put("battleList", battleService.getList(pageDTO.getCri()));
		map.put("pageMaker", pageDTO);
		return new ResponseEntity<>(map, HttpStatus.OK);
	}
	
	//배틀 정보 뷰
	@GetMapping("/battles/{battleNo}")
	public String member(@PathVariable("battleNo") String battleNo){
		log.info("BattleController battle view : " + battleNo);
		return "battles/get";
	}

	//배틀 상세 가져오기
	@GetMapping(value="/battles/{battleNo}", consumes="text/plain", 
				produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<BattleVO> getMember(@PathVariable("battleNo") int battleNo,
			HttpServletRequest request, HttpServletResponse response){
		log.info("BattleController getBattle() battleNo : " + battleNo);
		Cookies cookies = new Cookies(request);
		Cookie hitBattleCookie;
		Calendar cal = Calendar.getInstance();
		String hitBattleCookieName = "hitBattle_"+cal.get(Calendar.YEAR)
								+cal.get(Calendar.MONTH)+cal.get(Calendar.DATE);
		//당일 쿠키 없을경우 생성
		if(!cookies.exists(hitBattleCookieName)) {
			hitBattleCookie = new Cookie(hitBattleCookieName, "/"+Integer.toString(battleNo)+"/");
			if(!battleService.hitBattle(battleNo)) {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} else { //기존 쿠키 업데이트
			hitBattleCookie = cookies.getCookie(hitBattleCookieName);
			if(!hitBattleCookie.getValue().matches(".*/"+Integer.toString(battleNo)+"/.*")) {
				log.info(hitBattleCookie.getValue());
				log.info(hitBattleCookie.getValue().matches("/"+Integer.toString(battleNo)+"/"));
				if(!battleService.hitBattle(battleNo)) {
					return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
				}
				hitBattleCookie.setValue(hitBattleCookie.getValue() +battleNo+"/");
			}
		}
		BattleVO battleVO = battleService.getMember(battleNo);
		if(battleVO!=null) {
			hitBattleCookie.setMaxAge(60*60*24);
			hitBattleCookie.setPath("/");
			response.addCookie(hitBattleCookie);
			return new ResponseEntity<>(battleVO, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	//선호도 투표 수 업데이트
	@RequestMapping(method={RequestMethod.PUT, RequestMethod.PATCH},
					value = "/battles/vote", consumes = "application/json",
					produces= "application/json;charset=utf8")
	public ResponseEntity<String> vote(@RequestBody VoteVO voteVO, Principal principal) {
		log.info("BattleController vote() : " + voteVO);
		/*if(principal==null || principal.getName().equals("")) {
			map.put("message", "로그인 후 이용 가능합니다.");
			return new ResponseEntity<>(map, HttpStatus.UNAUTHORIZED); 
		}*/
		//battleVO.setUserId(principal.getName());
		voteVO.setUserId("user01");
		Map<String, String> map = battleService.vote(voteVO);
		if(map.containsKey("success")) {
			return new ResponseEntity<>(map.get("success"), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(map.get("error"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
//	//회원 프로필 photo값 삭제하기
//	@RequestMapping(method={RequestMethod.PUT, RequestMethod.PATCH},
//							value = "/members/deletePhoto",
//							consumes = "application/json")
//	public ResponseEntity<String> deletePhoto(@RequestBody Map<String, String> map) {
//		log.info("MemberController deletePhoto() : "+ map.get("userId"));
//		return memberService.deletePhoto(map.get("userId"))==true
//				?new ResponseEntity<>("회원 권한 수정 완료", HttpStatus.OK)
//				:new ResponseEntity<>("회원 권한 수정 실패", HttpStatus.INTERNAL_SERVER_ERROR);
//	}
//	
//	//회원 정지, 정지해제
////	@PreAuthorize("hasRole('ROLE_ADMIN')")
//	@RequestMapping(method={RequestMethod.PUT, RequestMethod.PATCH},
//							value = "/members/spnd",
//							consumes = "application/json")
//	public ResponseEntity<String> suspendMember(@RequestBody AuthVO authVO) {
//		log.info("MemberController suspendMember() : " + authVO);
//		return memberService.suspendMember(authVO)==true
//				?new ResponseEntity<>("업데이트 완료", HttpStatus.OK)
//				:new ResponseEntity<>("업데이트 실패", HttpStatus.INTERNAL_SERVER_ERROR);
//	}
//
//	//회원 정보 수정(self)
//	@PreAuthorize("principal.username==#memberVO.userId")
//	@RequestMapping(method={RequestMethod.PUT, RequestMethod.PATCH},
//							value = "/members/{userId}",
//							consumes = "application/json")
//	public ResponseEntity<String> modifyMember(@RequestBody MemberVO memberVO) {
//		log.info("MemberController modifyMember() : " + memberVO);
//		return memberService.modifyMember(memberVO)==true
//				?new ResponseEntity<>("회원 정보 수정 완료", HttpStatus.OK)
//				:new ResponseEntity<>("회원 정보 수정 실패", HttpStatus.INTERNAL_SERVER_ERROR);
//	}
//	
//	//회원 탈퇴(self)
//	@PreAuthorize("principal.username==#memberVO.userId")
//	@DeleteMapping(value = "{replyNo}", produces = MediaType.TEXT_PLAIN_VALUE)
//	public ResponseEntity<String> withdraw(@RequestBody MemberVO memberVO) {
//		log.info("MemberController withdraw() : " + memberVO);
//		return memberService.withdraw(memberVO.getUserId())==true
//				?new ResponseEntity<>("회원 탈퇴 완료", HttpStatus.OK)
//				:new ResponseEntity<>("회원 탈퇴 실패", HttpStatus.INTERNAL_SERVER_ERROR);
//	}
	
}
