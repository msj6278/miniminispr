package edu.springz.controller;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.bind.annotation.InitBinder;
//import org.zerock.domain.BoardAttachVO;

import edu.springz.domain.MemoAttachVO;
import edu.springz.domain.MemoVO;
import edu.springz.service.MemoService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

@Controller
@Log4j
@AllArgsConstructor
public class MemoController {
	private MemoService service;
	
	@InitBinder
	public void InitBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(java.util.Date.class, new CustomDateEditor(dateFormat, false));
	}
	
//	@GetMapping("write")
//	@PreAuthorize("isAuthenticated()")
//	public void write() {
//		log.info("memo write get ......................");
//	}
	
	//메모 목록 뷰
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@GetMapping("/memos")
	public String list() {
		log.info("MemoController list view.................");
		return "memos/list";
	}
	
//	@GetMapping("get")
//	public void get(@RequestParam("memoNo") Long memoNo, Model model, @ModelAttribute("cri") Criteria cri) {
//		log.info("memo get ......................");
//		model.addAttribute("mvo", service.get(memoNo));
//	}
//	
//	@GetMapping("modify")
//	@PreAuthorize("principal.username == #userId")
//	public void modify(@RequestParam("memoNo") Long memoNo, String userId, Model model, @ModelAttribute("cri") Criteria cri) {
//		log.info("memo modify ......................get");
//		model.addAttribute("mvo", service.get(memoNo));
//	}
//	
//	@PostMapping("modify")
//	@PreAuthorize("principal.username == #mvo.userId")
//	public String modify(MemoVO mvo, RedirectAttributes rttr, @ModelAttribute("cri") Criteria cri) {
//		log.info("memo modify ......................post");
//		log.info(mvo.getUserId());
//		rttr.addFlashAttribute("result", service.modify(mvo));
//		return "redirect:/memo/list" + cri.getListlink();
//	}
//	
//	@PostMapping("remove")
//	@PreAuthorize("principal.username == #userId")
//	public String remove(@RequestParam("memoNo") Long memoNo, String userId, RedirectAttributes rttr, @ModelAttribute("cri") Criteria cri) {
//		List<MemoAttachVO> attachList = service.getAttachList(memoNo);
//		if(service.remove(memoNo) && attachList != null && attachList.size() > 0) {
//			deleteFiles(attachList);
//		}
//		rttr.addFlashAttribute("result", true);
//		return "redirect:/memo/list" + cri.getListlink();
//	}
	
//	@PostMapping("write")
//	@PreAuthorize("isAuthenticated()")
//	public String write(MemoVO mvo, RedirectAttributes rttr) {
//		service.write(mvo);
//		rttr.addFlashAttribute("result", mvo.getMemoNo());
//		return "redirect:/memo/list";
//	}
//	
//	@GetMapping(value="getAttachList", produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
//	@ResponseBody
//	public ResponseEntity<List<MemoAttachVO>> getAttachList(Long memoNo){
//		log.info("MemoController getAttachList() memoNo : " + memoNo);
//		return new ResponseEntity<>(service.getAttachList(memoNo), HttpStatus.OK);
//	}
//	
//	private void deleteFiles(List<MemoAttachVO> attachList) {
//		if(attachList == null || attachList.size() <= 0) {
//			return;
//		}
//		log.info("deleteFiles..");
//		log.info(attachList);
//		attachList.forEach(avo -> {
//			try {
//				Path file = Paths.get("C:\\upload\\"+avo.getUploadPath()+"\\"+avo.getUuid()+"_"+avo.getFileName());	//java.lang io>nio(new input output; io의 업그레이드 버전)// Path; Path는 IO의 java.io.File 클래스에 대응되는 nio 인터페이스
//				if(avo.isFileType()) {
//					Path thumbNail = Paths.get("C:\\upload\\"+avo.getUploadPath()+"\\s_"+avo.getUuid()+"_"+avo.getFileName());
//					Files.delete(thumbNail);
//				}
//				Files.deleteIfExists(file);   //java.nio.file.Files 클래스는 파일과 디렉토리의 생성 및 삭제, 그리고 이들의 속성을 읽는 메소드 (디렉토리가 숨김인지, 디렉토리인지, 크리가 어떻게 되는지, 소유자가 누구인지에 대한 정보)
//			} catch(Exception e) {
//				e.printStackTrace();
//			}
//		});//END foreach
//	}//END deleteFiles()
	
}
