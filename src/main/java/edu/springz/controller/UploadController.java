package edu.springz.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.FileExistsException;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import edu.springz.domain.AttachFileDTO;
import lombok.extern.log4j.Log4j;
import net.coobird.thumbnailator.Thumbnailator;

@Controller
@Log4j
public class UploadController {
	
	//현재 시점의 연\\월\\일 폴더 경로 문자열 생성
	private String getFolder() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String str = sdf.format(date);
		return str.replace("-", File.separator);
	}
	
	private boolean checkImageType(File file) {	//이미지 파일 여부 확인
		try {
			String contentType = Files.probeContentType(file.toPath());
			return contentType.startsWith("image");
		} catch(NullPointerException e) {
//			e.printStackTrace();	probeContentType 없는경우
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	//프로필 사진 업로드
	@PostMapping("uploadProfile")
	@ResponseBody
	public ResponseEntity<AttachFileDTO> uploadProfile(MultipartFile[] uploadFile, Model model) {
		log.info("UploadController uploadProfile post mapping...");
		AttachFileDTO adto = new AttachFileDTO();
		String uploadPath = "C:\\upload\\";
		String uploadFolder = "profile\\";
		File uploadFullPath = new File(uploadPath, uploadFolder);
		if(!uploadFullPath.exists()) {
			uploadFullPath.mkdirs();	//mkdir() 이전경로가 존재해야 폴더 생성; mkdirs() 한번에 모든 경로 생성
		}
		log.info("Upload Path: " + uploadPath);
		log.info("Upload File Name: " + uploadFile[0].getOriginalFilename());
		log.info("Upload File Size: " + uploadFile[0].getSize());
		log.info("Upload File ContentType: " + uploadFile[0].getContentType());
		String uploadFileName = uploadFile[0].getOriginalFilename();
		UUID uuid = UUID.randomUUID();
		adto.setFileName(uploadFileName);
		adto.setUploadFolder(uploadFolder);
		adto.setUuid(uuid.toString());
		
		log.info("uuid : " + uuid);
		
		//IE file name에 전체 경로 포함; substring으로 마지막 \ 인덱스로 잘라내기
		uploadFileName = uuid.toString() + "_" + uploadFileName.substring(uploadFileName.lastIndexOf("\\")+1);
	
		File saveFile = new File(uploadFullPath, uploadFileName);	//new File(path, 파일명)
	
		try {
			uploadFile[0].transferTo(saveFile);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(adto, HttpStatus.OK);
	}//END uploadAjaxAction()
	
	//파일 업로드(+썸네일 생성)
	@PostMapping("uploadFile")
	@ResponseBody
	public ResponseEntity<List<AttachFileDTO>> uploadFile(MultipartFile[] uploadFile, Model model) {
		log.info("uploadController uploadFile()");
		for (MultipartFile file : uploadFile) {
			log.info("===========================");
			log.info(file);
		}
		List<AttachFileDTO> list = new ArrayList<AttachFileDTO>();
		String uploadFolder = "C:\\upload";
		//연\\월\\일 폴더가 없으면 생성
		File uploadPath = new File(uploadFolder, getFolder());
		if(!uploadPath.exists()) {
			uploadPath.mkdirs();	//mkdir() 이전경로가 존재해야 폴더 생성; mkdirs() 한번에 모든 경로 생성
		}
		
		for(MultipartFile multipartFile : uploadFile) {
			AttachFileDTO adto = new AttachFileDTO();
			log.info("Upload File Name: " + multipartFile.getOriginalFilename());
			log.info("Upload File Size: " + multipartFile.getSize());
			log.info("Upload File ContentType: " + multipartFile.getContentType());
			String uploadFileName = multipartFile.getOriginalFilename();
			UUID uuid = UUID.randomUUID();
			//list에 AttachFileDTO 추가
			adto.setFileName(uploadFileName);
			adto.setUploadFolder(getFolder());
			adto.setUuid(uuid.toString());
			log.info("uuid : " + uuid);
			//IE file name에 전체 경로 포함; substring으로 마지막 \ 인덱스로 잘라내기
			uploadFileName = uuid.toString() + "_" + uploadFileName.substring(uploadFileName.lastIndexOf("\\")+1);
			File saveFile = new File(uploadPath, uploadFileName);	//new File(path, 파일명)
			try {
				multipartFile.transferTo(saveFile);
				//이미지 파일이면 썸네일 파일 생성
				if(checkImageType(saveFile)) {
					adto.setImage(true);
					FileOutputStream fos = new FileOutputStream(new File(uploadPath, "s_" + uploadFileName));
					Thumbnailator.createThumbnail(multipartFile.getInputStream(), fos, 200, 200);
					fos.close();
				} else {
					adto.setImage(false);
				}
				list.add(adto);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}//END for
		return new ResponseEntity<>(list, HttpStatus.OK);
	}//END uploadAjaxAction()
	
	@GetMapping(value="display")
	@ResponseBody
	public ResponseEntity<byte[]> getFile(String fileName){
		log.info("UploadController display fileName: "+ fileName);
		File file;
		try {
			log.info("fileName: "+URLDecoder.decode(fileName, "UTF-8"));
			file = new File("C:\\upload\\"+URLDecoder.decode(fileName, "UTF-8"));
			if(!file.exists()) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			log.info("file: "+file);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		ResponseEntity<byte[]> result = null;
		try {
			HttpHeaders header = new HttpHeaders();
			header.add("Content-Type", Files.probeContentType(file.toPath()));
			result = new ResponseEntity<>(FileCopyUtils.copyToByteArray(file), header, HttpStatus.OK);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
		
	@GetMapping(value="download", produces=MediaType.APPLICATION_OCTET_STREAM_VALUE)
	@ResponseBody
	public ResponseEntity<Resource> downloadFile(String fileName, @RequestHeader("User-Agent") String userAgent) {
		log.info("download file: "+fileName);
		Resource resource;
		try {	//경로명 포함 파일객체 생성
			resource = new FileSystemResource("C:\\upload\\"+URLDecoder.decode(fileName, "UTF-8"));
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}	
		//resource가 없으면 상태 코드 404 반환
		if(resource.exists() == false) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		//UUID 제거하고 저장
		log.info("resource: "+resource);
		String resourceName = resource.getFilename().substring(resource.getFilename().indexOf("_")+1);
		HttpHeaders headers = new HttpHeaders();
		try {
			String downloadName = null;

			if(userAgent.contains("trident")) {		//IE의 경우
				log.info("IE browser");
				downloadName = URLEncoder.encode(resourceName, "UTF-8").replaceAll("\\+", " ");
			} else if(userAgent.contains("Edge")) {	//Edge의 경우
				log.info("Edge browser");
				downloadName = URLEncoder.encode(resourceName, "UTF-8");
				log.info("Edge name: "+downloadName);
			} else {								//그외의 경우
				downloadName = new String(resourceName.getBytes("UTF-8"), "ISO-8859-1");	//한글 깨지는경우 이걸로 설정 
			}
			headers.add("Content-Disposition", "attachment; filename="+downloadName);
		} catch(UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Resource>(resource, headers, HttpStatus.OK);
	}
	
	//이미지 파일의 경우 썸네일 파일명, 파일 타입 파라미터 요구 
	@PostMapping("deleteFile")
	@ResponseBody
	public ResponseEntity<String> deleteFile(String fileName, String type){
		log.info(type);
		File file;
		try {
			log.info("fileName: "+"C:\\upload\\"+ URLDecoder.decode(fileName, "UTF-8"));
			file = new File("C:\\upload\\"+ URLDecoder.decode(fileName, "UTF-8"));
			file.delete();
			//이미지 파일이면 원본 이미지 삭제
			if(type.equals("image")) {
				file = new File(file.getAbsolutePath().replace("s_", ""));
				log.info("fileName: "+"C:\\upload\\"+ URLDecoder.decode(fileName, "UTF-8"));
				file.delete();
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return new ResponseEntity<>("delete ok", HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>("delete ok", HttpStatus.OK);
	}
}
