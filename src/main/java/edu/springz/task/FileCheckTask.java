package edu.springz.task;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import edu.springz.domain.MemoAttachVO;
import edu.springz.mapper.MemoAttachMapper;
import lombok.Setter;
import lombok.extern.log4j.Log4j;

@Component
@Log4j
//@AllArgsConstructor
public class FileCheckTask {
	@Setter(onMethod_ = {@Autowired})
	private MemoAttachMapper attachMapper;
	
	//어제 날짜 폴더의 문자열 반환
	//어제 날짜의 연\\월\\일 폴더 경로 문자열 생성
	private String getFolderYesterDay() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date(System.currentTimeMillis()-24*60*60*1000);
		String path = sdf.format(date);
		return path.replace("-", File.separator);
	}
	
	@Scheduled(cron = "0 0 14 * * *")
	public void checkFiles() throws Exception{
		Date now = new Date();
		log.warn("Running................ : " +now.toLocaleString());
		log.warn("-------------------------------------------------");
		
		//1) 데이터 베이스에서 어제 사용된 파일의 목록 받아오기
		List<MemoAttachVO> fileList = attachMapper.getOldFiles();
		
		//2) 해당 폴더의 파일 목록에서 데이터베이스에 없는 파일 찾아내기
		//tbl_attach 테이블의 데이터를 목록으로 변환
		List<Path> fileListPaths = fileList.stream()
				.map(avo -> Paths.get("c:\\upload",
				avo.getUploadPath(), avo.getUuid()+"_"+avo.getFileName()))
				.collect(Collectors.toList());
		//섬네일 이미지가 있는 경우 목록에 추가
		fileList.stream().filter(avo -> avo.isFileType() == true)
				.map(avo -> Paths.get("c:\\upload",
				avo.getUploadPath(), "s_"+avo.getUuid()+"_"+avo.getFileName()))
				.forEach(p -> fileListPaths.add(p));
		
		//어제 날짜 폴더에서 실제 파일들 가져오기
		File targetDir = Paths.get("c:\\upload", getFolderYesterDay()).toFile();
		
		//3) 데이터베이스에 없는 파일들 삭제하기
		//목록을 비교하여 목록에 없는 삭제 대상 파일들을 배열에 저장
		File[] removeFiles = targetDir.listFiles(file -> fileListPaths.contains(file.toPath())==false);
		//배열에 파일들 삭제
		for (File f : removeFiles) {
			f.delete();
		}
		log.warn("Yesterday's files deleted");
		
	}
}
