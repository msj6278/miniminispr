package edu.springz.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.springz.domain.AuthVO;
import edu.springz.domain.MembersCriteria;
import edu.springz.domain.MemberVO;
import edu.springz.mapper.MemberMapper;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

@Service
@Log4j
@AllArgsConstructor
public class MemberServiceImpl implements MemberService{
	private MemberMapper memberMapper;

	@Override
	public boolean idExist(String userId) {
		return memberMapper.findUserId(userId);
	}

	@Transactional
	@Override
	public boolean join(MemberVO memberVO) {
		if(memberMapper.insert(memberVO)) {
			if(memberMapper.insertMemberAuth(memberVO.getUserId())) {
				return memberMapper.insertJoinVerify(memberVO.getUserId());
			}
		}
		return false;
	}

	@Transactional
	@Override
	public boolean verifyJoinCode(String userId, String joinCode) {
		if(memberMapper.deleteJoinVerify(userId, joinCode)) {
			AuthVO authVO = new AuthVO();
			authVO.setUserId(userId);
			authVO.setAuth("ROLE_USER");
			return memberMapper.updateAuth(authVO);
		}
		return false;
	}

	@Override
	public List<MemberVO> getList(MembersCriteria cri) {
		return memberMapper.getList(cri);
	}

	@Override
	public MemberVO getMember(String userId) {
		return memberMapper.selectUserId(userId);
	}

	@Override
	public int getTotalCount(MembersCriteria cri) {
		return memberMapper.getTotalCount(cri);
	}

	@Override
	public boolean modifyAuth(AuthVO authVO) {
		return memberMapper.updateAuth(authVO);
	}

	@Override
	public boolean deletePhoto(String userId) {
		return memberMapper.updateDelPhoto(userId);
	}

	@Override
	public boolean suspendMember(AuthVO authVO) {
		if(authVO.getAuth().equals("ROLE_USER")) {
			authVO.setAuth("ROLE_SUSPENDED");
		} else if(authVO.getAuth().equals("ROLE_SUSPENDED")) {
			authVO.setAuth("ROLE_USER");
		} else {	//USER, SUSPENDED 권한 외 요청은 false 처리
			return false;
		}
		return memberMapper.updateAuth(authVO);
	}

	@Override
	public boolean modifyMember(MemberVO memberVO) {
		return memberMapper.updateMember(memberVO);
	}

	@Override
	public boolean withdraw(String userId) {
		return false;
	}

	@Override
	public boolean verified(String userId) {
		String updatedAuth = memberMapper.selecttMemberAuthByUserId(userId);
		if(updatedAuth != null && !updatedAuth.equals("ROLE_UNVERIFIED")) {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			List<GrantedAuthority> updatedAuthorities = new ArrayList<>(auth.getAuthorities());
			updatedAuthorities.clear();
			updatedAuthorities.add(new SimpleGrantedAuthority(updatedAuth));
			Authentication newAuth = 
					new UsernamePasswordAuthenticationToken(auth.getPrincipal(), 
										auth.getCredentials(), updatedAuthorities);
			SecurityContextHolder.getContext().setAuthentication(newAuth);
			return true;
		} else {
			return false;
		}
	
	}
}
