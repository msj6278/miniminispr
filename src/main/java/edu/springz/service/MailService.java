package edu.springz.service;

import edu.springz.domain.MemberVO;

public interface MailService {
	public boolean sendJoinCode(MemberVO memberVO);
}
