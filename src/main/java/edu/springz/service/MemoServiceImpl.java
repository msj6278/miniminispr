package edu.springz.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import edu.springz.domain.MemoAttachVO;
import edu.springz.domain.MemoVO;
import edu.springz.mapper.MemoAttachMapper;
import edu.springz.mapper.MemoMapper;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

@Service
@Log4j
@AllArgsConstructor
public class MemoServiceImpl implements MemoService{
	private MemoAttachMapper attachMapper;
	private MemoMapper mapper;
	
//	@Override
//	public List<MemoVO> getList(Criteria cri) {
//		log.info("getList.............. cri");
//		return mapper.getListWithPaging(cri);
//	}
	
	@Override
	public List<MemoVO> getList() {
		log.info("getList..............");
		return mapper.getList();
	}
	
	@Override
	public MemoVO get(Long memoNo) {
		log.info("get..............");
		return mapper.get(memoNo);
	}
	
	@Transactional
	@Override
	public boolean modify(MemoVO mvo) {
		attachMapper.deleteAll(mvo.getMemoNo());
		if(mvo.getAttachList() != null && mvo.getAttachList().size() > 0) { //첨부 파일이 없으면 중단
			mvo.getAttachList().forEach(avo -> {	//첨부 파일이 있으면 tbl_attach에 insert
				avo.setMemoNo(mvo.getMemoNo());
				attachMapper.insert(avo);
			});
		}
		return mapper.update(mvo);
	}
	
	@Transactional
	@Override
	public boolean remove(Long memoNo) {
		attachMapper.deleteAll(memoNo);
		return mapper.delete(memoNo);
	}
	
	@Transactional
	@Override
	public void write(MemoVO mvo) {
		mapper.insertSelectKey(mvo);
		if(mvo.getAttachList() != null && mvo.getAttachList().size() > 0) { //첨부 파일이 없으면 중단
			mvo.getAttachList().forEach(avo -> {	//첨부 파일이 있으면 tbl_attach에 insert
				avo.setMemoNo(mvo.getMemoNo());
				attachMapper.insert(avo);
			});
		}
	}

//	@Override
//	public int getTotal(Criteria cri) {
//		log.info("get total count");
//		return mapper.getTotalCount(cri);
//	}

//	@Override
//	public int getTotalCount(Criteria cri) {
//		log.info("getTotalCount .......");
//		return mapper.getTotalCount(cri);
//	}

	@Override
	public List<MemoAttachVO> getAttachList(Long memoNo) {
		log.info("get Attach list by memoNo" + memoNo);
		return attachMapper.findByBno(memoNo);	//bno로 attachVO 가져오기
	}
}
