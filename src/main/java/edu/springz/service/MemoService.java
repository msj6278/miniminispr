package edu.springz.service;

import java.util.List;

import edu.springz.domain.MemoAttachVO;
import edu.springz.domain.MemoVO;

public interface MemoService {
	
	//public int getTotalCount(Criteria cri);
	
	public List<MemoVO> getList();
	
	public MemoVO get(Long memoNo);
	
	public boolean modify(MemoVO mvo);
	
	public boolean remove(Long memoNo);
	
	public void write(MemoVO mvo);
	
	//public List<MemoVO> getList(Criteria cri);

	//public int getTotal(Criteria cri);

	public List<MemoAttachVO> getAttachList(Long memoNo);
	
	
}
