package edu.springz.service;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.Date;
import java.util.Properties;
import java.util.Base64.Encoder;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.stereotype.Service;
import com.sun.mail.util.MailConnectException;

import edu.springz.domain.MemberVO;
import edu.springz.mapper.MemberMapper;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

@Service
@Log4j
@AllArgsConstructor
public class MailServiceImpl implements MailService{
	MemberMapper memberMapper;
	
	@Override
	public boolean sendJoinCode(MemberVO memberVO) {
		Encoder encoder = Base64.getEncoder();
		byte[] byteBuffer = new byte[32];
		SecureRandom random = new SecureRandom();
		random.nextBytes(byteBuffer);
		byteBuffer = encoder.encode(byteBuffer);
		String content = "<h3>선호도 배틀 사이트 회원 인증 메일입니다.</h3>"
				+ memberVO.getUserName() +"님, 선호도 배틀 사이트 가입을 진심으로 환영합니다.<br>"
				+ "본 이메일 인증은 원할한 서비스를 제공하기 위함이며, 이외의 목적으로 이용되지 않습니다.<br><br>"
				+ "아래의 회원 인증 하기 버튼을 클릭하시면 회원 인증을 완료합니다.<br><br>"
				+ "<form action='http://localhost:9090/members/verifyJoinCode' method='post' target='vrfd'>"
				+ "<input type='hidden' name='userId' value='"+memberVO.getUserId()+"'>"
				+ "<input type='hidden' name='joinCode' value='"
				+ new String(byteBuffer) + "'>"
				+ "<button>회원 인증 하기</button></form>";
		if(sendEmail(memberVO.getUserId(), memberVO.getEmail(), "제목", content)) {
			try {
				MessageDigest md = MessageDigest.getInstance("SHA-256");
				return memberMapper.updateJoinCode(memberVO.getUserId(), new String(encoder.encode(md.digest(byteBuffer))));
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}

	public boolean sendEmail(String userId, String email, String subject, String content) {
		//SMTP server information
	    String host = "smtp.gmail.com";
	    String port = "587";
	    String mailFrom = "test62780847@gmail.com";
	    String password = "1234qwer@@";
	
		Properties properties = new Properties();
	    properties.put("mail.smtp.host", host);
	    properties.put("mail.smtp.port", port);
	    properties.put("mail.smtp.auth", "true");
	    properties.put("mail.smtp.starttls.enable", "true");
	    
	    // creates a new session with an authenticator
        Authenticator auth = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(mailFrom, password);
            }
        };
        
		Session session = Session.getInstance(properties, auth);
		
		try {
			Transport transport = session.getTransport("smtp");
			
			MimeMessage message = new MimeMessage(session);
			InternetAddress[] addresses = {
					new InternetAddress(email)};
			try {
				message.setFrom(new InternetAddress(mailFrom, "선호도 배틀 사이트", "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			message.setRecipients(Message.RecipientType.TO, addresses);
			message.setSubject(subject, "UTF-8");
			message.setSentDate(new Date());
			message.setContent(content, "text/html; charset=UTF-8");
			transport.send(message);
		} catch (MailConnectException me) {
			return false;
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
			return false;
		} catch (AddressException e) {
			e.printStackTrace();
			return false;
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
