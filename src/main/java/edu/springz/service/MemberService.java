package edu.springz.service;

import java.util.List;

import edu.springz.domain.AuthVO;
import edu.springz.domain.MemberVO;
import edu.springz.domain.MembersCriteria;

public interface MemberService {
	public boolean idExist(String userId);
	public boolean join(MemberVO memberVO);
	public boolean verifyJoinCode(String userId, String joinCode);
	public List<MemberVO> getList(MembersCriteria cri);
	public MemberVO getMember(String userId);
	public int getTotalCount(MembersCriteria criteria);
	public boolean modifyAuth(AuthVO authVO);
	public boolean deletePhoto(String userId);
	public boolean suspendMember(AuthVO authVO);
	public boolean modifyMember(MemberVO memberVO);
	public boolean withdraw(String userId);
	public boolean verified(String userId);
}
