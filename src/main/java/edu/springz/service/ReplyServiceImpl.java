package edu.springz.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.springz.domain.ReplyPageDTO;
import edu.springz.domain.ReplyVO;
import edu.springz.mapper.MemoMapper;
import edu.springz.mapper.ReplyMapper;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;


@Service
@Log4j
@AllArgsConstructor
public class ReplyServiceImpl implements ReplyService {
	private ReplyMapper mapper;
	private MemoMapper memoMapper;
	
//	@Override
//	public List<ReplyVO> getList(Criteria cri, Long memoNo) {
//		log.info("ReplyServiceImpl getList() memoNo : " + memoNo);
//		return mapper.getListWithPaging(cri, memoNo);
//	}

	@Override
	public int modify(ReplyVO rvo) {
		log.info("ReplyServiceImpl modify() rvo : " + rvo);
		return mapper.update(rvo);
	}

	@Transactional
	@Override
	public int remove(Long rno) {
		log.info("ReplyServiceImpl remove() rno : " + rno);
		ReplyVO rvo = mapper.read(rno);
		memoMapper.updateReplyCnt(rvo.getMemoNo(), -1);
		return mapper.delete(rno);
	}

	@Override
	public ReplyVO get(Long rno) {
		log.info("ReplyServiceImpl get() rno : " + rno);
		return mapper.read(rno);
	}

	@Transactional
	@Override
	public int register(ReplyVO rvo) {
		log.info("ReplyServiceImpl register() rvo : " + rvo);
		memoMapper.updateReplyCnt(rvo.getMemoNo(), 1);
		return mapper.insert(rvo);
	}

//	@Override
//	public ReplyPageDTO getListPage(Criteria cri, Long memoNo) {
//		log.info("ReplyServiceImpl getListPage");
//		return new ReplyPageDTO(mapper.getCountByMemoNo(memoNo), mapper.getListWithPaging(cri, memoNo));
//	}
}
