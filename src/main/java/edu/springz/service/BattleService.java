package edu.springz.service;

import java.util.List;
import java.util.Map;

import edu.springz.domain.BattleVO;
import edu.springz.domain.BattlesCriteria;
import edu.springz.domain.VoteVO;

public interface BattleService {
	public int newBattle(BattleVO battleVO);

	public int getTotalCount(BattlesCriteria cri);

	public List<BattleVO> getList(BattlesCriteria cri);

	public BattleVO getMember(int battleNo);

	public boolean hitBattle(int battleNo);

	public Map<String, String> vote(VoteVO voteVO);

}
