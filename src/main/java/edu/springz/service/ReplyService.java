package edu.springz.service;

import java.util.List;

import edu.springz.domain.ReplyPageDTO;
import edu.springz.domain.ReplyVO;


public interface ReplyService {
	public int register(ReplyVO vo);
	public ReplyVO get(Long rno);
	public int modify(ReplyVO vo);
	public int remove(Long rno);
//	public List<ReplyVO> getList(Criteria cri, Long memoNo);
//	public ReplyPageDTO getListPage(Criteria cri, Long memoNo);
}
