package edu.springz.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import edu.springz.domain.BattleVO;
import edu.springz.domain.BattlesCriteria;
import edu.springz.domain.VoteVO;
import edu.springz.mapper.BattleMapper;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j;

@Service
@Log4j
@AllArgsConstructor
public class BattleServiceImpl implements BattleService{
	private BattleMapper battleMapper;

	@Override
	@Transactional
	public int newBattle(BattleVO battleVO) {
		battleMapper.insertSelectKey(battleVO);
		int battleNo = battleVO.getBattleNo();
		if(battleNo>0) {
			if(battleMapper.inserttBattleVoted(battleNo)) {
				return battleNo;
			}
		}
		return -1;
	}
	
	@Override
	public int getTotalCount(BattlesCriteria cri) {
		return battleMapper.getTotalCount(cri);
	}

	@Override
	public List<BattleVO> getList(BattlesCriteria cri) {
		return battleMapper.getList(cri);
	}
	
	@Override
	public BattleVO getMember(int battleNo) {
		return battleMapper.selectByBattleNo(battleNo);
	}

	@Override
	public boolean hitBattle(int battleNo) {
		return battleMapper.updateViewCount(battleNo);
	}

	@Override
	@Transactional
	public Map<String, String> vote(VoteVO voteVO) {
		HashMap<String, String> map = new HashMap<String, String>();
		if(battleMapper.updatetBattleVoted(voteVO)) {
			if(battleMapper.updateVote(voteVO)) {
				map.put("success", "투표가 완료되었습니다.");
			}
		} else {
			map.put("error", "투표 결과 저장 실패");
		}
		return map;
	}
	

}
