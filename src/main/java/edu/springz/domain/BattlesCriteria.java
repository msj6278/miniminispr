package edu.springz.domain;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BattlesCriteria {
	private int pageNum;
	private int amount;
	private String userId;
	private String title;
	private String content;
	private Date startRegistDate;
	private Date endRegistDate;
	private Date startUpdateDate;
	private Date endUpdateDate;
	
	//기본 페이지 번호 1, 페이지당 6개
	public BattlesCriteria( ) {
		this(1, 6);	
	}
	
	public BattlesCriteria(int pageNum, int amount) {
		this.pageNum = pageNum;
		this.amount = amount;
	}
}


