package edu.springz.domain;

import lombok.Data;

@Data
public class VoteVO {
	private String userId;
	private int    battleNo;
	private String vote;
}
