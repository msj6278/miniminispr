package edu.springz.domain;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class MemberVO {
	private String	userId;
	private String	userPassword;
	private String	userName;
	private String	email;
	private char 	gender;
	private String	photo;
	private String	intro;
	private Date	registDate;
	private Date	updateDate;
	private List<AuthVO> authList;
}
