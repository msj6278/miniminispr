package edu.springz.domain;

import lombok.Data;

@Data
public class MemoAttachVO {
	private String uuid;
	private String uploadPath;
	private String fileName;
	private boolean fileType;
	private Long memoNo;
}
