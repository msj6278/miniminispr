package edu.springz.domain;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class MemoVO {
	private List<MemoAttachVO> attachList;
	private Long	memoNo;
	private String	userId;
	private String	content;
	private Date	rDate;
	private int		replyCnt;
}
