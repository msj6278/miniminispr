package edu.springz.domain;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BattleVO {
	private int	   battleNo;
	private String userId;
	private String title;
	private String content;
	private String image1;
	private String image2;
	private int    select1Count;
	private int    select2Count;
	private int    viewCount;
	private Date   registDate;
	private Date   updateDate;
}
