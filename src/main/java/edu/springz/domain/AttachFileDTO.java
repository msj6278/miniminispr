package edu.springz.domain;

import lombok.Data;

@Data
public class AttachFileDTO {
	private String fileName;		//실제 파일명 UUID+fileName
	private String uploadFolder;	//루트 업로드 경로 제외한 업로드 폴더
	private String uuid;
	private Boolean image;
}
