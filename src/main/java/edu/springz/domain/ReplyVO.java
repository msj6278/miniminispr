package edu.springz.domain;

import java.util.Date;

import lombok.Data;

@Data
public class ReplyVO {
	private Long replyNo;
	private Long memoNo;
	private String reply;
	private String replyer;
	private Date replyDate;
	private Date updateDate;
}
