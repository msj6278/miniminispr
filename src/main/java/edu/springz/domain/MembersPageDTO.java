package edu.springz.domain;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class MembersPageDTO {
	private int startPage;
	private int endPage;
	private int realEndPage;
	private boolean prev, next;
	
	private int total;
	private MembersCriteria cri;
	
	public MembersPageDTO(MembersCriteria cri, int total) {	//total; 총 테이블 row 수, ...
		this.realEndPage = (int) (Math.ceil((total * 1.0) / cri.getAmount()));	//실제 끝 페이지 계산
		
		if(cri.getPageNum() > this.realEndPage) {
			cri.setPageNum(this.realEndPage);
		} else if(cri.getPageNum() < 1) {
			cri.setPageNum(1);
		}
		
		this.cri = cri;
		this.total = total;
		
		this.endPage = (int) (Math.ceil(cri.getPageNum()/10.0)) * 10;	//끝페이지 계산
		this.startPage = this.endPage - 9;								//시작 페이지 계산
		
		if(this.endPage > this.realEndPage) {				//끝 페이지가 실제 페이지보다 큰 경우
			this.endPage = this.realEndPage; 
		}
		
		this.prev = this.startPage > 1;			//이전 페이지 여부; boolean 값으로 활성 여부
		this.next = this.endPage < this.realEndPage;		//다음 페이지 여부
	}
	
}
