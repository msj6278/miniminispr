package edu.springz.domain;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MembersCriteria {
	private int pageNum;
	private int amount;
	private String userId;
	private String userName;
	private String email;
	private String gender;
	private Date startRegistDate;
	private Date endRegistDate;
	private Date startUpdateDate;
	private Date endUpdateDate;
	
	public MembersCriteria( ) {
		this(1, 10);	//기본 페이지 번호는 1번, 갯수는 10개로 설정
	}
	
	public MembersCriteria(int pageNum, int amount) {
		this.pageNum = pageNum;
		this.amount = amount;
	}
}


