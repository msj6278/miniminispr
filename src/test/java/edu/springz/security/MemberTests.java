package edu.springz.security;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import lombok.Setter;
import lombok.extern.log4j.Log4j;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"file:src/main/webapp/WEB-INF/spring/root-context.xml",
					"file:src/main/webapp/WEB-INF/spring/security-context.xml"})
public class MemberTests {
	@Setter(onMethod_= {@Autowired})
	private PasswordEncoder pwEncoder;
	@Setter(onMethod_= {@Autowired})
	private DataSource ds;
	
	//@Test
	public void testInsertMember() {
		String query ="insert into t_member(user_id, user_nm, user_pw, email1, email2, gender, intro, photo, reg_date) values(?, ?, ?)";
		try(Connection con = ds.getConnection();
			PreparedStatement pstmt = con.prepareStatement(query)) {
			for(int i=0; i<100; i++) {
				pstmt.setString(2, pwEncoder.encode("pw"+i));
				if(i<80) {
					pstmt.setString(1, "user"+i);
					pstmt.setString(3, "일반사용자"+i);
				} else if(i<90) {
					pstmt.setString(1, "manager"+i);
					pstmt.setString(3, "운영자"+i);
				} else {
					pstmt.setString(1, "admin"+i);
					pstmt.setString(3, "관리자"+i);
				}
				pstmt.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}
	
	//@Test
	public void testInsertAuth() {
		String query ="insert into t_member_auth(user_id, auth) values(?, ?)";
		try(Connection con = ds.getConnection();
			PreparedStatement pstmt = con.prepareStatement(query)) {
			for(int i=0; i<100; i++) {
				if(i<80) {
					pstmt.setString(1, "user"+i);
					pstmt.setString(2, "ROLE_USER");
				} else if(i<90) {
					pstmt.setString(1, "manager"+i);
					pstmt.setString(2, "ROLE_MEMBER");
				} else {
					pstmt.setString(1, "admin"+i);
					pstmt.setString(2, "ROLE_ADMIN");
				}
				pstmt.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}
	
	@Test
	public void testInsertMember2() {
		String query ="insert into t_member(user_id, user_name, user_password, email, gender, intro, photo, regist_date, update_date, enabled) values(?, ?, ?)";
		try(Connection con = ds.getConnection();
			PreparedStatement pstmt = con.prepareStatement(query)) {
			for(int i=0; i<100; i++) {
				pstmt.setString(2, pwEncoder.encode("pw"+i));
				if(i<80) {
					pstmt.setString(1, "user"+i);
					pstmt.setString(3, "일반사용자"+i);
				} else if(i<90) {
					pstmt.setString(1, "manager"+i);
					pstmt.setString(3, "운영자"+i);
				} else {
					pstmt.setString(1, "admin"+i);
					pstmt.setString(3, "관리자"+i);
				}
				pstmt.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}
	
}
