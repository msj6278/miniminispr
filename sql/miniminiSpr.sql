DROP TABLE t_member
DROP TABLE t_join_verify

CREATE TABLE t_member(
    user_id VARCHAR2(15),
    user_name VARCHAR2(30),
    user_password VARCHAR2(64),
    email   VARCHAR2(320),
    gender  CHAR(1),
    photo   VARCHAR2(48 CHAR) DEFAULT null,
    intro   VARCHAR2(100 CHAR) DEFAULT null,
    regist_date DATE DEFAULT SYSDATE,
    update_date DATE DEFAULT SYSDATE
)

DROP TABLE t_member

CREATE TABLE t_join_verify(
    user_id       VARCHAR2(15),
    join_code     VARCHAR2(44),
    send_date   DATE
)--DATE 만료시 데이터 삭제

CREATE TABLE t_member_auth(
    user_id       VARCHAR2(15),
    auth     VARCHAR2(15)
)--DATE 만료시 데이터 삭제

DROP TABLE t_member

INSERT INTO t_join_verify(user_id, regist_date) VALUES('userId', SYSDATE)


INSERT INTO t_member(user_id, user_name, user_password, email, gender,
		intro, photo, regist_date, update_date, enabled)
		VALUES('aaa', '가가', 'fsadfsdf', 'sdf@gamcil.com', 'm',
		'2131232', 'img', SYSDATE, SYSDATE, 'a')
        
CREATE TABLE t_memo(
    memo_no NUMBER(10),
    user_id VARCHAR2(15),
    content VARCHAR2(100),
    r_date  DATE,
    reply_cnt NUMBER(10)
    )
    
ALTER TABLE t_member_auth MODIFY (auth VARCHAR2(15))

CREATE TABLE t_table(
    datee date CHECK(datee<SYSDATE)
)

DECLARE
    date1 DATE := SYSDATE-3;
    i NUMBER:= 1;
BEGIN 
    WHILE SYSDATE > date1
    LOOP
        INSERT INTO t_member VALUES('user'||i, '홍길동'||1, 'pw'||i,
            'user'||i||'@gmail.com', 'm', '자기소개', '', date1, date1);
        date1 := date1 + dbms_random.value(0, 60*60)/(24*60*60);
        i := i+1;
        EXIT WHEN SYSDATE < date1;
    END LOOP;
END;

SELECT user_id, user_name, email, gender,
       intro, photo, regist_date, update_date
FROM   t_member
WHERE  user_id = ${userId}

SELECT * 
FROM t_member 
WHERE user_id Like '%a%' 
OR user_name Like '%홍길%'
OR email Like '%10%'
--345678901234567890123456789012345678901234567890


SELECT * FROM (SELECT /*+INDEX(t_reply t_reply_idx) */ 
		rownum rn, user_id, user_name, email, gender, regist_date, update_date
		FROM	t_member
		WHERE   (user_id LIKE '%a' OR user_name LIKE '%길%') AND gender='m' AND regist_date > '20/01/09'
        AND rownum <= 1 * 100 ORDER BY regist_date ASC)
WHERE   rn > (0 * 100)

SELECT * FROM (SELECT rownum rn, 
			battle_no, user_id, title, content, image1, image2,
			select1_count, select2_count, view_count, regist_date, update_date FROM (SELECT * FROM t_battle ORDER BY regist_date ASC)
		WHERE rownum <= 1 * 10)
WHERE   rn > (0 * 100)




UPDATE t_member_auth
SET auth = 'abc'
WHERE user_id = '1234a'

SELECT m.user_id, user_name, email, gender,
    intro, photo, regist_date, update_date, auth
FROM t_member m, t_member_auth a
WHERE m.user_id = a.user_id AND a.user_id = 'user95'

CREATE TABLE t_battle(
    battle_no  NUMBER(10, 0) PRIMARY KEY,
    user_id    VARCHAR2(50),
    title      VARCHAR2(200),
    content    VARCHAR2(50),
    image1     VARCHAR2(50),
    image2     VARCHAR2(50),
    vote1      NUMBER(10, 0),
    vote2      NUMBER(10, 0),
    view_count NUMBER(10, 0),
    regist_date DATE DEFAULT SYSDATE,
    update_date DATE DEFAULT SYSDATE
    )

ALTER TABLE t_battle RENAME COLUMN select1_count TO vote1

ALTER TABLE t_battle RENAME COLUMN select2_count TO vote2

DROP TABLE t_battle

CREATE SEQUENCE t_battle_seq

SELECT t_battle_seq.nextval FROM dual

DECLARE
    date1 DATE := SYSDATE-3;
    i NUMBER:= 1;
BEGIN 
    WHILE SYSDATE > date1
    LOOP
        INSERT INTO t_battle VALUES(t_battle_seq.nextval, 'user'||i, 'title'||i, 'contentcontent'||i,
            'image1_'||i||'.png', 'image2_'||i||'.png', 0, 0, 0, date1, SYSDATE);
        date1 := date1 + dbms_random.value(0, 60*60)/(24*60*60);
        i := i+1;
        EXIT WHEN SYSDATE < date1;
    END LOOP;
END;

DROP TABLE t_battle

CREATE TABLE CLOB_TBL( col1 CLOB, battle_no NUMBER(10) )

INSERT INTO CLOB_TBL VALUES('a')

DROP TABLE clob_tbl

SELECT col1 FROM CLOB_TBL WHERE col1 LIKE '%com%'
SELECT col1 FROM CLOB_TBL WHERE userid='user01'
--LOB DATA의 경우 LIKE 검색보다 DBMS_LOB 함수를 사용하는게 성능상 이점
--SELECT col1 FROM CLOB_TBL WHERE col1 LIKE '%com%'
SELECT col1 FROM CLOB_TBL WHERE DBMS_LOB.INSTR(col1, 'com') > 0

UPDATE CLOB_TBL
SET col1 = CONCAT(col1,'user02|')
WHERE battle_no = '1' AND DBMS_LOB.INSTR(col1, '1') > 0

CREATE TABLE t_battle_voted(battle_no NUMBER(10), user_ids CLOB)

DROP TABLE t_battle_selected

INSERT INTO t_battle_selected(battle_no, user_ids) values ('157', '|')

SELECT * FROM t_battle_selected

--selected 여부 반환
UPDATE t_battle_voted
SET user_ids = CONCAT(user_ids, 'user02|')
WHERE battle_no = '157' AND DBMS_LOB.INSTR(user_ids, 'user02') = 0

SELECT battle_no FROM t_battle_selected WHERE DBMS_LOB.INSTR(user_ids, '157') = 0

ALTER TABLE t_battle_selected RENAME COLUMN userid TO user_ids

ALTER TABLE t_battle MODIFY (image1 VARCHAR2(200))

ALTER TABLE t_battle MODIFY (image2 VARCHAR2(200))

UPDATE t_battle_voted SET user_ids = CONCAT(user_ids, 'user01'||'|') WHERE battle_no = 216 AND 
DBMS_LOB.INSTR(user_ids, 'user01') = 0

DROP TABLE t_battle_selected

ALTER TABLE t_member MODIFY (photo VARCHAR2(100))

SELECT COUNT(*) FROM t_member_auth WHERE user_id = 'user01122' AND auth != 'ROLE_UNVERIFIED'