@C:\javawork\public\test_data_eng.sql
@C:\javawork\public\emp.sql

SELECT * FROM emp
SELECT empno, ename FROM emp
SELECT empno 사원번호, 
       ename "사원 이름", 
       ename AS "사원's 이름" 
SELECT empno 사원번호, 
       ename "사원 이름", 
       ename AS "사원's 이름" 
FROM   emp

SELECT empno || ' : ' || ename as "사원번호 : 이름"
FROM   emp

SELECT * 
FROM   emp
WHERE  empno = 7900

SELECT * FROM   emp  WHERE  job = 'CLERK'
SELECT * FROM   emp  WHERE  job = 'clerk'
SELECT * FROM   emp  WHERE  sal >= 2000
SELECT * FROM   emp  WHERE  hiredate = '81/02/20'
SELECT * FROM   emp  WHERE  hiredate < '81/01/01'


SELECT * FROM   emp  WHERE  hiredate >= '81/01/01'
                     AND    hiredate <= '81/12/31'
SELECT * FROM   emp  WHERE  hiredate 
                     BETWEEN '81/01/01' AND    '81/12/31'
SELECT * FROM   emp  WHERE  hiredate LIKE '81%'
                     
SELECT * FROM   emp  WHERE sal >= 2000 AND sal <= 3000
SELECT * FROM   emp  WHERE sal BETWEEN 2000 AND 3000

SELECT * FROM   emp  WHERE ename BETWEEN 'KING' AND 'SMITH' 
SELECT * FROM   emp  WHERE ename BETWEEN 'KING' AND 'SMITH' ORDER BY ename 
SELECT * FROM   emp  WHERE ename BETWEEN 'KING' AND 'SMITH' ORDER BY 2 

SELECT * FROM   emp  WHERE job='CLERK' OR job='MANAGER' ORDER BY job ASC, sal DESC
SELECT * FROM   emp  WHERE job='CLERK' OR job='MANAGER' ORDER BY 3, 6
SELECT * FROM   emp  WHERE job IN('CLERK', 'MANAGER')   ORDER BY 3, 6 

SELECT * FROM   emp  WHERE ename LIKE 'J%'
SELECT * FROM   emp  WHERE ename LIKE '_ _ _ D'

SELECT * FROM employees WHERE phone_number LIKE '5__.123.%'
SELECT * FROM employees WHERE job_id LIKE '%R_R%' 
SELECT * FROM employees WHERE job_id LIKE '%R\_R%' ESCAPE '\'

SELECT * FROM emp  WHERE comm IS NULL
SELECT * FROM emp  WHERE comm IS NOT NULL

SELECT empno, ename, sal, comm, sal + comm bonus 
FROM   emp 
WHERE  comm IS NOT NULL AND comm > 0



emp 테이블에서 job이 SALESMAN이  아닌 사람들의
ename, job, sal 출력
단, job을 기준으로 오름차순 정렬하되
    같은 job의 경우 sal가 큰 순서로 출력
SELECT ename, job, sal FROM emp 
WHERE job NOT LIKE 'SALESMAN'
ORDER BY 2, 3 DESC

SELECT studno, name, deptno1, 1
FROM   student
WHERE  deptno1 = 101
UNION  ALL
SELECT profno, name, deptno, 2
FROM   professor
WHERE  deptno = 101

SELECT studno, name, deptno1, 1
FROM   student
WHERE  deptno1 = 101
UNION  
SELECT profno, name, deptno, 2
FROM   professor
WHERE  deptno = 101
 
SELECT studno, name
FROM   student
WHERE  deptno1 = 101
INTERSECT
SELECT studno, name
FROM   student
WHERE  deptno2 = 201

SELECT empno, ename, sal FROM emp 
MINUS
SELECT empno, ename, sal FROM emp WHERE sal > 2500


SELECT ename, INITCAP(ename), LOWER(job), UPPER(LOWER(job))
FROM   emp

SELECT SYSDATE FROM dual


SELECT 'KIM', LENGTH('KIM'), LENGTHB('KIM'), 
       '김',  LENGTH('김'), LENGTHB('김')
FROM   dual

SELECT ename, LENGTH(ename) || '자' 
FROM   emp
ORDER BY 2 DESC

SELECT ename, CONCAT(LENGTH(ename), '자')
FROM   emp
ORDER BY 2 DESC

SELECT 'abcdef', SUBSTR('abcdef',  3, 2),
                 SUBSTR('abcdef', -3, 2),
                 SUBSTR('abcdef',  3, 4),
                 SUBSTR('abcdef', -3, 4)
FROM   dual

SELECT name, SUBSTR(jumin, 1, 6) id_no, 
             SUBSTR(jumin, 1, 2) "year",
             SUBSTR(jumin, 3, 2) "month",
             SUBSTR(jumin, 5, 2) "date",
             SUBSTR(jumin, 5, 2) - 1 "before birthday" 
FROM   student
WHERE  deptno1 = 101

SELECT ename, hiredate
FROM   emp
WHERE  SUBSTR(hiredate, 5, 1) = '9'


SELECT '이것은', SUBSTR('이것은', 1, 2),
                SUBSTRB('이것은', 1, 3)
FROM   dual

SELECT 'A-B-C-D', INSTR('A-B-C-D', '-', 1, 3)  "1,3",
                  INSTR('A-B-C-D', '-', 3, 1) "3,1",
                  INSTR('A-B-C-D', '-', -1, 3) "-1,3",
                  INSTR('A-B-C-D', '-', -6, 2) "6,2"
FROM   dual

SELECT hiredate, INSTR(hiredate, '/', 1, 1)
FROM   emp


SELECT url, INSTR(url, '/', 1, 2), INSTR(url, '/', 1, 3)
FROM   t_url

SELECT url, SUBSTR(url, 
                   INSTR(url, '/', 1, 2) + 1,
                   INSTR(url, '/', 1, 3) -1 - INSTR(url, '/', 1, 2) )
FROM   t_url
 
 
SELECT name, tel, SUBSTR(tel, 1, INSTR(tel, ')') - 1) "AREA",
                  SUBSTR(tel, INSTR(tel, ')') + 1) "PHONE"
FROM   student
WHERE  deptno1 = 201

SELECT name, id, LPAD(id, 10, '*'),
                 RPAD(id, 10, '*')
FROM   student
WHERE  deptno1 = 201

SELECT ename, LPAD(ename, 9, '123456789') LPAD ,
              RPAD(ename, 9, SUBSTR('123456789', 
                                    LENGTH(ename) + 1)) RPAD
FROM   emp
WHERE  deptno = 10

SELECT ename, LTRIM(ename, 'K'),
              RTRIM(ename, 'K')
FROM   emp
WHERE  deptno = 10

SELECT empno, LTRIM(empno, 7),
              RTRIM(empno, 9)
FROM   emp

SELECT ename, 
       REPLACE(ename, 'K', '*'),
       REPLACE(ename, SUBSTR(ename, 1, 2), '*'),
       REPLACE(ename, SUBSTR(ename, 1, 2), '**'),
       REPLACE(ename, SUBSTR(ename, 1, 2), '***')
FROM   emp
WHERE  deptno = 10


--REPLACE QUIZ. 1
SELECT ename, REPLACE(ename, SUBSTR(ename, 2, 2), '--')
FROM   emp
WHERE  deptno = 20


--REPLACE QUIZ. 2
SELECT name, JUMIN, REPLACE(jumin, SUBSTR(jumin, 7), '-/-/-/-')
FROM   student
WHERE  deptno1 = 101


--REPLACE QUIZ. 3
SELECT name, tel, REPLACE(tel, 
                          SUBSTR(tel, INSTR(tel, ')') + 1, 3), 
                          '***')
FROM   student
WHERE  deptno1 = 102

--REPLACE QUIZ. 4
SELECT name, tel, REPLACE(tel, 
                          SUBSTR(tel, INSTR(tel, '-') + 1), 
                          '****')
FROM   student
WHERE  deptno1 = 101



SELECT '987.654', ROUND(987.654),
                  ROUND(987.654,  0),
                  ROUND(987.654,  1),
                  ROUND(987.654, -1),
                  ROUND(987.654,  2),
                  ROUND(947.654, -2)
FROM   dual

SELECT ename, sal, sal * 0.33333 "bonus",
                   ROUND(sal * 0.33333, 2) "bonus i",
                   RPAD(ROUND(sal * 0.33333, 2), 6, '.00') "bonus ii",
                   ROUND(sal * 0.33333, -2) "bonus iii"
FROM   emp
WHERE  ROUND(sal * 0.33333, 2) < 1000



SELECT '987.654', TRUNC(987.654) ,
                  TRUNC(987.654,  0),
                  TRUNC(987.654,  1),
                  TRUNC(987.654, -1),
                  TRUNC(987.654,  2),
                  TRUNC(987.654, -2)
FROM   dual


SELECT ename, hiredate,TRUNC((SYSDATE - hiredate)/365),
                       (SYSDATE - hiredate)/365, 
                       SYSDATE - hiredate
FROM  emp


SELECT MOD(121, 10), 
       CEIL(123.456), FLOOR(123.456), 
       CEIL(123.667), FLOOR(123.667)
FROM   dual

SELECT ROWNUM, ename, CEIL(ROWNUM/3), ROWNUM/3
FROM   emp
ORDER BY ename

SELECT POWER(2, 8)
FROM   dual

SELECT SYSDATE, SYSTIMESTAMP
FROM   dual


ALTER SESSION SET NLS_DATE_FORMAT='RRRR-MM-DD HH24:MI:SS'

SELECT SYSDATE, SYSTIMESTAMP
FROM   dual

SELECT MONTHS_BETWEEN('20/01/31', '19/09/17')
FROM   dual


SELECT first_name, hire_date,
             MONTHS_BETWEEN(SYSDATE, hire_date),
       ROUND(MONTHS_BETWEEN(SYSDATE, hire_date), 2),
       ROUND((SYSDATE-hire_date)/365*12, 2)
FROM   employees


SELECT hiredate, ADD_MONTHS(hiredate, 6)
FROM   emp

SELECT SYSDATE, NEXT_DAY(SYSDATE, '금'),
                NEXT_DAY(SYSDATE, '금요일'),
                LAST_DAY(SYSDATE)
FROM   dual

SELECT SYSDATE, ROUND(SYSDATE), TRUNC(SYSDATE)
FROM   dual


-------------------- 형 변환 ----------------------
SELECT  1 + 1, 
        1 + '1',
        1 + TO_NUMBER('1')
FROM   dual


SELECT SYSDATE, TO_CHAR(SYSDATE, 'YYYY.MM.DD HH24:MI:SS') AS YYYY,
                TO_CHAR(SYSDATE, 'RRRR.MON.DAY HH:MI:SS') AS RRRR,
                TO_CHAR(SYSDATE, 'YY.MONTH.DDTH')   AS YY,
                TO_CHAR(SYSDATE, 'RR')   AS RR,
                TO_CHAR(SYSDATE, 'YEAR') AS YEAR
FROM   dual


SELECT ename, hiredate, TO_CHAR(hiredate, 'yyyy.MM.dd') hiredate2
FROM   emp

-- QUIZ. 1
SELECT studno, name, birthday
FROM   student
WHERE  TO_CHAR(birthday, 'MM') = '01'
 
-- QUIZ. 2
SELECT empno, ename, hiredate
FROM   emp
WHERE  TO_CHAR(hiredate,'MM') IN ('01', '02', '03')


SELECT empno, ename, sal, comm, 
       TO_CHAR((sal*12)+comm,   '9,999' ) SALARY1,
       TO_CHAR((sal*12)+comm,  '99,999' ) SALARY2,
       TO_CHAR((sal*12)+comm, '999,999' ) SALARY3,
       (sal*12)+comm
FROM   emp
WHERE  ename = 'ALLEN'

SELECT name, pay, bonus, 
       TO_CHAR((pay*12) + bonus, '999,999.99') TOTAL1, 
       TO_CHAR((pay*12) + bonus, '000,000.00') TOTAL2
FROM   professor
WHERE  deptno = 201


-- QUIZ. 3
SELECT empno, ename, 
       TO_CHAR(hiredate, 'yyyy-MM-dd') hiredate,
       TO_CHAR( sal*12+comm, '$999,999') sal,
       TO_CHAR((sal*12+comm)*1.15, '$999,999') "15% UP"
FROM   emp
WHERE  comm IS NOT NULL


SELECT TO_NUMBER('20190917')- 20190909  FROM   dual

SELECT TO_DATE('20190917', 'yyyy/MM/dd')FROM   dual
SELECT TO_DATE('20190917', 'rr/MM/dd')FROM   dual

SELECT TO_CHAR(SYSDATE, 'YYYY')         FROM   dual

SELECT TO_CHAR(123456, '999,999.99')    FROM   dual

-----------------------------------
SELECT ename, sal, comm, 
       sal + comm AS bonus,
       sal + NVL(comm, 0) AS bonus2
FROM   emp

SELECT ename, NVL(mgr, 9999)
FROM   emp

SELECT deptno, name, pay, bonus,
       pay * 12 + bonus AS total1,
       TO_CHAR(pay * 12 + NVL(bonus, 0), '999,999') AS total2
FROM   professor
WHERE  deptno = 201

SELECT deptno, ename, sal, comm,
       sal + comm,
       sal + NVL(comm, 0) AS nvl,
       NVL2(comm, sal + comm, sal * 0) AS nvl2
FROM   emp
WHERE  deptno = 30

SELECT deptno, ename, sal, comm,
       NVL2(comm, 'Exist', 'Null') AS nvl2
FROM   emp
WHERE  deptno = 30


SELECT deptno, name, 
       DECODE(deptno, 101, 'Computing', '-') AS dname   
FROM   professor


SELECT deptno, name, 
       DECODE(deptno, 101, 'Computing') AS dname0,
       DECODE(deptno, 101, 'Computing', '-') AS dname1,
       DECODE(deptno, 101, 'Computing', 
                      102, 'Multimedia',
                      103, 'Software',
                           'ETC') AS dname2
FROM   professor


SELECT deptno, name, 
       DECODE(deptno, 101, DECODE(name, 'Audie Murphy', 'Best!', 'Good'), 'N/A')
FROM   professor


SELECT deptno1, name, jumin, 
       DECODE(SUBSTR(jumin, 7, 1), 1, '남자', '여자') AS gender
FROM   student
WHERE  deptno1 = 101
 
SELECT deptno1, name, tel, 
       DECODE(SUBSTR(tel, 1, INSTR(tel, ')') -1), 
              02, 'SEOUL',   031, 'GYEONGGI',
              051, 'BUSUAN', 052, 'ULSAN',
              055, 'GYEONGNAM') LOCATION
FROM   student
WHERE  deptno1 = 101       

SELECT deptno1, name, tel, 
       CASE SUBSTR(tel, 1, INSTR(tel, ')') -1)
             WHEN '02'  THEN 'SEOUL'
             WHEN '031' THEN 'GYEONGGI'
             WHEN '051' THEN 'BUSUAN'
             WHEN '052' THEN 'ULSAN'
             WHEN '055' THEN 'GYEONGNAM'
             ELSE 'ETC'
       END LOCATION
FROM   student
WHERE  deptno1 = 201    

SELECT name, SUBSTR(jumin, 3, 2) month,
       CASE WHEN SUBSTR(jumin, 3, 2) BETWEEN '01' AND '03' THEN '1/4'
            WHEN SUBSTR(jumin, 3, 2) BETWEEN '04' AND '06' THEN '2/4'
            WHEN SUBSTR(jumin, 3, 2) BETWEEN '07' AND '09' THEN '3/4'
            WHEN SUBSTR(jumin, 3, 2) BETWEEN '10' AND '12' THEN '4/4'
       END  QUATER
FROM   student

SELECT empno, ename, sal, 
       CASE WHEN sal BETWEEN    1 AND 1000 THEN 'LEVEL1'
            WHEN sal BETWEEN 1001 AND 2000 THEN 'LEVEL2'
            WHEN sal BETWEEN 2001 AND 3000 THEN 'LEVEL3'
            WHEN sal BETWEEN 3001 AND 4000 THEN 'LEVEL4'
            WHEN sal > 4000                THEN 'LEVEL5'
--          ELSE                                'LEVEL5'
       END "LEVEL"
FROM emp
ORDER BY 3              


-----------------------정규표현식

SELECT *  
FROM   t_reg
WHERE  REGEXP_LIKE(text, '[a-z]')

SELECT * 
FROM   t_reg
WHERE  REGEXP_LIKE(text, '[[:lower:]]')


SELECT * 
FROM   t_reg
WHERE  REGEXP_LIKE(text, '[A-Z]')

SELECT * 
FROM   t_reg
WHERE  REGEXP_LIKE(text, '[[:upper:]]')


SELECT * 
FROM   t_reg
WHERE  REGEXP_LIKE(text, '[a-zA-Z]')

SELECT * 
FROM   t_reg
WHERE  REGEXP_LIKE(text, '[[:alpha:]]')

SELECT * FROM   t_reg
WHERE    REGEXP_LIKE(text, '[a-z] ')

SELECT * FROM   t_reg
WHERE    REGEXP_LIKE(text, '[a-z] [0-9]')
SELECT * FROM   t_reg
WHERE    REGEXP_LIKE(text, '[[:lower:]] [[:digit:]]')


SELECT * FROM   t_reg
WHERE    REGEXP_LIKE(text, ' ')
SELECT * FROM   t_reg
WHERE    REGEXP_LIKE(text, '[[:space:]]')

SELECT * 
FROM   t_reg
WHERE  REGEXP_LIKE(text, '[A-Z]{2}')


SELECT * 
FROM   t_reg
WHERE  REGEXP_LIKE(text, '[0-9]{4}')

SELECT * 
FROM   t_reg
WHERE  REGEXP_LIKE(text, '[A-Z][0-9]{3}')
SELECT * 
FROM   t_reg
WHERE  REGEXP_LIKE(text, '[0-9][A-Z]{3}')

SELECT * 
FROM   t_reg
WHERE  REGEXP_LIKE(text, '[0-9][a-z]{3}')

SELECT * 
FROM   t_reg
WHERE  REGEXP_LIKE(text, '^[a-zA-Z]')

SELECT * 
FROM   t_reg
WHERE  REGEXP_LIKE(text, '^[A-Z0-9]')

SELECT * 
FROM   t_reg
WHERE  REGEXP_LIKE(text, '^[A-Z]|^[0-9]')

SELECT * 
FROM   t_reg
WHERE  REGEXP_LIKE(text, '[A-Z0-9]$')

SELECT name, id FROM student
WHERE  REGEXP_LIKE(id, '^M(a|o)')


SELECT * FROM   t_reg
WHERE    REGEXP_LIKE(text, '^[^a-z]')

SELECT * FROM   t_reg
WHERE    REGEXP_LIKE(text, '[^a-z]')

SELECT * FROM   t_reg
WHERE    NOT REGEXP_LIKE(text, '[a-z]')

SELECT name, tel FROM student
WHERE  REGEXP_LIKE(tel, '^[0-9]{2}\)[0-9]{4}')

SELECT name, id FROM student
WHERE  REGEXP_LIKE(id, '^...r')


SELECT * FROM t_reg2
WHERE  REGEXP_LIKE(ip, '^[10]{2}\.[10]{2}\.[10]{2}\.')

SELECT * FROM t_reg2
WHERE  REGEXP_LIKE(ip, '^[172]{3}\.[16]{2}\.[168]{3}')

SELECT * FROM t_reg2
WHERE  ip LIKE '172.16.168%'


SELECT text, REGEXP_REPLACE(text, '[0-9]', '*') replaced
FROM t_reg


SELECT text, REGEXP_REPLACE(text, '[0-9]', '\1-*') replaced1,
             REGEXP_REPLACE(text, '([0-9])', '\1-*') replaced2
FROM t_reg


SELECT ip, REGEXP_REPLACE(ip, '\.', '') replaced1,
           REGEXP_REPLACE(ip, '\.', '/') replaced2,
           REGEXP_REPLACE(ip, '\.', '/', 1, 1) replaced3,
           REGEXP_REPLACE(ip, '\.', '/', 1, 2) replaced4
FROM t_reg2


SELECT 'aaa bbb', 
       REGEXP_REPLACE('aaa bbb ccc   ddd', '( ){1,}', '') re1,  
       REGEXP_REPLACE('aaa bbb', '( ){1}', '') re2,
       REGEXP_REPLACE('aaa bbb', '( ){2}', '') re3,
       REGEXP_REPLACE('aaa  bbb', '( ){2}', '') re4,
       REGEXP_REPLACE('aaa   bbb', '( ){2,4}', '') re5
FROM   dual

SELECT studno, name, id  FROM student
WHERE  id = LOWER(REGEXP_REPLACE('&id', '( ){1,}', ''))

SELECT '20190918', 
        REGEXP_REPLACE('20190918',  
                       '([0-9]{4})([0-9]{2})([0-9]{2})',
                       '\1-\2-\3')
FROM   dual               


SELECT REGEXP_SUBSTR('abc *def ghidef', 
                     '[^ ]+[def]')          FROM   dual  
                     
SELECT REGEXP_SUBSTR(' def ghi', 
                     '[^ ]+[def]')       sub0,
       REGEXP_SUBSTR('abc def ghi', 
                     '[^ ]+[def]', 1, 1) sub1,
      REGEXP_SUBSTR('abc *def ghidef', 
                     '[^ ]+[def]', 1, 2) sub2   FROM   dual 
                     
                     
SELECT name, hpage,
       REGEXP_SUBSTR(hpage, '/([a-zA-Z0-9]+\.?){3,4}?') substr,
       LTRIM(REGEXP_SUBSTR(hpage, '/([a-zA-Z0-9]+\.?){3,4}?'), '/') ltrim
FROM   professor
WHERE  hpage IS NOT NULL

        
SELECT name, hpage,
       REGEXP_SUBSTR(hpage, '/([a-zA-Z0-9]+\.?){3,4}?') substr,
       LTRIM(REGEXP_SUBSTR(hpage, '/([a-zA-Z0-9]+\.?){3,4}?'), '/') ltrim
FROM   professor
WHERE  hpage IS NOT NULL


SELECT name, email,
       REGEXP_SUBSTR(email, '[^@]+', 1, 1) ID,
       REGEXP_SUBSTR(email, '[^@]+', 1, 2) ID2,
       LTRIM(REGEXP_SUBSTR(email, '@([a-zA-Z0-9]+\.?){3,4}?'), '@') domain
FROM   professor


SELECT text, REGEXP_COUNT(text, 'A')
FROM   t_reg


SELECT text, REGEXP_COUNT(text, 'c', 1), 
             REGEXP_COUNT(text, 'c', 1, 'i')
FROM   t_reg

SELECT text, REGEXP_COUNT(text, 'aa')    , 
             REGEXP_COUNT(text, 'a{2}')  , 
             REGEXP_COUNT(text, '(a)(a)')
FROM   t_reg


----------------------------그룹 함수
SELECT COUNT(*), COUNT(comm)
FROM   emp

SELECT COUNT(*) 
FROM   emp
WHERE  ename = 'JAMES'

SELECT COUNT(*) 
FROM   emp
WHERE  ename = 'JAMES' AND empno = 8000 

SELECT COUNT(*)
FROM   emp
WHERE  job LIKE '%MAN'


SELECT COUNT(comm), SUM(comm), AVG(comm), AVG(NVL(comm, 0)),
       ROUND(AVG(NVL(comm, 0)), 2) avg3
FROM   emp


SELECT MAX(sal), MIN(sal), MAX(hiredate), MIN(hiredate),
       MAX(ename), MIN(ename)
FROM   emp


SELECT STDDEV(sal), VARIANCE(sal)
FROM   emp

SELECT   deptno, job AS 업무, AVG(sal)
FROM     emp
GROUP BY deptno, job, comm
ORDER BY 1

SELECT deptno, AVG(sal)
FROM   emp
WHERE  deptno = 10
GROUP BY deptno
--WHERE  AVG(sal) >= 2000
HAVING  AVG(sal) >= 2000        


SELECT deptno, job, ROUND(AVG(sal), 2) 평균급여, COUNT(*) 사원수
FROM   emp
GROUP BY deptno, job
ORDER BY 1, 2

SELECT deptno, ROUND(AVG(sal), 2) 평균급여, COUNT(*) 사원수
FROM   emp
GROUP BY deptno 
ORDER BY 1

SELECT ROUND(AVG(sal), 2) 평균급여, COUNT(*) 사원수 
FROM   emp


SELECT deptno, job, 
       ROUND(AVG(sal), 2) 평균급여, 
       COUNT(*) 사원수
FROM   emp
GROUP BY ROLLUP(deptno, job)


CREATE TABLE professor2
AS 
SELECT deptno, position, pay
FROM   professor


SELECT deptno, job, ROUND(AVG(sal), 2) 평균급여, COUNT(*) 사원수
FROM   emp
GROUP BY CUBE(deptno, job)
ORDER BY 1

SELECT grade, deptno1, COUNT(*)
FROM   student
GROUP BY GROUPING SETS(grade, deptno1)

SELECT MAX(DECODE(day, 'SUN', dayno)) SUN,
       MAX(DECODE(day, 'MON', dayno)) MON,
       MAX(DECODE(day, 'TUE', dayno)) TUE,
       MAX(DECODE(day, 'WED', dayno)) WED,
       MAX(DECODE(day, 'THU', dayno)) THU,
       MAX(DECODE(day, 'FRI', dayno)) FRI,
       MAX(DECODE(day, 'SAT', dayno)) SAT
FROM   cal
GROUP BY weekno
ORDER BY weekno


SELECT * 
FROM   ( SELECT weekno week, day, dayno
         FROM   cal)
PIVOT  ( MAX(dayno) FOR day IN( 'SUN' "SUN", 'MON' "MON",'TUE' "TUE",
                                'WED' "WED", 'THU' "THU",'FRI' "FRI",
                                'SAT' "SAT")
        )
ORDER BY week


SELECT * 
FROM   ( SELECT deptno, job, empno FROM   emp )
PIVOT  ( COUNT(empno) 
         FOR job IN( 'CLERK'     "CLERK",     'MANAGER' "MANAGER",
                     'PRESIDENT' "PRESIDENT", 'ANALYST' "ANALYST",
                     'SALESMAN'  "SALESMAN") )
ORDER BY deptno

CREATE TABLE emp_pivot
AS  SELECT * 
    FROM   ( SELECT deptno, job, empno FROM   emp )
    PIVOT  ( COUNT(empno) 
             FOR job IN( 'CLERK'     "CLERK",     'MANAGER' "MANAGER",
                         'PRESIDENT' "PRESIDENT", 'ANALYST' "ANALYST",
                        'SALESMAN'  "SALESMAN") )
    ORDER BY deptno

SELECT * FROM emp_pivot
UNPIVOT( empno FOR job IN(CLERK, MANAGER, PRESIDENT, ANALYST, SALESMAN) )



SELECT * 
FROM   ( SELECT deptno, job, sal, empno FROM   emp )
PIVOT  ( COUNT(empno) AS cnt,
         SUM(sal) AS sum
         FOR job IN( 'CLERK'     "CLERK",     'MANAGER' "MANAGER",
                     'PRESIDENT' "PRESIDENT", 'ANALYST' "ANALYST",
                     'SALESMAN'  "SALESMAN") )
ORDER BY deptno


SELECT ename, hiredate, sal,
       LAG(sal, 1, 0) OVER(ORDER BY hiredate) lag
FROM   emp


SELECT ename, hiredate, sal,
       LAG(sal, 2, 0) OVER(ORDER BY hiredate) lag
FROM   emp


SELECT ename, hiredate, sal,
       LAG(sal, 3, 999) OVER(ORDER BY hiredate) lag
FROM   emp

SELECT p_store, p_date, p_code, p_qty, p_total,                         
       LAG(p_total, 1, 0) OVER(ORDER BY p_date) before, 
       p_total - LAG(p_total, 1, 0) OVER(ORDER BY p_date) "+-"
FROM   panmae 
WHERE  p_store = 1000 

SELECT ename, hiredate, sal,
       LEAD(sal, 1, 0) OVER(ORDER BY hiredate) LEAD
FROM   emp

SELECT RANK('SMITH') WITHIN GROUP(ORDER BY ename) 
FROM   emp

SELECT empno, ename, sal,
       RANK() OVER(ORDER BY sal)            RANK,
       RANK() OVER(ORDER BY sal ASC)        "ASC",
       RANK() OVER(ORDER BY sal DESC)       "DESC",
       DENSE_RANK() OVER(ORDER BY sal DESC) "DENSE"
FROM   emp


SELECT ROWNUM, deptno, empno, ename, sal, job,
       RANK() OVER(PARTITION BY deptno, job
                   ORDER BY sal DESC) "PARTITION",
       ROW_NUMBER() OVER(ORDER BY sal DESC) "ROW_NUM"
FROM   emp


SELECT p_store, p_date, p_code, p_qty, p_total,                         
       SUM(p_total) OVER(ORDER BY p_total) TOTAL
FROM   panmae 
WHERE  p_store = 1000 


SELECT p_store, p_date, p_code, p_qty, p_total,                         
       SUM(p_total) OVER(PARTITION BY p_code
                         ORDER BY p_total) TOTAL
FROM   panmae 
WHERE  p_store = 1000 


SELECT p_code, p_store, p_date, p_qty, p_total,                         
       SUM(p_total) OVER(PARTITION BY p_code, p_store
                         ORDER BY p_total) TOTAL
FROM   panmae 


SELECT p_code, p_store, 
       SUM( SUM(p_qty) ) OVER()             "TOTAL_Q",
       p_qty,
       ROUND( RATIO_TO_REPORT(p_qty) OVER() * 100, 2)      "TOTAL_%",
       ROUND( RATIO_TO_REPORT( SUM(p_qty) ) OVER() * 100, 2) "TOTAL_%2"
FROM   panmae
WHERE  p_code = 100
GROUP BY p_code, p_qty, p_store
ORDER BY 2



----------------------------------JOIN
SELECT * FROM emp
SELECT * FROM dept

SELECT * FROM emp, dept
SELECT * FROM emp CROSS JOIN dept

SELECT ename, job, deptno, deptno, dname
FROM   emp, dept
--WHERE  emp.deptno = dept.deptno

SELECT emp.ename, emp.job, emp.deptno, 
       dept.deptno, dept.dname
FROM   emp, dept
WHERE  emp.deptno = dept.deptno

SELECT e.ename, e.job, e.deptno, d.deptno, d.dname
FROM   emp e, dept d
WHERE  e.deptno = d.deptno 

SELECT e.ename, e.job, e.deptno, d.deptno, d.dname
FROM   emp e  JOIN  dept d
ON     e.deptno = d.deptno

SELECT s.name, s.profno, p.name 지도교수
FROM   student s, professor p
WHERE  s.profno = p.profno

SELECT s.name, s.profno, p.name 지도교수
FROM   student s JOIN professor p
ON     s.profno = p.profno

SELECT s.name 학생, d.dname 학과, p.name 지도교수
FROM   student s, professor p, department d
WHERE  s.profno = p.profno
AND    s.deptno1 = d.deptno

SELECT s.name 학생, d.dname 학과, p.name 지도교수
FROM   student s  JOIN  professor p
ON     s.profno = p.profno
JOIN   department d   
ON     s.deptno1 = d.deptno


SELECT s.name, s.deptno1, p.name 지도교수
FROM   student s, professor p
WHERE  s.profno = p.profno      -- 조인 조건
AND    s.deptno1 = 101          -- 검색 조건

SELECT s.name, s.deptno1, p.name 지도교수
FROM   student s  JOIN professor p
ON     s.profno = p.profno      -- 조인 조건
AND    s.deptno1 = 101          -- 검색 조건

SELECT e.employee_id, e.first_name, 
       j.job_title, d.department_name, l.city
FROM   employees e, departments d, jobs j, locations l
WHERE  e.job_id = j.job_id
AND    e.department_id = d.department_id
AND    d.location_id = l.location_id
AND    e.department_id = 110


SELECT c.gname, c.point, g.gname
FROM   customer c, gift g
WHERE  c.point BETWEEN g.g_start AND g.g_end

SELECT c.gname, c.point, g.gname
FROM   customer c  JOIN  gift g
ON     c.point >= g.g_start 
AND    c.point <= g.g_end


SELECT st.name , sc.total, ha.grade
FROM   student st, score sc, hakjum ha
WHERE  st.studno = sc.studno
AND    sc.total BETWEEN ha.min_point AND ha.max_point

SELECT st.name, sc.total, ha.grade
FROM   student st JOIN  score sc
ON     st.studno = sc.studno
JOIN   hakjum ha
ON     sc.total >= ha.min_point 
AND    sc.total <= ha.max_point


SELECT s.name, p.name
FROM   student s, professor p
WHERE  s.profno = p.profno(+)

SELECT s.name, p.name
FROM   student s  LEFT OUTER JOIN professor p
ON     s.profno = p.profno


SELECT s.name, p.name
FROM   student s, professor p
WHERE  s.profno(+) = p.profno

SELECT s.name, p.name
FROM   student s  RIGHT OUTER JOIN professor p
ON     s.profno = p.profno


SELECT s.name, p.name
FROM   student s, professor p
WHERE  s.profno = p.profno(+)
UNION
SELECT s.name, p.name
FROM   student s, professor p
WHERE  s.profno(+) = p.profno

SELECT s.name, p.name
FROM   student s  FULL OUTER JOIN professor p
ON     s.profno = p.profno


SELECT e.empno, e.ename, e.mgr, NVL(m.ename, 'CEO') "mname"
FROM   emp e, emp m
WHERE  e.mgr = m.empno(+)



---------------------------DDL
CREATE TABLE new_table (
   no    NUMBER(3),
   name  VARCHAR2(10),
   birth DATE
)

CREATE TABLE tt02 (
   no       NUMBER(3, 1) DEFAULT 0,
   name     VARCHAR2(10) DEFAULT 'No Name',
   hiredate DATE         DEFAULT SYSDATE
)

INSERT INTO tt02(no) VALUES(1)
INSERT INTO tt02(no, name) VALUES(1, 'Lee')
INSERT INTO tt02 VALUES(1, 'Lee', '19/09/19')
--INSERT INTO tt02 VALUES(1, 'LeeLeeLeeLeeLee', '19/09/19')
--INSERT INTO tt02 VALUES(111.1, 'LEE', '19/09/19')
INSERT INTO tt02 VALUES(11.1, 'LEE', '19/09/19')
ROLLBACK
SELECT * FROM TT02
INSERT INTO tt02 VALUES(11.1, 'LEE', '19/09/19')
COMMIT

CREATE GLOBAL TEMPORARY TABLE temp01(
   no       NUMBER,
   name     VARCHAR2(10))
ON COMMIT delete ROWS

CREATE GLOBAL TEMPORARY TABLE temp02(
   no       NUMBER,
   name     VARCHAR2(10))
ON COMMIT preserve ROWS

INSERT INTO temp01 VALUES(1, 'Han')
INSERT INTO temp02 VALUES(1, 'Han')

SELECT * FROM temp01
SELECT * FROM temp02
COMMIT
SELECT * FROM temp01
SELECT * FROM temp02

DESC user_tables
SELECT table_name, temporary, duration 
FROM   user_tables ORDER BY 1

SELECT temporary, duration 
FROM   user_tables 
WHERE  table_name = 'TEMP01'


SELECT table_name, temporary, duration 
FROM   user_tables 
WHERE  temporary = 'Y' 
ORDER BY 1

CREATE TABLE dept3
AS
SELECT * FROM dept2

CREATE TABLE dept4
AS
SELECT dcode, dname FROM dept2

CREATE TABLE dept5
AS
SELECT * FROM dept2 WHERE 1=2


CREATE TABLE dept6
AS
SELECT dcode, dname 
FROM   dept2 
WHERE  dcode IN (1000, 1001, 1002)

ALTER TABLE dept6
ADD   (location VARCHAR2(10))

ALTER TABLE dept6  ADD   (location2 VARCHAR2(10) DEFAULT 'SEOUL')
ALTER TABLE dept6  RENAME  COLUMN location2 TO loc
RENAME dept6 TO dept7
ALTER TABLE dept7  MODIFY  (loc VARCHAR2(20))
ALTER TABLE dept7  DROP COLUMN loc

SELECT * FROM dept7
TRUNCATE TABLE dept7
DROP TABLE dept7


CREATE TABLE dept77
AS
SELECT * FROM   dept2


DELETE FROM dept77 WHERE  dcode = '0001'
DELETE      dept77 WHERE  dcode = '1000'
DELETE      dept77
TRUNCATE TABLE dept77
DROP TABLE dept77
SELECT * FROM   dept77


CREATE TABLE emp22
AS
SELECT * FROM emp WHERE 1=2

INSERT INTO emp22(empno, ename, job, hiredate, deptno)
VALUES(1, 'Ann', 'PROG', SYSDATE, 10)

ALTER TABLE emp22 READ ONLY
ALTER TABLE emp22 READ WRITE
SELECT * FROM emp22
DROP TABLE emp22
SELECT * FROM emp22

CREATE TABLE vt1(
  col1 NUMBER,
  col2 NUMBER,
  col3 NUMBER GENERATED ALWAYS AS (col1 + col2))

INSERT INTO vt1 VALUES(1, 2, 3)
INSERT INTO vt1(col1, col2) VALUES(1, 2)
SELECT * FROM vt1

UPDATE vt1 
SET    col1 = 50, col2 = 100

UPDATE vt1 
SET    col1 = 1000
WHERE  col1 = 1

ALTER TABLE vt1
ADD   (col4 GENERATED ALWAYS AS (col1*12+col2))

SELECT * FROM vt1

DESC USER_TAB_COLUMNS

SELECT column_name, data_type, data_default
FROM   USER_TAB_COLUMNS
WHERE  table_name = 'VT1'

DESC ALL_TABLES
SELECT table_name, owner FROM ALL_TABLES ORDER BY 2

DESC DBA_TABLES
SELECT table_name, owner FROM DBA_TABLES ORDER BY 2


------------------------------DML
INSERT INTO dept2 VALUES('9000', 'temp_1', '1006', 'temp_area')
INSERT INTO dept2 VALUES('9001', 'temp_2', '1006', 'temp_area')
INSERT INTO dept2(dcode, dname, pdept)
VALUES('9002', 'temp_3', '1006')

SELECT * FROM dept2 ORDER BY 1 DESC

INSERT INTO professor(profno, name, id, position, 
                      pay, hiredate, bonus, deptno)
VALUES(5002, 'James Bond', 'Love_me', '정교수', 
                      510, '2014-10-23', null, null)

SELECT * FROM   professor

CREATE TABLE professor3 AS SELECT * FROM professor WHERE 1= 2

INSERT INTO professor3
SELECT * FROM professor

SELECT * FROM professor3

CREATE TABLE professor4 AS SELECT profno, name, pay FROM professor WHERE 1=2

INSERT INTO professor4
SELECT profno, name, pay FROM   professor WHERE  profno > 4000

SELECT * FROM professor4

CREATE TABLE prof_3(
  profno NUMBER, 
  name   VARCHAR2(25))
  
CREATE TABLE prof_4(
  profno NUMBER, 
  name   VARCHAR2(25))
  
INSERT ALL 
    WHEN profno BETWEEN 1000 AND 1999 THEN INTO prof_3 VALUES(profno, name)
    WHEN profno BETWEEN 2000 AND 2999 THEN INTO prof_4 VALUES(profno, name)
SELECT profno, name FROM professor

INSERT ALL 
         INTO prof_3 VALUES(profno, name)
         INTO prof_4 VALUES(profno, name)
SELECT profno, name FROM professor
WHERE  profno BETWEEN 3000 AND 3999



UPDATE professor
SET    bonus = 200
WHERE  position = 'assistant professor'

SELECT * FROM professor  WHERE  position = 'assistant professor'

SELECT position FROM professor WHERE name = 'Sharon Stone'
Professor 테이블에서 'Sharon Stone'과 같은 직급 중에서 
pay가  250 미만인 데이터를 15% 인상
UPDATE professor SET    pay = pay * 1.15
WHERE  pay < 250 AND    position = 'instructor'

UPDATE professor 
SET    pay = pay * 1.15
WHERE  pay < 250 
AND    position = (SELECT position 
                   FROM professor 
                   WHERE name = 'Sharon Stone')
                   
                   
DELETE dept2
WHERE  dcode BETWEEN '9000' AND '9999'


CREATE TABLE charge_01(
  u_date  VARCHAR2(6),
  cust_no NUMBER,
  u_time  NUMBER,
  charge  NUMBER) 

CREATE TABLE charge_02(
  u_date  VARCHAR2(6),
  cust_no NUMBER,
  u_time  NUMBER,
  charge  NUMBER) 
  
CREATE TABLE ch_total(
  u_date  VARCHAR2(6),
  cust_no NUMBER,
  u_time  NUMBER,
  charge  NUMBER) 
  
INSERT INTO charge_01 VALUES('141001', 1000, 2, 1000)
INSERT INTO charge_01 VALUES('141001', 1001, 2, 1000)
INSERT INTO charge_01 VALUES('141001', 1002, 5, 500)

INSERT INTO charge_02 VALUES('141002', 1000, 3, 1500)
INSERT INTO charge_02 VALUES('141002', 1001, 4, 2000)
INSERT INTO charge_02 VALUES('141002', 1003, 1, 500)


MERGE INTO ch_total  total
USING      charge_01 ch01
ON         (total.u_date = ch01.u_date)
WHEN MATCHED THEN 
     UPDATE SET total.cust_no = ch01.cust_no
WHEN NOT MATCHED THEN 
     INSERT VALUES(ch01.u_date, ch01.cust_no, ch01.u_time, ch01.charge)

MERGE INTO ch_total  total
USING      charge_02 ch02
ON         (total.u_date = ch02.u_date)
WHEN MATCHED THEN 
     UPDATE SET total.cust_no = ch02.cust_no
WHEN NOT MATCHED THEN 
     INSERT VALUES(ch02.u_date, ch02.cust_no, ch02.u_time, ch02.charge)
     
ROLLBACK   
COMMIT



--------------------CONSTRAINT
CREATE TABLE new_emp1(
  no    NUMBER(4)   CONSTRAINT emp1_no_pk PRIMARY KEY,
  name  VARCHAR2(20)CONSTRAINT emp1_name_nn NOT NULL,
  jumin VARCHAR2(13)CONSTRAINT emp1_jumin_nn NOT NULL
                    CONSTRAINT emp1_jumin_uk UNIQUE,
  loc_code NUMBER(1)CONSTRAINT emp1_area_ck   CHECK(loc_code < 5),
  deptno VARCHAR2(6)CONSTRAINT emp1_deptno_fk REFERENCES dept2(dcode) )

INSERT INTO new_emp1 VALUES(1, 'Lee', '1111111234567', 1, '1000')
INSERT INTO new_emp1 VALUES(2, 'Kim', '2222221234567', 1, '1000')
INSERT INTO new_emp1(no, name, jumin, loc_code, deptno)
VALUES(3,'Kim', '3333331234567', 1, '1000')
INSERT INTO new_emp1 VALUES(4, 'Han', '4444441234567', 4, '1000')
INSERT INTO new_emp1 VALUES(5, 'Ahn', '5555551234567', 4, '2000')



CREATE TABLE new_emp2(
  no    NUMBER(4)       PRIMARY KEY,
  name  VARCHAR2(20)    NOT NULL,
  jumin VARCHAR2(13)    NOT NULL UNIQUE,
  loc_code NUMBER(1)    CHECK(loc_code < 5),
  deptno VARCHAR2(6)    REFERENCES dept2(dcode) )
  
  
ALTER TABLE new_emp2
ADD   CONSTRAINT emp2_name_uk UNIQUE(name)

--ALTER TABLE new_emp2
--ADD   CONSTRAINT emp2_loccode_nn NOT NULL(loc_code)

ALTER TABLE new_emp2
MODIFY (loc_code CONSTRAINT emp2_loccode_nn NOT NULL)


ALTER TABLE new_emp2 
ADD   CONSTRAINT emp2_no_fk FOREIGN KEY(no)
                            REFERENCES emp2(empno)
                            
ALTER TABLE new_emp2 
ADD   CONSTRAINT emp2_name_fk FOREIGN KEY(name)
                              REFERENCES emp2(name)
                              
ALTER TABLE emp2
ADD   CONSTRAINT emp2_name_ukk UNIQUE(name)

ALTER TABLE new_emp2 
ADD   CONSTRAINT emp2_name_fk FOREIGN KEY(name)
                              REFERENCES emp2(name)
                              
CREATE TABLE emp22 AS SELECT * FROM emp2

ALTER TABLE emp22
ADD   CONSTRAINT emp22_empno_pk PRIMARY KEY(empno)

CREATE TABLE new_emp22(
  no    NUMBER          PRIMARY KEY,
  name  VARCHAR2(20)    NOT NULL,
  jumin VARCHAR2(13)    NOT NULL UNIQUE,
  loc_code NUMBER(1)    CHECK(loc_code < 5),
  deptno VARCHAR2(6)    REFERENCES dept2(dcode),
  
  CONSTRAINT  emp22_empno_fk    FOREIGN KEY (no)
                                REFERENCES emp22(empno)     
                                ON DELETE CASCADE
)
 
INSERT INTO new_emp22 VALUES(19900101, 'Goo', '1111111234567', 1, '1000')
SELECT * FROM new_emp22
DELETE emp22 WHERE empno = 19900101
ROLLBACK


CREATE TABLE new_emp222(
  no    NUMBER          PRIMARY KEY,
  name  VARCHAR2(20)    NOT NULL,
  jumin VARCHAR2(13)    NOT NULL UNIQUE,
  loc_code NUMBER(1)    CHECK(loc_code < 5),
  deptno VARCHAR2(6)    REFERENCES dept2(dcode),
  
  CONSTRAINT  emp222_empno_fk   FOREIGN KEY (no)
                                REFERENCES emp22(empno)     
                                ON DELETE SET NULL
)
 
INSERT INTO new_emp222 VALUES(19900101, 'Goo', '1111111234567', 1, '1000')
SELECT * FROM new_emp222
DELETE emp22 WHERE empno = 19900101
SELECT * FROM new_emp222

ALTER TABLE new_emp222 DROP   CONSTRAINT SYS_C007161 

ALTER TABLE emp22 DROP COLUMN empno

ALTER TABLE emp22 DROP COLUMN empno CASCADE CONSTRAINTS

DROP TABLE emp22
DROP TABLE new_emp22
DROP TABLE new_emp222

INSERT INTO t_novalidate VALUES(1, 'DDD')

ALTER TABLE t_novalidate 
DISABLE NOVALIDATE CONSTRAINT SYS_C007061
INSERT INTO t_novalidate VALUES(1, 'DDD')

INSERT INTO t_validate VALUES(4, NULL)
ALTER TABLE t_validate 
DISABLE VALIDATE CONSTRAINT TV_NAME_NN
INSERT INTO t_validate VALUES(4, NULL)
INSERT INTO t_validate VALUES(4, 'DDD')
INSERT INTO t_validate(no) VALUES(4)


INSERT INTO t_enable VALUES(1, 'AAA')
INSERT INTO t_enable VALUES(2, 'BBB')
INSERT INTO t_enable VALUES(3, NULL)

ALTER TABLE t_enable DISABLE CONSTRAINT TE_NAME_NN
INSERT INTO t_enable VALUES(3, NULL)

ALTER TABLE t_enable ENABLE NOVALIDATE CONSTRAINT TE_NAME_NN
INSERT INTO t_enable VALUES(4, NULL)
 
ALTER TABLE t_enable DISABLE CONSTRAINT TE_NAME_NN 
ALTER TABLE t_enable ENABLE VALIDATE CONSTRAINT TE_NAME_NN

SELECT * FROM t_enable
UPDATE t_enable SET name = 'CCC' WHERE  no = 3

ALTER TABLE t_enable ENABLE VALIDATE CONSTRAINT TE_NAME_NN



---------------------------INDEX
SELECT ROWNUM, ROWID, ename
FROM   emp


CREATE UNIQUE INDEX IDX_DEPT2_DNAME
ON dept2(dname)

INSERT INTO dept2 VALUES(9100, 'temp01', 1006, 'Seoul Branch')
INSERT INTO dept2 VALUES(9101, 'temp01', 1006, 'Busan Branch')


CREATE INDEX IDX_DEPT2_AREA ON dept2(area)
INSERT INTO dept2 VALUES(9101, 'temp02', 1006, 'Seoul Branch')


CREATE TABLE emp22
AS SELECT * FROM emp2

ALTER TABLE emp22
ADD CONSTRAINT emp22_empno_pk primary key(empno)

CREATE INDEX idx_emp22_pay ON emp22(pay)
CREATE INDEX idx_emp22_pay12 ON emp22(pay*12)
DROP INDEX idx_emp22_pay 
CREATE INDEX idx_emp22_pay_desc ON emp22(pay DESC)

CREATE INDEX idx_emp22_cmp ON emp22(pay, position)
CREATE BITMAP INDEX idx_emp22_pempno_bit ON emp22(pempno)
SELECT empno, name, pay, pay*12
FROM   emp22
WHERE  pay * 12 >= 1200000000

CREATE INDEX idx_emp_sal ON emp(sal)
DESC user_indexes
SELECT table_name, index_name, visibility
FROM   user_indexes
WHERE  table_name = 'EMP'

ALTER INDEX idx_emp_sal INVISIBLE
SELECT table_name, index_name, visibility
FROM   user_indexes
WHERE  table_name = 'EMP'

ALTER INDEX idx_emp_sal VISIBLE
SELECT table_name, index_name, visibility
FROM   user_indexes
WHERE  table_name = 'EMP'

SELECT /*+ index (emp idx_emp_sal) */ ename
FROM   emp
WHERE  ename > '0'

SELECT REGEXP_SUBSTR(tel, '[^)]+') AS DDD, 
       DECODE( REGEXP_SUBSTR(tel, '[^)]+'), '02',  'SEOUL')     AS SEOUL, 
       DECODE( REGEXP_SUBSTR(tel, '[^)]+'), '031', 'GYEONGGI')  AS GYEONGGI, 
       DECODE( REGEXP_SUBSTR(tel, '[^)]+'), '051', 'BUSAN')     AS BUSAN, 
       DECODE( REGEXP_SUBSTR(tel, '[^)]+'), '052', 'ULSAN')     AS ULSAN, 
       DECODE( REGEXP_SUBSTR(tel, '[^)]+'), '053', 'DAEGU')     AS DAEGU, 
       DECODE( REGEXP_SUBSTR(tel, '[^)]+'), '055', 'GYEONGNAM') AS GYEONGNAM
FROM   student

SELECT COUNT(*) || 'EA(' || COUNT(*)/COUNT(*)*100 || '%)'  AS TOTAL,
       COUNT( DECODE( REGEXP_SUBSTR(tel, '[^)]+'), '02',  1) ) || 'EA(' || 
       COUNT( DECODE( REGEXP_SUBSTR(tel, '[^)]+'), '02',  1) )/COUNT(*)*100 || '%)'
       AS SEOUL, 
       COUNT( DECODE( REGEXP_SUBSTR(tel, '[^)]+'), '031', 1) ) || 'EA(' AS GYEONGGI, 
       COUNT( DECODE( REGEXP_SUBSTR(tel, '[^)]+'), '051', 1) ) || 'EA(' AS BUSAN, 
       COUNT( DECODE( REGEXP_SUBSTR(tel, '[^)]+'), '052', 1) ) || 'EA(' AS ULSAN, 
       COUNT( DECODE( REGEXP_SUBSTR(tel, '[^)]+'), '053', 1) ) || 'EA(' AS DAEGU, 
       COUNT( DECODE( REGEXP_SUBSTR(tel, '[^)]+'), '055', 1) ) || 'EA(' AS GYEONGNAM
FROM   student



-----------------------------------------VIEW
SELECT * FROM emp_details_view

CREATE OR REPLACE VIEW v_emp1
AS  SELECT empno, ename, hiredate
    FROM   emp

SELECT * FROM v_emp1

CREATE TABLE t_emp
AS SELECT * FROM employees

CREATE OR REPLACE VIEW v_emp1
AS  SELECT empno, ename, hiredate
    FROM   emp
    
CREATE INDEX idx_v_emp_ename
ON     v_emp1(ename)

CREATE VIEW view_emp
AS  SELECT employee_id, last_name, job_id, salary
    FROM   t_emp
    WHERE  job_id ='IT_PROG' OR job_id='PU_CLERK'
WITH CHECK OPTION

CREATE OR REPLACE VIEW view_emp
AS  SELECT employee_id, first_name, job_id, salary
    FROM   t_emp
    WHERE  job_id ='IT_PROG' OR job_id='PU_CLERK'

CREATE OR REPLACE VIEW view_emp
AS  SELECT employee_id, last_name, job_id, salary
    FROM   t_emp
    WHERE  job_id ='IT_PROG' OR job_id='PU_CLERK'
WITH READ ONLY 


CREATE OR REPLACE VIEW view_emp(사원번호, 성명, 직무번호, 급여)
AS  SELECT employee_id, last_name, job_id, salary
    FROM   t_emp
    WHERE  job_id ='IT_PROG' OR job_id='PU_CLERK'

SELECT * FROM view_emp

UPDATE view_emp
SET    job_id = 'IT_PROG' WHERE  job_id = 'IT_GUY'

부서별 급여 총액과 평균을 조회만 가능한 view_sal 뷰 작성
단, 급여 총액이 큰 순서로 출력
CREATE OR REPLACE VIEW view_sal
AS  SELECT department_id, TO_CHAR(SUM(salary), '999,999') 급여총액, 
                       TO_CHAR(FLOOR(AVG(salary)),  '999,999') 급여평균
    FROM   t_emp  GROUP BY department_id  ORDER BY SUM(salary) DESC
WITH READ ONLY

CREATE OR REPLACE VIEW v_emp
AS  SELECT e.ename, d.dname
    FROM   emp e, dept d
    WHERE  e.deptno = d.deptno
    
SELECT * FROM v_emp


Finance 부서 소속 사원들의 
employee_id, first_name, salary, job_title, department_name, city를
조회하는 view_emp_fiance 뷰 작성
CREATE OR REPLACE VIEW view_emp_finance
AS  SELECT e.employee_id, e.first_name, e.salary,
           j.job_title, d.department_name, l.city
    FROM   t_emp e, departments d, jobs j, locations l
    WHERE  e.job_id = j.job_id
    AND    e.department_id = d.department_id(+)
    AND    d.location_id = l.location_id
    AND    d.department_name = 'Finance'
    
SELECT * FROM view_emp_finance    
        
        
SELECT e.deptno, d.dname, e.sal
FROM   (SELECT deptno, MAX(sal) sal
        FROM  emp GROUP BY deptno) e, dept d
WHERE   e.deptno = d.deptno


employees 테이블에서 job_id가 IT_PROG인 사원들의 소속 부서를 인라인뷰를 이용하여 출력
first_name, job_id, job_title, department_name

SELECT first_name, job_id, department_id
        FROM  employees
        WHERE job_id = 'IT_PROG'

SELECT  e.first_name, e.job_id, j.job_title, d.department_name
FROM   (SELECT first_name, job_id, department_id
        FROM  employees
        WHERE job_id = 'IT_PROG') e, departments d, jobs j
WHERE   e.job_id = j.job_id
AND     e.department_id = d.department_id

SELECT DECODE(deptno, ndeptno, null, deptno) DEPTNO,
       profno, name
FROM   ( SELECT LAG(deptno) OVER(ORDER BY deptno) ndeptno,
                deptno, profno, name
        FROM   professor)
        
DROP VIEW view_sal


SELECT ROWNUM 행번호, last_name, TO_CHAR(hire_date,'yyyy/MM/dd') hire_date
FROM   employees      ORDER BY 3

CREATE OR REPLACE VIEW v_emp_hire
AS  SELECT last_name, TO_CHAR(hire_date,'yyyy/MM/dd') hire_date
    FROM   employees  ORDER BY 2
    
SELECT ROWNUM 행번호, last_name, hire_date
FROM   v_emp_hire      
WHERE  ROWNUM <= 5 

employees 테이블에서 salary 상위 10명의 
ranking,first_name, job_id, salary 출력(인라인뷰를 이용)

SELECT  ROWNUM ranking, first_name, job_id, salary
FROM   (SELECT * FROM employees ORDER BY salary DESC)
WHERE   ROWNUM <= 10



-------------------------------- SUB QUERY

SELECT ename, comm FROM emp WHERE ename = 'WARD'
SELECT ename, comm FROM emp WHERE comm < 500

SELECT ename, comm 
FROM   emp 
WHERE  comm < ( SELECT comm 
                FROM emp WHERE ename = 'WARD' )
                
                
SELECT deptno1 FROM student WHERE name = 'Anthony Hopkins'

SELECT s.deptno1, s.name, d.dname
FROM   student s, department d
WHERE  s.deptno1 = d.deptno
AND    s.deptno1 = ( SELECT deptno1 FROM student 
                     WHERE name = 'Anthony Hopkins' )

SELECT hiredate FROM professor WHERE name = 'Meg Ryan'

SELECT p.name, p.hiredate, d.dname
FROM   professor p, department d
WHERE  p.deptno = d.deptno
AND    p.hiredate > ( SELECT hiredate FROM professor 
                      WHERE name = 'Meg Ryan')

SELECT AVG(weight) FROM student WHERE deptno1 = 201

SELECT name, weight
FROM   student
WHERE  weight > (SELECT AVG(weight) FROM student 
                 WHERE deptno1 = 201)
                                  
                                  
SELECT dcode  FROM   dept2 WHERE  area = 'Pohang Main Office'

SELECT empno, name, deptno
FROM   emp2
WHERE  deptno IN (SELECT dcode  FROM   dept2 
                  WHERE  area = 'Pohang Main Office')
                  
                  
SELECT deptno FROM dept WHERE deptno = 20

SELECT * FROM dept 
WHERE EXISTS ( SELECT deptno FROM dept WHERE deptno = 20)

SELECT * FROM dept 
WHERE EXISTS ( SELECT deptno FROM dept WHERE deptno = 90)
                                  
SELECT * FROM dept 
WHERE deptno IN ( SELECT deptno FROM dept WHERE deptno = 20)

SELECT department_id, first_name, salary
FROM   employees
WHERE  salary > ALL (SELECT salary FROM   employees
                     WHERE  department_id = 30 )
                     
                     
SELECT department_id, first_name, salary
FROM   employees
WHERE  salary > ANY (SELECT salary FROM   employees
                     WHERE  department_id = 30 )
                     
             
SELECT pay FROM   emp2 WHERE  position = 'Section head'

SELECT name, position, TO_CHAR(pay, '$999,999,999') SALARY
FROM   emp2
WHERE  pay > ANY(SELECT pay FROM   emp2 
                 WHERE  position = 'Section head')
ORDER BY 3 DESC

SELECT weight FROM student WHERE grade = 2

SELECT name, grade, weight
FROM   student
WHERE  weight < ALL( SELECT weight FROM student WHERE grade = 2 )

SELECT AVG(pay) FROM emp2 GROUP BY deptno

SELECT d.dname, e.name, TO_CHAR(e.pay, '$999,999,999') SALARY
FROM   dept2 d, emp2 e
WHERE  d.dcode = e.deptno
AND    e.pay < ALL( SELECT AVG(pay) FROM emp2 GROUP BY deptno )


SELECT grade, MAX(weight) FROM student GROUP BY grade

SELECT grade, name, weight
FROM   student
WHERE  (grade, weight) IN ( SELECT grade, MAX(weight) 
                            FROM student GROUP BY grade)
ORDER BY 1



SELECT deptno, MIN(hiredate) FROM   professor GROUP BY deptno
SELECT p.profno, p.name, p.hiredate, p.deptno, d.dname
FROM   professor p, department d
WHERE  p.deptno = d.deptno
AND    (p.deptno, p.hiredate) IN ( SELECT deptno, MIN(hiredate) 
                                   FROM   professor GROUP BY deptno )
ORDER BY 3


SELECT position, MAX(pay) FROM emp2 GROUP BY position
SELECT name, position, TO_CHAR(pay, '$999,999,999') salary
FROM   emp2
WHERE  (position, pay) IN (SELECT position, MAX(pay) 
                           FROM emp2 GROUP BY position)
ORDER BY 3


SELECT position, AVG(pay)  FROM emp2  GROUP BY position
SELECT name, position, pay 
FROM   emp2 a
WHERE  pay >= (SELECT AVG(pay)  FROM emp2 b
               WHERE a.position = b.position )


SELECT name, (SELECT dname FROM dept2 d
              WHERE  e.deptno = d.dcode) DNAME
FROM   emp2 e



------------------------paging
SELECT CEIL(COUNT(*)/10) pages  FROM   employees

SELECT * FROM employees ORDER BY hire_date DESC

SELECT rowno, first_name, hire_date
FROM   (SELECT ROWNUM AS rowno, m.*
        FROM  (SELECT * FROM employees 
                ORDER BY hire_date DESC ) m
        WHERE  ROWNUM <= 10 * 11 )
WHERE   rowno > 10 * (11 - 1)


-------------------------SEQNENCE & SYNONYM
CREATE SEQUENCE jno_seq
INCREMENT BY 1
START WITH   100
MAXVALUE     110
MINVALUE     90
CYCLE
CACHE 2

SELECT EMPLOYEES_SEQ.nextval FROM   dual
SELECT EMPLOYEES_SEQ.currval FROM   dual 
SELECT EMPLOYEES_SEQ.nextval FROM   dual

SELECT jno_seq.CURRVAL  FROM   dual 
SELECT jno_seq.NEXTVAL  FROM   dual 
SELECT jno_seq.CURRVAL  FROM   dual 

INSERT INTO temp01 VALUES(jno_seq.NEXTVAL, 'Lee')
INSERT INTO temp01 VALUES(jno_seq.NEXTVAL, 'LeeLee')
INSERT INTO temp01 VALUES(jno_seq.NEXTVAL, 'LeeLeeLee')


SELECT * FROM temp01
DROP SEQUENCE jno_seq

CREATE SYNONYM e FOR emp
SELECT * FROM e

CREATE PUBLIC SYNONYM d2 FOR dept
CREATE PUBLIC SYNONYM d22 FOR dept
SELECT * FROM d2
DROP SYNONYM e



----------------------- level
SELECT ename, empno, mgr FROM   emp

SELECT LEVEL,
       LPAD(ename, LEVEL * 4, '*')  ename FROM   emp
CONNECT BY PRIOR empno = mgr
START WITH       empno = 7839

SELECT LEVEL, 
       LPAD(ename, LEVEL * 5, '-')  ename FROM   emp
CONNECT BY       empno = PRIOR mgr
START WITH       empno = 7369

SELECT empno, ename, job, mgr, PRIOR ename AS mgrname, 
       LEVEL, LPAD(' ', (LEVEL-1)*2, ' ') || ename, 
       SYS_CONNECT_BY_PATH(ename, '-')                  FROM   emp
START WITH mgr IS NULL
CONNECT BY PRIOR empno = mgr
ORDER SIBLINGS BY empno


SELECT empno, ename, job, mgr, PRIOR ename AS mgrname, 
       LEVEL, LPAD(' ', (LEVEL-1)*2, ' ') || ename, 
       SYS_CONNECT_BY_PATH(ename, '-')                  FROM   emp
START WITH mgr IS NULL
CONNECT BY PRIOR empno = mgr AND ename != 'JONES'
ORDER SIBLINGS BY empno

SELECT empno, ename, job, mgr, PRIOR ename AS mgrname, 
       LEVEL, LPAD(' ', (LEVEL-1)*2, ' ') || ename, 
       SYS_CONNECT_BY_PATH(ename, '-')                  FROM   emp
WHERE  ename != 'JONES'
START WITH mgr IS NULL  CONNECT BY PRIOR empno = mgr
ORDER SIBLINGS BY empno



SELECT LEVEL, LPAD(ename, LEVEL * 5, '-')  ename, 
              SYS_CONNECT_BY_PATH(ename, '-')   FROM   emp
CONNECT BY       empno = PRIOR mgr
START WITH       empno = 7369


SELECT LEVEL, LPAD(ename, LEVEL * 5, '-')  ename, 
              SYS_CONNECT_BY_PATH(ename, '-')   FROM   emp
WHERE         CONNECT_BY_ISLEAF = 0
CONNECT BY       empno = PRIOR mgr
START WITH       empno = 7369

SELECT LEVEL, LPAD(ename, LEVEL * 5, '-')  ename, 
              SYS_CONNECT_BY_PATH(ename, '-')   FROM   emp
WHERE         CONNECT_BY_ISLEAF = 1
CONNECT BY       empno = PRIOR mgr
START WITH       empno = 7369

SELECT  LEVEL, empno, ename, CONNECT_BY_ROOT empno,
              SYS_CONNECT_BY_PATH(ename, '-')   FROM   emp
WHERE   LEVEL > 1 AND empno = 7369
CONNECT BY  PRIOR  empno = mgr


CREATE TABLE t_board(
    board_id    NUMBER   PRIMARY KEY,
    p_board_id  NUMBER,
    title       VARCHAR2(100),
    content     VARCHAR2(4000),
    reg_date    DATE)

CREATE SEQUENCE t_board_seq
INCREMENT BY 1  START WITH   1  MAXVALUE     9999999999999999999

ALTER TABLE t_board  RENAME  COLUMN board_id   TO board_no  
ALTER TABLE t_board  RENAME  COLUMN p_board_id TO p_board_no  


INSERT INTO t_board VALUES(t_board_seq.NEXTVAL, 0, 'THE', 'First', SYSDATE)
INSERT INTO t_board VALUES(t_board_seq.NEXTVAL, 8, 'real last', 're re re', SYSDATE)


SELECT  board_no, p_board_no, level,
        LPAD('--',  (LEVEL-1)*2) || title AS title1,
        LPAD('re:', (LEVEL-1)*5) || title AS title2
FROM    t_board
START WITH       p_board_no = 0
CONNECT BY PRIOR board_no = p_board_no


SELECT LEVEL, LPAD(e.name || '-' || d.dname || '-' || NVL(e.position, 'Worker'),
                  (LEVEL*27), '.') "Name and position"
FROM   emp2 e, dept2 d
WHERE  e.deptno = d.dcode
CONNECT BY PRIOR e.empno = e.pempno
START WITH e.pempno IS NULL


SELECT LEVEL, LPAD(' ', (LEVEL-1)*5, '-') || 
                   e.name || ' | ' || d.dname || ' | ' || 
                   NVL(e.position, 'Team Worker') 
                   AS "Name and position"
FROM   emp2 e, dept2 d
WHERE  e.deptno = d.dcode
CONNECT BY PRIOR e.empno = e.pempno
START WITH       e.pempno IS NULL
ORDER SIBLINGS BY e.name

SELECT LEVEL, LPAD(' ', (LEVEL-1)*5, '-') || 
                   e.name || ' | ' || d.dname || ' | ' || 
                   NVL(e.position, 'Team Worker') 
                   AS "Name and position"
FROM   emp2 e, dept2 d
WHERE  e.deptno = d.dcode
CONNECT BY PRIOR e.empno = e.pempno
START WITH       e.name = 'Kevin Bacon'


SELECT LEVEL, LPAD(' ', (LEVEL-1)*5, '-') || 
                   e.name || ' | ' || d.dname || ' | ' || 
                   NVL(e.position, 'Team Worker') 
                   AS "Name and position"
FROM   emp2 e, dept2 d
WHERE  e.deptno = d.dcode
CONNECT BY e.empno = PRIOR e.pempno
START WITH       e.name = 'Kevin Costner'


SELECT  name , PRIOR name "MGR NAME"
FROM    emp2
START WITH pempno IS NULL
CONNECT BY PRIOR empno = pempno


SELECT empno, e.name || ' | ' || d.dname || ' | ' || 
              NVL(e.position, 'Team Worker')  AS "Name",
              ( SELECT COUNT(*) FROM emp2
                START WITH empno = e.empno
                CONNECT BY PRIOR empno = pempno ) - 1 "COUNT"
FROM   emp2 e, dept2 d
WHERE  e.deptno = d.dcode
ORDER BY 3 DESC


SELECT LEVEL, LPAD(' ', (LEVEL-1)*5, '-') || 
                   e.name || ' | ' || d.dname || ' | ' || 
                   NVL(e.position, 'Team Worker') AS "Name and position",
              SYS_CONNECT_BY_PATH(e.name, ' - ') PATH
FROM   emp2 e, dept2 d
WHERE  e.deptno = d.dcode
CONNECT BY PRIOR e.empno = e.pempno
START WITH       e.name = 'Kevin Bacon'


-------------------------ACCOUNT MANAGEMENT
CREATE PROFILE prof_passwd LIMIT
FAILED_LOGIN_ATTEMPTS 5
PASSWORD_LOCK_TIME   30
PASSWORD_LIFE_TIME   30
PASSWORD_REUSE_TIME  30

CREATE PROFILE prof_resource LIMIT
CPU_PER_SESSION 1500
CONNECT_TIME     480
IDLE_TIME         15

ALTER SYSTEM SET RESOURCE_LIMIT = true

SELECT username, profile
FROM   dba_users
WHERE  username = 'HR' OR username = 'SYS'

SELECT  * FROM    DBA_PROFILES  WHERE   profile = 'DEFAULT'
SELECT  * FROM    DBA_PROFILES  WHERE   profile = 'PROF_PASSWD'
SELECT  * FROM    DBA_PROFILES  WHERE   profile = 'PROF_RESOURCE'

ALTER USER hr PROFILE prof_passwd
SELECT username, profile   FROM   dba_users   
WHERE  username = 'HR'

ALTER USER hr PROFILE prof_resource
SELECT username, profile   FROM   dba_users   
WHERE  username = 'HR'

DROP PROFILE prof_resource
DROP PROFILE prof_resource CASCADE

SELECT  *  FROM    dba_sys_privs WHERE   grantee = 'HR'

DESC dba_sys_privs

GRANT CREATE PUBLIC SYNONYM TO hr
REVOKE CREATE PUBLIC SYNONYM FROM hr

CREATE USER   user1  IDENTIFIED BY 1111
GRANT CREATE SESSION TO user1



SELECT  *  FROM    dba_sys_privs WHERE   grantee = 'HR'

REVOKE CREATE PUBLIC SYNONYM FROM hr

CREATE USER   user1  IDENTIFIED BY 1111
GRANT CREATE SESSION TO user1
GRANT CREATE TABLE TO user1
GRANT CREATE TABLESPACE TO user1
GRANT UNLIMITED TABLESPACE TO user1

GRANT  select ON hr.emp  TO   user1
REVOKE select ON hr.emp  FROM user1

GRANT  select ON hr.emp  TO   user1 WITH GRANT OPTION

CREATE USER   user2  IDENTIFIED BY 1111
GRANT CREATE SESSION TO user2

GRANT CREATE VIEW TO user1 WITH ADMIN OPTION

REVOKE CREATE VIEW FROM user1

SELECT * FROM dba_role_privs WHERE  grantee = 'HR' 
SELECT * FROM dba_sys_privs WHERE  grantee = 'CONNECT' 
SELECT * FROM dba_sys_privs WHERE  grantee = 'RESOURCE' 


CREATE USER   user3  IDENTIFIED BY 1111
CREATE USER   dev  IDENTIFIED BY 1111
CREATE USER   team1  IDENTIFIED BY 1111
CREATE USER   team2  IDENTIFIED BY 1111
CREATE USER   team3  IDENTIFIED BY 1111
CREATE USER   team4  IDENTIFIED BY 1111
GRANT CONNECT, RESOURCE TO user3
GRANT CONNECT, RESOURCE TO dev
GRANT CONNECT, RESOURCE TO team1
GRANT CONNECT, RESOURCE TO team2
GRANT CONNECT, RESOURCE TO team3
GRANT CONNECT, RESOURCE TO team4

CREATE ROLE testrole
GRANT CREATE TABLE TO testrole
GRANT testrole TO user2

SELECT * FROM dba_role_privs WHERE  grantee = 'USER2' 




---------------------------------------PL/SQL
SET SERVEROUTPUT ON
BEGIN
    DBMS_OUTPUT.PUT_LINE('Hello World');
END;

DECLARE 
    vno     NUMBER(4);
    vname   VARCHAR2(10);
BEGIN
    SELECT employee_id, first_name INTO vno, vname
    FROM   employees    WHERE  employee_id =200;
    DBMS_OUTPUT.PUT_LINE(vno || '   ' || vname);
END;


DECLARE 
    v_empid     employees.employee_id%TYPE;
    v_salary    employees.salary%TYPE;
BEGIN
    SELECT employee_id, salary INTO v_empid, v_salary
    FROM   employees    WHERE  employee_id = 197;
    DBMS_OUTPUT.PUT_LINE(v_empid || ' : ' || v_salary);
END;


DECLARE 
    v_empid     employees.employee_id%TYPE;
    v_salary    employees.salary%TYPE;
BEGIN
    SELECT employee_id, salary INTO v_empid, v_salary
    FROM   employees    WHERE  employee_id = &id;
    DBMS_OUTPUT.PUT_LINE(v_empid || ' : ' || v_salary);
END;


CREATE TABLE pl_test( 
    no NUMBER,
    name VARCHAR2(20)
)
    
CREATE SEQUENCE pl_seq

BEGIN
    INSERT INTO pl_test 
    VALUES(pl_seq.NEXTVAL, 'AAA');
END;

COMMIT



CREATE TABLE pl_test2( 
    no NUMBER,
    name VARCHAR2(10), 
    addr VARCHAR2(10)
)
 
DECLARE
    v_no    NUMBER          := '&no';
    v_name  VARCHAR2(10)    := '&name';
    v_addr  VARCHAR2(10)    := '&addr';
BEGIN
    INSERT INTO pl_test2 VALUES(v_no, v_name, v_addr);
END;

SET VERIFY OFF

BEGIN   
    UPDATE pl_test SET name = 'BBB' WHERE no = 2;
END;

BEGIN   
    DELETE pl_test WHERE no = 1;
END;


CREATE TABLE pl_merge1( 
    no NUMBER,
    name VARCHAR2(10)
)

CREATE TABLE pl_merge2
AS  SELECT * FROM pl_merge1


INSERT INTO pl_merge1 VALUES(1, 'AAA')
INSERT INTO pl_merge1 VALUES(2, 'BBB')
INSERT INTO pl_merge2 VALUES(1, 'CCC')
INSERT INTO pl_merge2 VALUES(3, 'DDD')

BEGIN
    MERGE INTO pl_merge2 m2
    USING      pl_merge1 m1
    ON(m1.no = m2.no)
    WHEN MATCHED THEN
        UPDATE SET m2.name = m1.name
    WHEN NOT MATCHED THEN
        INSERT VALUES(m1.no, m1.name);
END;

DECLARE
    v_first VARCHAR2(5) := 'Outer';
    v_second VARCHAR2(5) := 'Inner';    
BEGIN
    DECLARE
    --    v_second VARCHAR2(5) := 'Inner';    
    BEGIN
        DBMS_OUTPUT.PUT_LINE('in 1 : ' || v_first);
        DBMS_OUTPUT.PUT_LINE('in 2 : ' || v_second);
    END;
    DBMS_OUTPUT.PUT_LINE('out 1 : ' || v_first);
    DBMS_OUTPUT.PUT_LINE('out 2 : ' || v_second);
END;

CREATE TABLE pl_employees3
AS SELECT employee_id, first_name, salary
   FROM   employees
   
DECLARE
    v_row pl_employees3%ROWTYPE;
BEGIN
    SELECT * INTO v_row
    FROM   pl_employees3
    WHERE  employee_id = 180;
    DBMS_OUTPUT.PUT_LINE(v_row.employee_id || ' : ' ||
                         v_row.first_name  || ' : ' ||
                         v_row.salary);
END;

DECLARE
    TYPE emp_record_type IS RECORD(
        emp_id  employees.employee_id%TYPE,
        f_name  employees.first_name%TYPE,
        e_sal   employees.salary%TYPE 
    );
    v_rec1  emp_record_type;
BEGIN
    SELECT employee_id, first_name, salary  INTO v_rec1
    FROM   employees    WHERE employee_id = 180;
    DBMS_OUTPUT.PUT_LINE(v_rec1.emp_id || '|' || 
                         v_rec1.f_name || '|' || v_rec1.e_sal);
END;


DECLARE
    t_name  VARCHAR2(20);
    TYPE    tbl_emp_name  IS TABLE OF    
            employees.first_name%TYPE
            INDEX BY BINARY_INTEGER;
    v_name  tbl_emp_name;
BEGIN
    SELECT first_name   INTO t_name
    FROM   employees    WHERE  employee_id = 180;
    v_name(1) := t_name;
    DBMS_OUTPUT.PUT_LINE(v_name(1));
END;

VARIABLE v_bind NUMBER;
BEGIN
    SELECT (salary * 12) INTO :v_bind
    FROM   employees
    WHERE  employee_id = 180;
END;

PRINT  v_bind;


DECLARE     
    vempid  employees.employee_id%TYPE;
    vfname  employees.first_name%TYPE;
    vdeptid employees.department_id%TYPE;
    vdname  VARCHAR2(20);
BEGIN
    SELECT employee_id, first_name, department_id
           INTO vempid, vfname, vdeptid
    FROM   employees
    WHERE  employee_id = 203;
    
    IF(vdeptid = 10)   THEN vdname := 'Administration'; END IF;
    IF(vdeptid = 20)   THEN vdname := 'Marketing';      END IF;
    IF(vdeptid = 30)   THEN vdname := 'Purchasing';     END IF;
    IF(vdeptid = 40)   THEN vdname := 'Human Resources';END IF;
    DBMS_OUTPUT.PUT_LINE(vempid || '|' || vfname || '|' || 
                         vdeptid|| '|' || vdname);
END;



DECLARE     
    vempid  employees.employee_id%TYPE;
    vfname  employees.first_name%TYPE;
    vdeptid employees.department_id%TYPE;
    vdname  VARCHAR2(20);
    vsal    employees.salary%TYPE;
BEGIN
    SELECT employee_id, first_name, department_id, salary
           INTO vempid, vfname, vdeptid, vsal
    FROM   employees
    WHERE  employee_id = 203;
    
    IF(vsal >= 10000)     THEN  
        DBMS_OUTPUT.PUT_LINE(vsal || ' OVER 10000');
    ELSE                
        DBMS_OUTPUT.PUT_LINE(vsal || ' UNDER 10000');
    END IF;
    
    IF(vdeptid = 10)      THEN vdname := 'Administration'; 
    ELSIF(vdeptid = 20)   THEN vdname := 'Marketing';       
    ELSIF(vdeptid = 30)   THEN vdname := 'Purchasing';      
    ELSIF(vdeptid = 40)   THEN vdname := 'Human Resources';END IF;
    DBMS_OUTPUT.PUT_LINE(vempid || '|' || vfname || '|' || 
                         vdeptid|| '|' || vdname);
    DBMS_OUTPUT.PUT_LINE('------------------------------');                
    vdname := CASE vdeptid
                WHEN 10      THEN 'Administration' 
                WHEN 20      THEN 'Marketing'  
                WHEN 30      THEN 'Purchasing'   
                WHEN 40      THEN 'Human Resources'
              END;
    DBMS_OUTPUT.PUT_LINE(vempid || '|' || vfname || '|' || 
                         vdeptid|| '|' || vdname);
END;


DECLARE
    nol NUMBER := 0;
BEGIN 
    LOOP 
        DBMS_OUTPUT.PUT_LINE(nol);
        nol := nol + 1;
        EXIT WHEN nol > 5;
    END LOOP;
END;


DECLARE
    nol NUMBER := 0;
BEGIN 
    WHILE(nol <= 5) LOOP 
        DBMS_OUTPUT.PUT_LINE(nol);
        nol := nol + 1;
    END LOOP;
END;

BEGIN 
    FOR i IN 0..5 LOOP 
        DBMS_OUTPUT.PUT_LINE(i);
    END LOOP;
END;



DECLARE
    t_name  VARCHAR2(20);
    TYPE    emp_first_name  IS TABLE OF    
            employees.first_name%TYPE
            INDEX BY BINARY_INTEGER;
    v_name  emp_first_name;
    cnt     BINARY_INTEGER := 0;
BEGIN
    FOR i IN (SELECT first_name FROM employees) LOOP
        cnt := cnt + 1;
        v_name(cnt) := i.first_name;
    END LOOP;
    FOR j IN 1..cnt LOOP
        DBMS_OUTPUT.PUT_LINE(v_name(j));
    END LOOP;
END;


DECLARE
    vempid  employees.employee_id%TYPE;
    vfname  employees.first_name%TYPE;
    vdeptid employees.department_id%TYPE;
    CURSOR cl IS  SELECT employee_id, first_name, department_id
                  FROM   employees    WHERE  department_id = 30;
BEGIN
    OPEN cl;
    LOOP    FETCH cl INTO vempid, vfname, vdeptid;
            EXIT WHEN cl%NOTFOUND;
            DBMS_OUTPUT.PUT_LINE(vempid || '|' || vfname || '|' || vdeptid);
    END LOOP;
    CLOSE cl;
    DBMS_OUTPUT.PUT_LINE('--------------------1');
    FOR i   IN cl   LOOP     
         DBMS_OUTPUT.PUT_LINE(i.employee_id || '|' || 
                              i.first_name || '|' || i.department_id);
    END LOOP;
    DBMS_OUTPUT.PUT_LINE('--------------------2');
    FOR i   IN ( SELECT employee_id, first_name, department_id
                 FROM   employees    
                 WHERE  department_id = 30 )         LOOP     
         DBMS_OUTPUT.PUT_LINE(i.employee_id || '|' || 
                              i.first_name || '|' || i.department_id);
    END LOOP;
END;


DECLARE     
    vfname  employees.first_name%TYPE;
BEGIN
    SELECT first_name    INTO vfname 
    FROM   employees
    WHERE  first_name LIKE 'B%';
    DBMS_OUTPUT.PUT_LINE('사원명은 ' || vfname);
    
    EXCEPTION
        WHEN NO_DATA_FOUND  THEN    
            DBMS_OUTPUT.PUT_LINE('해당 사원 X');
        WHEN TOO_MANY_ROWS  THEN    
            DBMS_OUTPUT.PUT_LINE('해당 사원 두 명 이상');
END;


SELECT first_name, salary FROM   employees 
WHERE  employee_id = 206


BEGIN 
    UPDATE employees SET salary = salary + 5000
    WHERE  employee_id = 206;
END;

SELECT first_name, salary FROM   employees 
WHERE  employee_id = 206

ROLLBACK

CREATE TABLE employees2 AS  SELECT * FROM employees


CREATE OR REPLACE PROCEDURE up_sal(
    vempid employees2.employee_id%TYPE )
IS 
    BEGIN 
        UPDATE employees2 SET salary = salary + 5000
        WHERE  employee_id = vempid;
    END;
/

SELECT first_name, salary FROM   employees2
WHERE  employee_id = 206

EXEC up_sal(206)

ROLLBACK

DROP PROCEDURE up_sal

SELECT text
FROM   user_source
WHERE  name = 'UP_SAL'


-------------------------- FUNCTION
CREATE OR REPLACE FUNCTION max_sal(
    s_deptno    employees.department_id%TYPE )
    RETURN NUMBER
IS
    max_sal     employees.salary%TYPE;
BEGIN
    SELECT MAX(salary) INTO max_sal
    FROM   employees   WHERE department_id = s_deptno;
    RETURN max_sal;
END;

SELECT max_sal(10) FROM dual;
SELECT max_sal(20) FROM dual;
SELECT DISTINCT max_sal(20) FROM employees;

SELECT text
FROM   user_source
WHERE  type = 'FUNCTION' 
AND    name = 'MAX_SAL'



-------------------------PACKAGE
CREATE OR REPLACE PACKAGE emp_total
AS
    PROCEDURE emp_sum;
    PROCEDURE emp_avg;
END emp_total;

CREATE OR REPLACE PACKAGE BODY emp_total AS  
    PROCEDURE emp_sum IS  
        CURSOR emp_total_sum IS  
            SELECT COUNT(*), SUM(NVL(sal, 0)) FROM   emp;
            total_num   NUMBER;
            total_sum   NUMBER;
        BEGIN   OPEN emp_total_sum;
                FETCH emp_total_sum INTO total_num, total_sum;
                DBMS_OUTPUT.PUT_LINE(' 총 인원수 : ' ||total_num || 
                                     ' 급여 합계 : ' ||total_sum);
                CLOSE emp_total_sum;
    END emp_sum;
    
    PROCEDURE emp_avg IS  
        CURSOR emp_total_avg IS  
            SELECT COUNT(*), AVG(NVL(sal, 0)) FROM   emp;
            total_num   NUMBER;
            total_avg   NUMBER;
        BEGIN   OPEN emp_total_avg;
                FETCH emp_total_avg INTO total_num, total_avg;
                DBMS_OUTPUT.PUT_LINE(' 총 인원수 : ' ||total_num || 
                                     ' 급여 평균 : ' ||total_avg);
                CLOSE emp_total_avg;
    END emp_avg;
END emp_total;

SET SERVEROUTPUT ON
EXEC emp_total.emp_sum
EXEC emp_total.emp_avg


SELECT text
FROM   user_source
WHERE  type = 'PACKAGE'


SELECT text
FROM   user_source
WHERE  type LIKE 'PACKAGE BODY'


emp22 테이블의 모든 레코드를 삭제하는 del_all_emp22 프로시저 작성

CREATE OR REPLACE PROCEDURE del_all_emp22 IS
BEGIN
    DELETE FROM emp22;
END;

emp22 테이블의 레코드가 삭제되면 메시지를 출력하는 trg_del_emp22 트리거 작성
CREATE OR REPLACE TRIGGER trg_del_emp22
AFTER  DELETE ON emp22
BEGIN   
    DBMS_OUTPUT.PUT_LINE('-- Data deleted on emp22 --');
END;

EXEC del_all_emp22
ROLLBACK


emp22 테이블에 레코드가 추가되기 전에
현재 레코드 수를 출력하는 cnt_emp22 프로시저를 호출하는 trg_cnt_emp22 트리거 작성

CREATE OR REPLACE PROCEDURE cnt_emp22 
IS  cnt_emp22 NUMBER;
BEGIN
    SELECT COUNT(*) INTO cnt_emp22
    FROM emp22;
    DBMS_OUTPUT.PUT_LINE('emp22 테이블의 총 레코드 수 : ' || cnt_emp22);
END;

EXEC cnt_emp22;

CREATE OR REPLACE TRIGGER trg_cnt_emp22
BEFORE INSERT ON emp22
BEGIN   
    cnt_emp22;
END;

INSERT INTO emp22(empno, name, deptno)
VALUES (20000999, 'Han', '9999')


EXEC cnt_emp22;


emp22 테이블에 레코드가 추가되면
추가된 empno를 테이블의 이름(user_empno)로 테이블을 생성하는
트리거 trg_tbl_emp22 작성




매개변수로 넘겨받은 임의의 이름으로 테이블을 생성하는 
create_tbl 프로시저 작성

CREATE OR REPLACE PROCEDURE create_tbl(tbl_name  VARCHAR2 )
IS 
    stmt  VARCHAR2(1000);
BEGIN 
    stmt := 'CREATE TABLE user_' || tbl_name ||
            '(no NUMBER, timeinfo DATE)';
    DBMS_OUTPUT.PUT_LINE('stmt : ' || stmt);
    DBMS_OUTPUT.PUT_LINE('table name : user_' || tbl_name);
    EXECUTE IMMEDIATE stmt;
    DBMS_OUTPUT.PUT_LINE('table created');
END;

EXEC create_tbl('aaa')




----------------------DEV WORK
CREATE TABLE t_member(
    user_id     VARCHAR2(20) PRIMARY KEY,
    user_pw     VARCHAR2(20),
    user_nm     VARCHAR2(20),
    email       VARCHAR2(100),
    gender      CHAR(1),
    photo       VARCHAR2(100),
    reg_date    DATE
)



----------------SPRING --------------------
CREATE SEQUENCE seq_board

CREATE TABLE tbl_board(
   bno      NUMBER(10, 0),
   title    VARCHAR2(200) NOT NULL,
   content  VARCHAR2(2000) NOT NULL,
   writer   VARCHAR2(50) NOT NULL,
   regdate  DATE DEFAULT sysdate,
   updatedate DATE DEFAULT sysdate)
   
ALTER TABLE tbl_board 
ADD CONSTRAINT pk_board PRIMARY KEY(bno)


SELECT * FROM tbl_board ORDER BY bno DESC

SELECT /*+INDEX_DESC (tbl_board pk_board) */ *
FROM   tbl_board


-------- 댓글
CREATE TABLE tbl_reply(
    rno     NUMBER(10, 0),
    bno     NUMBER(10, 0) NOT NULL,
    reply   VARCHAR2(1000)  NOT NULL,
    replyer VARCHAR2(50)    NOT NULL,
    replyDate   DATE DEFAULT SYSDATE,
    updateDate  DATE DEFAULT SYSDATE )

CREATE SEQUENCE seq_reply

ALTER TABLE tbl_reply
ADD CONSTRAINTS pk_reply PRIMARY KEY(rno)

ALTER TABLE tbl_board
ADD CONSTRAINTS pk_board PRIMARY KEY(bno)

ALTER TABLE tbl_reply
ADD CONSTRAINTS fk_reply FOREIGN KEY(bno)
REFERENCES tbl_board(bno)


CREATE INDEX idx_reply ON tbl_reply(bno DESC, rno ASC)


CREATE TABLE tbl_sample1(col1 VARCHAR2(100))
CREATE TABLE tbl_sample2(col2 VARCHAR2(10))


ALTER TABLE tbl_board ADD(replycnt NUMBER DEFAULT 0)

UPDATE tbl_board
SET    replycnt = (SELECT COUNT(rno) 
                    FROM   tbl_reply
                    WHERE  tbl_reply.bno = tbl_board.bno)
                    
                    
                    
--------------- 첨부 파일
CREATE TABLE tbl_attach(
   uuid         VARCHAR2(100)  NOT NULL,
   uploadPath    VARCHAR2(200)  NOT NULL,
   fileName      VARCHAR2(100)  NOT NULL,
   fileType      CHAR(1) default 'I',
   bno           NUMBER(10, 0)
)

ALTER TABLE tbl_attach 
ADD CONSTRAINT pk_attach PRIMARY KEY(uuid)

ALTER TABLE tbl_attach
ADD CONSTRAINT fk_board_attach FOREIGN KEY(bno)
REFERENCES tbl_board(bno)

CREATE TABLE users(
    username VARCHAR2(50) not null primary key,
    password VARCHAR2(50) not null,
    enabled char(1) default '1')
    
CREATE TABLE authorities(
    username VARCHAR2(50) not null,
    authority VARCHAR2(50) not null,
    constraint fk_authorities_users foreign key(username) references users(username))

CREATE unique index ix_auth_username on authorities (username, authority)

insert into users (username, password) values ('user00', 'pw00')
insert into users (username, password) values ('member00', 'pw00')
insert into users (username, password) values ('admin00', 'pw00')

insert into authorities (username, authority) values ('user00','ROLE_USER');
insert into authorities (username, authority) values ('member00','ROLE_MANAGER');
insert into authorities (username, authority) values ('admin00','ROLE_MANAGER');
insert into authorities (username, authority) values ('admin00','ROLE_ADMIN');

create table tbl_member(
    userid varchar2(50) not null primary key,
    userpw varchar2(100) not null,
    username varchar2(100) not null,
    regdate date default sysdate,
    updatedate date default sysdate,
    enabled char(1) default '1')
    
create table tbl_member_auth(
    userid varchar2(50) not null,
    auth varchar2(50) not null,
    constraint fk_member_auth foreign key(userid) references tbl_member(userid))
    
SELECT * FROM tbl_member_auth WHERE userid='admin99'


SELECT m.userid, userpw, username, regdate, updatedate, enabled, auth FROM tbl_member m 
JOIN tbl_member_auth a ON m.userid = a.userid AND a.userid = 'admin99'
--inner join

SELECT m.userid, userpw, username, regdate, updatedate, enabled, auth FROM tbl_member m 
, tbl_member_auth a WHERE m.userid = a.userid AND a.userid = 'admin99'
--oracle inner join


CREATE OR REPLACE PROCEDURE create_tbl(tbl_name  VARCHAR2 )
IS 
    stmt  VARCHAR2(1000);
BEGIN 
    stmt := 'CREATE TABLE user_' || tbl_name ||
            '(no NUMBER, timeinfo DATE)';
    DBMS_OUTPUT.PUT_LINE('stmt : ' || stmt);
    DBMS_OUTPUT.PUT_LINE('table name : user_' || tbl_name);
    EXECUTE IMMEDIATE stmt;
    DBMS_OUTPUT.PUT_LINE('table created');
END;

EXEC create_tbl('aaa')




----------------------DEV WORK
CREATE TABLE t_member(
    user_id     VARCHAR2(20) PRIMARY KEY,
    user_pw     VARCHAR2(20),
    user_nm     VARCHAR2(20),
    email       VARCHAR2(100),
    gender      CHAR(1),
    photo       VARCHAR2(100),
    reg_date    DATE
)



----------------SPRING --------------------
CREATE SEQUENCE seq_board

CREATE TABLE tbl_board(
   bno      NUMBER(10, 0),
   title    VARCHAR2(200) NOT NULL,
   content  VARCHAR2(2000) NOT NULL,
   writer   VARCHAR2(50) NOT NULL,
   regdate  DATE DEFAULT sysdate,
   updatedate DATE DEFAULT sysdate)
   
ALTER TABLE tbl_board 
ADD CONSTRAINT pk_board PRIMARY KEY(bno)


SELECT * FROM tbl_board ORDER BY bno DESC

SELECT /*+INDEX_DESC (tbl_board pk_board) */ *
FROM   tbl_board


-------- 댓글
CREATE TABLE tbl_reply(
    rno     NUMBER(10, 0),
    bno     NUMBER(10, 0) NOT NULL,
    reply   VARCHAR2(1000)  NOT NULL,
    replyer VARCHAR2(50)    NOT NULL,
    replyDate   DATE DEFAULT SYSDATE,
    updateDate  DATE DEFAULT SYSDATE )

DECLARE
sell NUMBER;
BEGIN 
    FOR i IN 1..130 LOOP 
        SELECT seq_reply.nextval INTO sell from dual;
    END LOOP;
END;

BEGIN 
    FOR i IN 10..200 LOOP 
        INSERT INTO tbl_reply VALUES(i, 1, '내용'||i, '작성자'||i, SYSDATE, SYSDATE);
    END LOOP;
END;

DECLARE
    date1 DATE := SYSDATE-3;
    i NUMBER:= 1;
BEGIN 
    WHILE SYSDATE > date1
    LOOP
        INSERT INTO tbl_reply VALUES(i, 1, '내용'||i, '작성자'||i, date1, date1);
        date1 := date1 + dbms_random.value(0, 60*60)/(24*60*60);
        i := i+1;
        EXIT WHEN SYSDATE < date1;
    END LOOP;
END;
COMMIT

DECLARE
      vn_base_num NUMBER := 3;
      vn_cnt      NUMBER := 1;
    BEGIN

      WHILE  vn_cnt <= 9           -- vn_cnt가 9보다 작거나 같을 때만 반복 처리
      LOOP
         DBMS_OUTPUT.PUT_LINE (vn_base_num || '*' || vn_cnt || '= ' || vn_base_num * vn_cnt);
         EXIT WHEN vn_cnt = 5;     -- vn_cnt 값이 5가 되면 루프 종료
         vn_cnt := vn_cnt + 1;     -- vn_cnt 값을 1씩 증가
      END LOOP;
    END;

SELECT SYSDATE+ dbms_random.value(0, 24*60*60)/(24*60*60) FROM dual
dbms_random.value(0, 24*60*60)

CREATE SEQUENCE seq_reply

ALTER TABLE tbl_reply
ADD CONSTRAINTS pk_reply PRIMARY KEY(rno)

ALTER TABLE tbl_reply
ADD CONSTRAINTS fk_reply FOREIGN KEY(bno)
REFERENCES tbl_board(bno)

CREATE INDEX idx_reply on tbl_reply (bno desc, rno desc)

DROP INDEX idx_reply

CREATE INDEX idx_reply on tbl_reply (bno desc, rno desc)

CREATE INDEX idx_memo on t_memo (memo_no desc)

BEGIN 
    FOR i IN 1..500 LOOP 
        INSERT INTO t_memo VALUES(i, 'user'||i, '내용'||i, SYSDATE);
    END LOOP;
END;

CREATE TABLE t_memo(
    memo_no NUMBER(10,0),
    user_id	VARCHAR2(15 BYTE),
    content	VARCHAR2(100 BYTE),
    r_date	DATE
    reply_cnt	NUMBER(10,0)
    )

DECLARE
    date1 DATE := SYSDATE-7;
    i NUMBER:= 1;
BEGIN 
    WHILE SYSDATE > date1
    LOOP
        INSERT INTO t_memo VALUES(i, 'user'||i, '내용'||i, date1, 1);
        date1 := date1 + dbms_random.value(0, 60*60)/(24*60*60);
        i := i+1;
        EXIT WHEN SYSDATE < date1;
    END LOOP;
END;

INSERT INTO t_memo VALUES(1, 'user01', '내용', SYSDATE, 1)

COMMIT
DROP TABLE t_memo

SELECT * FROM (SELECT /*+INDEX_DESC(memo_no) */rownum rn, 
		memo_no, user_id, content, r_date FROM t_memo 
		WHERE rownum <= 1*10 ORDER BY memo_no) WHERE rn > 1-1*10
        
        (SELECT /*+INDEX(tbl_board pk_board) */rownum rn, 
		memo_no, user_id, content, r_date FROM t_memo 
		WHERE rownum <= 1*2 ORDER BY memo_no)
        idx_memo
SELECT rownum rn, 
		memo_no, user_id, content, r_date FROM t_memo 
		WHERE rownum <= 3*10 ORDER BY memo_no
        
SELECT * FROM (SELECT rownum rn, 
            memo_no, user_id, content, r_date FROM t_memo 
WHERE rownum <= 2*10 ORDER BY memo_no DESC) WHERE rn > (2-1)*10

CREATE SEQUENCE t_reply_seq

CREATE SEQUENCE p_seq

INSERT INTO t_reply(reply_no, memo_no, reply, replyer)
	VALUES(t_reply_seq.NEXTVAL, 341, 'a', 'b')
drop table tbl_sample2

ALTER TABLE tbl_sample1 MODIFY (col1 VARCHAR2(100))
CREATE table tbl_sample1(col1 varchar2(10))
CREATE table tbl_sample2(col2 varchar2(10))

ALTER TABLE tbl_board add(replycnt number default 0)

UPDATE tbl_board SET replycnt = (select count(rno) from tbl_reply
WHERE tbl_reply.bno = tbl_board.bno)

CREATE table t_attach(
    uuid varchar2(100) not null,
    upload_path varchar2(200) not null,
    file_name varchar2(100) not null,
    filetype char(1) default 'I',
    memo_no number(10,0)
    );
    
DROP table t_attach

DELETE tbl_attach WHERE bno = 285

INSERT ALL INTO tbl_attach VALUES(profno, name)
SELECT profno, name FROM professor
WHERE  profno BETWEEN 3000 AND 3999

DROP TABLE persistent_logins

CREATE TABLE persistent_logins (
    username VARCHAR2(64) NOT NULL,
    series   VARCHAR2(64) PRIMARY KEY,
    token    VARCHAR2(64) NOT NULL,
    last_userd timestamp NOT NULL)
    
ALTER TABLE t_reply DROP CONSTRAINT T_REPLY_FK
ALTER TABLE t_reply ADD CONSTRAINT T_REPLY_FK FOREIGN KEY (memo_no)  
REFERENCES t_memo(memo_no) ON DELETE CASCADE 

drop table t_member
--create table t_member(
--    user_id varchar2(50) not null primary key,
--    user_nm varchar2(100) not null,
--    user_pw varchar2(100) not null,
--    email1 varchar2(100) not null,
--    email2 varchar2(100) not null,
--    gender char(1) not null,
--    intro 
--    regdate date default sysdate,
--    updatedate date default sysdate,
--    enabled char(1) default '1')

ALTER TABLE t_member  MODIFY (user_pw VARCHAR2(100))

UPDATE 	tbl_reply 
    SET		reply = 33, updateDate = sysdate
    WHERE 	rno = 2
    
SELECT * FROM tbl_attach WHERE bno = 2

DELETE tbl_attach WHERE bno = 2 

ALTER TABLE tbl_attach ADD CONSTRAINT FK_tbl_attach FOREIGN KEY (bno)
REFERENCES tbl_board (bno) ON DELETE CASCADE

ALTER TABLE tbl_attach DROP CONSTRAINT FK_tbl_attach

ALTER TABLE tbl_reply DROP CONSTRAINT FK_reply

ALTER TABLE tbl_reply ADD CONSTRAINT FK_reply FOREIGN KEY (bno)
REFERENCES tbl_board (bno) ON DELETE CASCADE

SELECT * FROM t_battle

SELECT * FROM t_reply

CREATE TABLE t_reply(
    reply_no     NUMBER(10, 0),
    memo_no     NUMBER(10, 0) NOT NULL,
    reply   VARCHAR2(1000)  NOT NULL,
    replyer VARCHAR2(50)    NOT NULL,
    replyDate   DATE DEFAULT SYSDATE,
    updateDate  DATE DEFAULT SYSDATE )
    
ALTER TABLE t_reply ADD CONSTRAINT t_reply_FK FOREIGN KEY (memo_no)
REFERENCES t_memo (memo_no) ON DELETE CASCADE

ALTER TABLE t_memo
MODIFY   (memo_no NUMBER(10, 0))

CREATE TABLE t_attach(
   uuid         VARCHAR2(100)  NOT NULL,
   uploadPath    VARCHAR2(200)  NOT NULL,
   fileName      VARCHAR2(100)  NOT NULL,
   fileType      CHAR(1) default 'I',
   memo_no           NUMBER(10, 0)
)


ALTER TABLE t_attach 
ADD CONSTRAINT t_attach_pk PRIMARY KEY(uuid)

ALTER TABLE t_attach
ADD CONSTRAINT t_memo_attach_fk FOREIGN KEY(memo_no)
REFERENCES t_memo(memo_no)


DROP TABLE t_member
DROP TABLE t_memo
DROP TABLE t_reply
DROP TABLE t_Attach



CREATE TABLE t_member(
    user_id VARCHAR2(15),
    user_name VARCHAR2(30),
    user_password VARCHAR2(64),
    email   VARCHAR2(320),
    gender  CHAR(1),
    photo   VARCHAR2(100 CHAR),
    intro   VARCHAR2(100 CHAR),
    regist_date DATE,
    update_date DATE,
    enabled  CHAR(1) )
             
ALTER TABLE tbl_member  DROP COLUMN enabled

ALTER TABLE tbl_member2  RENAME COLUMN userid TO user_id
ALTER TABLE tbl_member2  RENAME COLUMN userpw TO user_password
ALTER TABLE tbl_member2  RENAME COLUMN username TO user_name


ALTER TABLE tbl_member_auth2  RENAME COLUMN userid TO user_id

DROP TABLE tbl_member2